﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Branch.Master" AutoEventWireup="true" CodeBehind="BranchOrdersList.aspx.cs" Inherits="AyyappaInfra.BranchOrdersList" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Order's List
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>

                              
                            <div class="col-md-3">
                                <div class="form-group">
                                  Search Customer<asp:TextBox ID="txtcust" CssClass="form-control" OnTextChanged="Customer_Change" AutoPostBack="true" runat="server"></asp:TextBox>
                                     <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtcust"
                                                MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1"
                                                CompletionInterval="1" ServiceMethod="GetVendor" UseContextKey="True">
                                            </cc1:AutoCompleteExtender>
                                </div>
                            </div>
                                      </ContentTemplate>
                            </asp:UpdatePanel>
                              <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="billno" HeaderText="Bill No" />
                                        <asp:BoundField DataField="CustomerName" HeaderText="Name" />
                                        <asp:BoundField DataField="Mobile" HeaderText="Mobile" />
                                        <asp:BoundField DataField="Total" HeaderText="Total" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnview" CssClass="btn btn-success btn-xs" runat="server" Text="View" CommandArgument='<%# Eval("Billno") %>' OnClick="View_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnprint" runat="server" CssClass="btn btn-warning btn-xs" Text="Print Bill" CommandArgument='<%# Eval("Billno") %>' OnClick="Print_Click" />
                                                </ItemTemplate>
                                             </asp:TemplateField>--%>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnedit" CssClass="btn btn-primary btn-xs" runat="server" CommandArgument='<%# Eval("Billno") %>' Text="Bill" OnClick="Bill_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                         <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnprint" CssClass="btn btn-warning btn-xs" runat="server" CommandArgument='<%# Eval("Billno") %>' Text="Print Bill" OnClick="Print_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                                    </ContentTemplate>
                                  </asp:UpdatePanel>

                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" CancelControlID="btnclose" PopupControlID="Panel1"></cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="500px" Width="650px">
                                 <h3 class="text-center">Item Details</h3>
                                <hr />
                                  <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                <asp:GridView ID="Gridpopup" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="Brand" HeaderText="Brand" />
                                        <asp:BoundField DataField="Product" HeaderText="Product" />
                                        <asp:BoundField DataField="Category" HeaderText="Category" />
                                        <asp:BoundField DataField="Size" HeaderText="Size" />
                                        <asp:BoundField DataField="Quantity" HeaderText="Qty" />
                                        <asp:BoundField DataField="Price" HeaderText="Price" />
                                        <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                        <asp:BoundField DataField="Tax" HeaderText="Tax" />
                                        <asp:BoundField DataField="TaxAmount" HeaderText="Tax Amt" />
                                        <asp:BoundField DataField="totalamt" HeaderText="Total" />
                                    </Columns>
                                </asp:GridView>
                                    </ContentTemplate>
                                      </asp:UpdatePanel>
                                  <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                <div class="clearfix"></div>
                                <div class=" col-md-12 text-right">
                                     
                                    <span style="padding-right:150px">   Sub Total <asp:Label ID="lblsubtotal" runat="server" Text='<%# Eval("subtotal") %>'></asp:Label></span>
                                    
                                
                                  <span style="padding-right:2px"> 
                                        Total <asp:Label ID="lbltotal" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                        </span>
                                     </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                        <asp:Button ID="btnclose" CssClass="btn btn-danger" runat="server" Text="Close" />
                                        </div>
                                    </div>
                            </asp:Panel>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
