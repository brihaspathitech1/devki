﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace AyyappaInfra
{
    public partial class Logins : System.Web.UI.Page
    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["DevkiInterior"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindGrid();
            }
        }

        protected void Submit_Click(object sender,EventArgs e)
        {
            if(txtemp.Text==""||droptype.SelectedItem.Text==""||dropbranch.SelectedItem.Text==""||txtuser.Text==""||txtpwd.Text=="")
            {
                string message = "alert('Fill all the values...!')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            }
            else
            {
                //if(droptype.SelectedItem.Text=="Admin")
                //{
                //    SqlCommand cmd = new SqlCommand("Insert into Login(EmployeeName,Role,Branch,Name,Password,DashBoard) values('" + txtemp.Text + "','" + droptype.SelectedItem.Text + "','" + dropbranch.SelectedItem.Text + "','" + txtuser.Text + "','" + txtpwd.Text + "','ItemsAdd')", connstr);
                //    connstr.Open();
                //    cmd.ExecuteNonQuery();
                //    connstr.Close();
                //}
                /*else*/ if(droptype.SelectedItem.Text=="Branch Employee")
                {
                    SqlCommand cmd = new SqlCommand("Insert into Login(EmployeeName,Role,Branch,Name,Password,DashBoard) values('" + txtemp.Text + "','" + droptype.SelectedItem.Text + "','" + dropbranch.SelectedItem.Text + "','" + txtuser.Text + "','" + txtpwd.Text + "','BranchBilling')", connstr);
                    connstr.Open();
                    cmd.ExecuteNonQuery();
                    connstr.Close();
                }
                else if (droptype.SelectedItem.Text == "Super Admin")
                {
                    SqlCommand cmd = new SqlCommand("Insert into Login(EmployeeName,Role,Branch,Name,Password,DashBoard) values('" + txtemp.Text + "','" + droptype.SelectedItem.Text + "','" + dropbranch.SelectedItem.Text + "','" + txtuser.Text + "','" + txtpwd.Text + "','InwardList')", connstr);
                    connstr.Open();
                    cmd.ExecuteNonQuery();
                    connstr.Close();
                }

                txtemp.Text = null;
                txtpwd.Text = null;
                txtuser.Text = null;
                droptype.SelectedValue = null;
                dropbranch.SelectedValue = null;
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            SqlCommand cmd = new SqlCommand("select * from Login", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if(dt.Rows.Count>0)
            {
                GridLogin.DataSource = dt;
                GridLogin.DataBind();
            }
            else
            {
                dt.Rows.Add(dt.NewRow());
                GridLogin.DataSource = dt;
                GridLogin.DataBind();
                int columncount = GridLogin.Rows[0].Cells.Count;
                GridLogin.Rows[0].Cells.Clear();
                GridLogin.Rows[0].Cells.Add(new TableCell());
                GridLogin.Rows[0].Cells[0].ColumnSpan = columncount;
                GridLogin.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void delete_Click(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Delete from Login where id='" + id + "'", connstr);
            connstr.Open();
            cmd.ExecuteNonQuery();
            connstr.Close();
            BindGrid();
        }

        protected void Edit_Click(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text= btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Select * from Login where id='" + lblid.Text + "'", connstr);
            connstr.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                txtemployee.Text = dr["EmployeeName"].ToString();
                txtuname.Text = dr["Name"].ToString();
                txtpassword.Text = dr["Password"].ToString();
                dropemptype.SelectedItem.Text= dr["Role"].ToString();
                dropempbranch.SelectedItem.Text= dr["Branch"].ToString();
            }
            connstr.Close();
            ModalPopupExtender1.Show();

        }

        protected void Update_Click(object sender,EventArgs e)
        {
            //if(dropemptype.SelectedItem.Text=="Admin")
            //{
            //    SqlCommand cmd = new SqlCommand("Update Login set EmployeeName='" + txtemployee.Text + "',Name='" + txtuname.Text + "',Password='" + txtpassword.Text + "',Role='" + dropemptype.SelectedItem.Text + "',Branch='" + dropempbranch.SelectedItem.Text + "',DashBoard='ItemsAdd' where id='" + lblid.Text + "'", connstr);
            //    connstr.Open();
            //    cmd.ExecuteNonQuery();
            //    connstr.Close();
            //}
           /* else*/ if(dropemptype.SelectedItem.Text == "Branch Employee")
            {
                SqlCommand cmd = new SqlCommand("Update Login set EmployeeName='" + txtemployee.Text + "',Name='" + txtuname.Text + "',Password='" + txtpassword.Text + "',Role='" + dropemptype.SelectedItem.Text + "',Branch='" + dropempbranch.SelectedItem.Text + "',DashBoard='BranchBilling' where id='" + lblid.Text + "'", connstr);
                connstr.Open();
                cmd.ExecuteNonQuery();
                connstr.Close();
            }
            else if (dropemptype.SelectedItem.Text == "Super Admin")
            {
                SqlCommand cmd = new SqlCommand("Update Login set EmployeeName='" + txtemployee.Text + "',Name='" + txtuname.Text + "',Password='" + txtpassword.Text + "',Role='" + dropemptype.SelectedItem.Text + "',Branch='" + dropempbranch.SelectedItem.Text + "',DashBoard='InwardList' where id='" + lblid.Text + "'", connstr);
                connstr.Open();
                cmd.ExecuteNonQuery();
                connstr.Close();
            }
            BindGrid();
        }
    }
}