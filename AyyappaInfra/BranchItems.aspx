﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Branch.Master" AutoEventWireup="true" CodeBehind="BranchItems.aspx.cs" Inherits="AyyappaInfra.BranchItems" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Available Items
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>

                                
                            <div class="col-md-3">
                                <div class="form-group">
                                  Search by Category <asp:DropDownList ID="dropcategory" CssClass="form-control" runat="server" OnSelectedIndexChanged="dropcateory_Change" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                                    </ContentTemplate>
                            </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                            <div class="col-md-12">
                                <asp:GridView ID="griditems" CssClass="table table-respnsive table-striped" GridLines="None" runat="server" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="Brand" HeaderText="Brand" />
                                         <asp:BoundField DataField="Product" HeaderText="Product" />
                                         <asp:BoundField DataField="Category" HeaderText="Category" />
                                         <asp:BoundField DataField="Size" HeaderText="Size" />
                                         <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                                        <asp:BoundField DataField="QuR2" HeaderText="Quantity" />
                                        <asp:BoundField DataField="QuR12" HeaderText="Quantity" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
