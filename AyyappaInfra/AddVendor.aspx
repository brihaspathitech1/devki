﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddVendor.aspx.cs" Inherits="AyyappaInfra.AddVendor" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Add Vendor
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                             <div class="col-md-3">
                                <div class="form-group">
                            Name<asp:TextBox ID="txtname" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtname" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                 </div>
                             <div class="col-md-3">
                                <div class="form-group">
                            Mobile No<asp:TextBox ID="txtmobile" CssClass="form-control" runat="server" MaxLength="10"></asp:TextBox>
                                    <label style="color:red">10 Characters are required</label>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtmobile" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtmobile" FilterType="Numbers" />
                                  
                                    </div>
                                 </div>
                             <div class="col-md-3">
                                <div class="form-group">
                            Email Id<asp:TextBox ID="txtemail" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                 </div>
                             <div class="col-md-3">
                                <div class="form-group">

                            Address<asp:TextBox ID="txtaddress" CssClass="form-control" runat="server" TextMode="MultiLine" Height="50px" Width="250px"></asp:TextBox>
                                     
                                    </div>
                                 </div>
                            <div class="clearfix"></div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    GST Details<asp:TextBox ID="txtgst" CssClass="form-control" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtgst" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                 </div>
                            <div class="col-md-3" style="padding-top:18px">
                                <div class="form-group">
                            <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" CausesValidation="true" OnClick="Submit" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
