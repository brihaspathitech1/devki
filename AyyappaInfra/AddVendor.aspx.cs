﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace AyyappaInfra
{
    public partial class AddVendor : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //Inseting into vendor table
        protected void Submit(object sender,EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Vendor where mobile='" + txtmobile.Text + "'";
                    cmd.Connection = conn;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if(dt.Rows.Count>0)
                    {
                        string message = "alert('Duplicate Entry...!')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                    else
                    {
                        
                        if (txtname.Text == "" || txtmobile.Text == "")
                        {
                            string message = "alert('Fill all the values...!')";
                            ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                           
                        }
                        else
                        {
                            SqlCommand cmd2 = new SqlCommand("Insert into Vendor(Name,mobile,email,address,GSTDetails) values('" + txtname.Text + "','" + txtmobile.Text + "','" + txtemail.Text + "','" + txtaddress.Text + "','"+txtgst.Text+"')", conn);
                            conn.Open();
                            cmd2.ExecuteNonQuery();
                            conn.Close();
                            Response.Redirect("~/VendorsList.aspx");
                        }
                    }
                }
            }
           
        }
    }
}