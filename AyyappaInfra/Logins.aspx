﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Logins.aspx.cs" Inherits="AyyappaInfra.Logins" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Logins
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                             <div class="col-md-3">
                                <div class="form-group">
                            Employee Name <asp:TextBox ID="txtemp" CssClass="form-control" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtemp"  ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                 </div>
                             <div class="col-md-3">
                                <div class="form-group">
                            Type<asp:DropDownList ID="droptype" CssClass="form-control" runat="server">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="2">Super Admin</asp:ListItem>
                               <%-- <asp:ListItem Value="0">Admin</asp:ListItem>--%>
                                <asp:ListItem Value="1">Branch Employee</asp:ListItem>
                                </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="droptype"  ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                 </div>
                             <div class="col-md-3">
                                <div class="form-group">
                            Branch<asp:DropDownList ID="dropbranch" CssClass="form-control" runat="server">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="0">Road 2 Banjara Hills</asp:ListItem>
                                <asp:ListItem Value="1">Road 7 Banjara Hills</asp:ListItem>
                                <asp:ListItem Value="2">Road 12 Banjara Hills</asp:ListItem>
                                  </asp:DropDownList>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="dropbranch"  ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                 </div>
                             <div class="col-md-3">
                                <div class="form-group">
                            User Name<asp:TextBox ID="txtuser" CssClass="form-control" runat="server"></asp:TextBox>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtuser"  ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                 </div>
                             <div class="col-md-3">
                                <div class="form-group">
                            Password <asp:TextBox ID="txtpwd" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="txtpwd"  ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                    </div>
                                
                             <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" />
                                    </div>
                                 </div>
                            <div class="col-md-12">
                            <asp:GridView ID="GridLogin" AutoGenerateColumns="false" CssClass="table table-respnsive table-striped" GridLines="None" runat="server">
                                <Columns>
                                    <asp:BoundField DataField="EmployeeName" HeaderText="Name" />
                                    <asp:BoundField DataField="Role" HeaderText="Role" />
                                    <asp:BoundField DataField="Branch" HeaderText="Branch" />
                                    <asp:BoundField DataField="Name" HeaderText="User Name" />
                                    <asp:BoundField DataField="Password" HeaderText="Password" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" CssClass="btn btn-primary" runat="server" Text="Edit" CommandArgument='<%# Eval("Id") %>' OnClick="Edit_Click" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btndelete" CssClass="btn btn-danger" runat="server" Text="Delete" OnClientClick="if ( !confirm('Are you sure you want to delete this Login?')) return false;" CommandArgument='<%# Eval("Id") %>' OnClick="delete_Click" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                </div>

                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1" CancelControlID="btncancel" TargetControlID="HiddenField1"></cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="300px" Width="650px">
                                  <h3 class="text-center">Update Login Details</h3>
                                <hr />
                                <div class="col-md-6">
                                <div class="form-group">
                            Employee Name <asp:TextBox ID="txtemployee" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                 </div>
                             <div class="col-md-6">
                                <div class="form-group">
                            Type<asp:DropDownList ID="dropemptype" CssClass="form-control" runat="server">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="2">Super Admin</asp:ListItem>
                                <%--<asp:ListItem Value="0">Admin</asp:ListItem>--%>
                                <asp:ListItem Value="1">Branch Employee</asp:ListItem>
                                </asp:DropDownList>
                                    </div>
                                 </div>
                             <div class="col-md-6">
                                <div class="form-group">
                            Branch<asp:DropDownList ID="dropempbranch" CssClass="form-control" runat="server">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="0">Road 2 Banjara Hills</asp:ListItem>
                                <asp:ListItem Value="1">Road 7 Banjara Hills</asp:ListItem>
                                <asp:ListItem Value="2">Road 12 Banjara Hills</asp:ListItem>
                                  </asp:DropDownList>
                                    </div>
                                 </div>
                             <div class="col-md-6">
                                <div class="form-group">
                            User Name<asp:TextBox ID="txtuname" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                 </div>
                             <div class="col-md-6">
                                <div class="form-group">
                            Password <asp:TextBox ID="txtpassword" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                 </div>
                                <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:Button ID="btnupdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="Update_Click" />
                                    </div>
                                    </div>
                                <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                 <asp:Button ID="btncancel" CssClass="btn btn-danger" runat="server" Text="Cancel" />
                                    </div>
                                    </div>
                            </asp:Panel>
                            <asp:Label ID="lblid" runat="server" Text="" Visible="false"></asp:Label>
                            </div>
                    </div>
                        </div>
                    </div>
                
            </section>
        </div>
</asp:Content>
