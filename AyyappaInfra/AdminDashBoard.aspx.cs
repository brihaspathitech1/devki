﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class AdminDashBoard : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bindcash();
                Bindcard();
                BindCheque();
                BindTotal();
            }
        }

        protected void Bindcash()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select SUM(isnull(CONVERT(float,Paidamt),0)) as cash from Payment p inner join login l on l.EmployeeName=p.username where l.Branch='" + Session["Branch"].ToString() + "' and p.status='through cash'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lblcash.Text = dr["cash"].ToString();
            }
            if(lblcash.Text=="")
            {
                lblcash.Text = "0";
            }
            conn.Close();
        }

        protected void Bindcard()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select SUM(isnull(CONVERT(float,Paidamt),0)) as card from Payment p inner join login l on l.EmployeeName=p.username where l.Branch='" + Session["Branch"].ToString() + "' and p.status='through Card'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lblcard.Text = dr["card"].ToString();
            }
            if(lblcard.Text=="")
            {
                lblcard.Text = "0";
            }
            conn.Close();
        }

        protected void BindCheque()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select SUM(isnull(CONVERT(float,Paidamt),0)) as cheque from Payment p inner join login l on l.EmployeeName=p.username where l.Branch='" + Session["Branch"].ToString() + "' and p.status='through cheque'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lblcheque.Text = dr["cheque"].ToString();
            }
            if (lblcheque.Text == "")
            {
                lblcheque.Text = "0";
            }
            conn.Close();
        }

        protected void BindTotal()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select SUM(isnull(CONVERT(float,Paidamt),0)) as total from Payment p inner join login l on l.EmployeeName=p.username where l.Branch='" + Session["Branch"].ToString() + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lbltotal.Text = dr["total"].ToString();
            }
            if (lblcheque.Text == "")
            {
                lbltotal.Text = "0";
            }
            conn.Close();
        }
    }
}