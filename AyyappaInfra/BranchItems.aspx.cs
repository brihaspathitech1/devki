﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class BranchItems : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCategory();
                BindGrid();
            }
        }

        protected void BindCategory()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    // cmd.CommandText = "Select * from Category where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "'";
                    cmd.CommandText = "Select * from Category order by Category asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropcategory.DataSource = dr;
                    dropcategory.DataTextField = "Category";
                    dropcategory.DataValueField = "Id";
                    dropcategory.DataBind();
                    dropcategory.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //Binding Items from AddItems table
        protected void BindGrid()
        {
            if (Session["Branch"].ToString() == "Road 2 Banjara Hills")
            {
                SqlConnection conn = new SqlConnection(connstr);
                SqlCommand cmd = new SqlCommand("Select * from AddItem where QuR2 is not null order by Product", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    griditems.Columns[4].Visible = false;
                    griditems.Columns[6].Visible = false;
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    int columncount = griditems.Rows[0].Cells.Count;
                    griditems.Rows[0].Cells.Clear();
                    griditems.Rows[0].Cells.Add(new TableCell());
                    griditems.Rows[0].Cells[0].ColumnSpan = columncount;
                    griditems.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
            else if (Session["Branch"].ToString() == "Road 7 Banjara Hills")
            {
                SqlConnection conn = new SqlConnection(connstr);
                SqlCommand cmd = new SqlCommand("Select * from AddItem order by Product", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    griditems.Columns[5].Visible = false;
                    griditems.Columns[6].Visible = false;
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    int columncount = griditems.Rows[0].Cells.Count;
                    griditems.Rows[0].Cells.Clear();
                    griditems.Rows[0].Cells.Add(new TableCell());
                    griditems.Rows[0].Cells[0].ColumnSpan = columncount;
                    griditems.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
            else if (Session["Branch"].ToString() == "Road 12 Banjara Hills")
            {
                SqlConnection conn = new SqlConnection(connstr);
                SqlCommand cmd = new SqlCommand("Select * from AddItem where QuR12 is not null order by Product", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    griditems.Columns[4].Visible = false;
                    griditems.Columns[5].Visible = false;
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    int columncount = griditems.Rows[0].Cells.Count;
                    griditems.Rows[0].Cells.Clear();
                    griditems.Rows[0].Cells.Add(new TableCell());
                    griditems.Rows[0].Cells[0].ColumnSpan = columncount;
                    griditems.Rows[0].Cells[0].Text = "No Records Found";
                }
            }

        }

        protected void dropcateory_Change(object sender,EventArgs e)
        {
            if (Session["Branch"].ToString() == "Road 2 Banjara Hills")
            {
                SqlConnection conn = new SqlConnection(connstr);
                SqlCommand cmd = new SqlCommand("Select * from AddItem where category='"+dropcategory.SelectedItem.Text+"' and  QuR2 is not null order by Product", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    griditems.Columns[4].Visible = false;
                    griditems.Columns[6].Visible = false;
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    int columncount = griditems.Rows[0].Cells.Count;
                    griditems.Rows[0].Cells.Clear();
                    griditems.Rows[0].Cells.Add(new TableCell());
                    griditems.Rows[0].Cells[0].ColumnSpan = columncount;
                    griditems.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
            else if (Session["Branch"].ToString() == "Road 7 Banjara Hills")
            {
                SqlConnection conn = new SqlConnection(connstr);
                SqlCommand cmd = new SqlCommand("Select * from AddItem category='" + dropcategory.SelectedItem.Text + "' order by Product", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    griditems.Columns[5].Visible = false;
                    griditems.Columns[6].Visible = false;
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    int columncount = griditems.Rows[0].Cells.Count;
                    griditems.Rows[0].Cells.Clear();
                    griditems.Rows[0].Cells.Add(new TableCell());
                    griditems.Rows[0].Cells[0].ColumnSpan = columncount;
                    griditems.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
            else if (Session["Branch"].ToString() == "Road 12 Banjara Hills")
            {
                SqlConnection conn = new SqlConnection(connstr);
                SqlCommand cmd = new SqlCommand("Select * from AddItem where category='" + dropcategory.SelectedItem.Text + "' and QuR12 is not null order by Product", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    griditems.Columns[4].Visible = false;
                    griditems.Columns[5].Visible = false;
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    griditems.DataSource = ds;
                    griditems.DataBind();
                    int columncount = griditems.Rows[0].Cells.Count;
                    griditems.Rows[0].Cells.Clear();
                    griditems.Rows[0].Cells.Add(new TableCell());
                    griditems.Rows[0].Cells[0].ColumnSpan = columncount;
                    griditems.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }
    }
}