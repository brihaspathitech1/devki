﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Branch.Master" AutoEventWireup="true" CodeBehind="BranchTotalAmount.aspx.cs" Inherits="AyyappaInfra.BranchTotalAmount" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Total Cash
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                     <div class="col-md-3">
                                <div class="form-group">
                                    Start Date
                                    <asp:TextBox ID="txtstart" runat="server" CssClass="form-control"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtstart" />
                                    </div>
                                         </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                     <div class="col-md-3">
                                <div class="form-group">
                                    End Date
                                    <asp:TextBox ID="txtend" runat="server" CssClass="form-control"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtend" />
                                    </div>
                                         </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>

                            <%--<asp:UpdatePanel ID="update" runat="server">
                                <ContentTemplate>

                               
                            <div class="col-md-3">
                                <div class="form-group">
                                 Search by Branch   <asp:DropDownList ID="dropaddress" CssClass="form-control"  runat="server">
                                     <asp:ListItem></asp:ListItem>
                                     <asp:ListItem>Road 2 Banjara Hills</asp:ListItem>
                                     <asp:ListItem>Road 7 Banjara Hills</asp:ListItem>
                                     <asp:ListItem>Road 12 Banjara Hills</asp:ListItem>
                                                    </asp:DropDownList>
                                </div>
                            </div>
                                     </ContentTemplate>
                            </asp:UpdatePanel>--%>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                     <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="Submit" />
                                    </div>
                                         </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel> <%--OnSelectedIndexChanged="Branch_Change" AutoPostBack="true"--%>

                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" ShowFooter="true" CssClass="table table-respnsive table-striped" GridLines="None" OnRowDataBound="Databound">
                                    <Columns>
                                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:TemplateField HeaderText="Mobile">
                                            <ItemTemplate>
                                                <%# Eval("Mobile")+"-"+Eval("Email") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="GstDetails" HeaderText="GST No" />
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lblgst" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblval" runat="server" Text="Total Amount" Font-Bold="true"/>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltotal" runat="server" Text='<%# Eval("Totalamt") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblgrandtotal" runat="server" Text="Total Amount" ForeColor="Blue" Font-Bold="true"/>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Paid Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpaid" runat="server" Text='<%# Eval("Paidamt") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblpaidtotal" runat="server" Text="Total Amount" ForeColor="Green" Font-Bold="true"/>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remaining Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblremain" runat="server" Text='<%# Eval("remainamt") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="remaintotal" runat="server" Text="Total Amount" ForeColor="Red" Font-Bold="true"/>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                              <%--<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>--%>
                                      <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="btnexcel" runat="server" Text="Export to Excel" CssClass="btn btn-primary" OnClick="Excel_Click" />
                                    </div>
                                          </div>
                                   <%-- </ContentTemplate>
                                  </asp:UpdatePanel>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
