﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class VendorsList : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from Vendor order by Id desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                GridVendor.DataSource = ds;
                GridVendor.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridVendor.DataSource = ds;
                GridVendor.DataBind();
                int columncount = GridVendor.Rows[0].Cells.Count;
                GridVendor.Rows[0].Cells.Clear();
                GridVendor.Rows[0].Cells.Add(new TableCell());
                GridVendor.Rows[0].Cells[0].ColumnSpan = columncount;
                GridVendor.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        //Searching Vendor name
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVendor(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DevkiInterior"].ToString());
            SqlDataAdapter adp = new SqlDataAdapter(" SELECT Name FROM Vendor where Name like'" + prefixText + "%'", con);


            DataTable dt = new DataTable();
            adp.Fill(dt);
            List<string> Mobilenumber = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Mobilenumber.Add(dt.Rows[i]["Name"].ToString());
            }
            return Mobilenumber;
        }

        //Binding Grid Based on vendor name
        protected void Name_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from Vendor where Name='" + txtvendor.Text+"' order by Id desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridVendor.DataSource = ds;
                GridVendor.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridVendor.DataSource = ds;
                GridVendor.DataBind();
                int columncount = GridVendor.Rows[0].Cells.Count;
                GridVendor.Rows[0].Cells.Clear();
                GridVendor.Rows[0].Cells.Add(new TableCell());
                GridVendor.Rows[0].Cells[0].ColumnSpan = columncount;
                GridVendor.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Edit(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from Vendor where id='" + lblid.Text + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                txtname.Text = dr["Name"].ToString();
                txtmobile.Text = dr["mobile"].ToString();
                txtemail.Text = dr["Email"].ToString();
                txtaddress.Text = dr["Address"].ToString();
                txtgst.Text = dr["GstDetails"].ToString();
            }
            conn.Close();
            ModalPopupExtender1.Show();
        }

        //Updating Vendor details
        protected void Update(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Update Vendor set Name='" + txtname.Text + "',mobile='" + txtmobile.Text + "',Email='" + txtemail.Text + "',Address='" + txtaddress.Text + "',GstDetails='"+txtgst.Text+"' where Id='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid();
        }

        //Deleting a vendor

        protected void delete(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Delete from Vendor where Id='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid();
        }
    }
}