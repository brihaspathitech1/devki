﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="BranchWiseItems.aspx.cs" Inherits="AyyappaInfra.BranchWiseItems" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Branch Wise Item's List
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            <div class="col-md-4">
                                <div class="form-group">
                               Search by Category<asp:DropDownList ID="dropcategory" CssClass="form-control" OnSelectedIndexChanged="Category_Change" AutoPostBack="true" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                             <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:GridView ID="GridItems" AutoGenerateColumns="false" CssClass="table table-respnsive table-striped" GridLines="None" runat="server">
                                                <Columns>
                                                    <asp:BoundField DataField="Brand" HeaderText="Brand" />
                                                    <asp:BoundField DataField="Product" HeaderText="Product" />
                                                    <asp:BoundField DataField="Category" HeaderText="Category" />
                                                    <asp:BoundField DataField="Size" HeaderText="Size" />
                                                    <asp:BoundField DataField="Quantity" HeaderText="R-7 Qty" ItemStyle-ForeColor="Blue" ItemStyle-Font-Bold="true" />
                                                    <asp:BoundField DataField="QuR2" HeaderText="R-2 Qty" ItemStyle-ForeColor="#009933" ItemStyle-Font-Bold="true" />
                                                    <asp:BoundField DataField="QuR12" HeaderText="R-12 Qty" ItemStyle-ForeColor="#ff3399" ItemStyle-Font-Bold="true" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnedit" runat="server" Text="Edit" CssClass="btn btn-success btn-xs" CommandArgument='<%# Eval("Id") %>' OnClick="btnedit_Click" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            </div>
                                 </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" CancelControlID="btnclose" PopupControlID="Panel1"></ajaxToolkit:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="300px" Width="800px">
                                <h3 style="text-align:center">Update Quantity</h3>
                                <hr />
                                 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Brand <asp:TextBox ID="txtbrand" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Product <asp:TextBox ID="txtproduct" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Category <asp:TextBox ID="txtcategory" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Size <asp:TextBox ID="txtsize" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                        R-7 Qty <asp:TextBox ID="txt7qty" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                     </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        R-2 Qty <asp:TextBox ID="txt2qty" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                     </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        R-12 Qty <asp:TextBox ID="txt12qty" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                     </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                         <div class="clearfix"></div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                        <asp:Button ID="btnupdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="btnUpdate" />
                                        </div>
                                     </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:Button ID="btnclose" runat="server" CssClass="btn btn-danger" Text="Close" />
                                        </div>
                                    </div>
                            </asp:Panel>

                            <asp:Label ID="lblid" Visible="false" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
