﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AyyappaInfra
{
    public partial class BranchCustomer : System.Web.UI.Page
    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["DevkiInterior"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVendor(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DevkiInterior"].ToString());
            SqlDataAdapter adp = new SqlDataAdapter("SELECT Name FROM Customer where Name like'" + prefixText + "%'", con);


            DataTable dt = new DataTable();
            adp.Fill(dt);
            List<string> Mobilenumber = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Mobilenumber.Add(dt.Rows[i]["Name"].ToString());
            }
            return Mobilenumber;
        }

        protected void BindGrid()
        {
            SqlCommand cmd = new SqlCommand(" With cte as (select Date,CustomerName,Mobile,Billno,Bno,Total,ROW_NUMBER() over (partition by billno order by billno desc) as rn from Billing where username='" + Session["Empname"].ToString() + "') select * from cte where rn=1 order by bno desc", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Customer_Change(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand(" With cte as (select Date,CustomerName,Mobile,Billno,Bno,Total,ROW_NUMBER() over (partition by billno order by billno desc) as rn from Billing where customername='" + txtcust.Text + "' and username='" + Session["Empname"].ToString() + "') select * from cte where rn=1 order by bno desc", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void View_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string billno = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Select Brand,product,category,size,quantity,Price,amount,Tax,Taxamount,Subtotal,Total,SUM(convert(float,amount)+convert(float,Taxamount)) as totalamt from Billing where Billno='" + billno.ToString() + "' group by Brand,product,category,size,quantity,Price,amount,Tax,Subtotal,Total,Taxamount", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblsubtotal.Text = ds.Tables[0].Rows[0]["Subtotal"].ToString();
                lbltotal.Text = ds.Tables[0].Rows[0]["Total"].ToString();
                Gridpopup.DataSource = ds;
                Gridpopup.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                Gridpopup.DataSource = ds;
                Gridpopup.DataBind();
                int columncount = Gridpopup.Rows[0].Cells.Count;
                Gridpopup.Rows[0].Cells.Clear();
                Gridpopup.Rows[0].Cells.Add(new TableCell());
                Gridpopup.Rows[0].Cells[0].ColumnSpan = columncount;
                Gridpopup.Rows[0].Cells[0].Text = "No Records Found";
            }
            ModalPopupExtender1.Show();
        }

        protected void Print_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string billno = btn.CommandArgument;

            Session["Bill"] = billno.ToString();
            Response.Redirect("~/ViewBill.aspx");
        }
    }
}