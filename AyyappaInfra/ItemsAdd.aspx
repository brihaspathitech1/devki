﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin2.Master" AutoEventWireup="true" CodeBehind="ItemsAdd.aspx.cs" Inherits="AyyappaInfra.ItemsAdd" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Add Items
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <asp:UpdatePanel ID="Update" runat="server">
                                <ContentTemplate>                 
                            <div class="col-md-3">
                                <div class="form-group">
                                    Brand:<asp:DropDownList ID="dropBrand" CssClass="form-control" OnSelectedIndexChanged="Brand_Change" AutoPostBack="true" runat="server"></asp:DropDownList>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="dropBrand" InitialValue="0" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                                     </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            <div class="col-md-1" style="padding-top: 23px">
                                <div class="form-group">
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal1">+</button>
                                </div>
                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="form-group">
                                    Product:<asp:DropDownList ID="dropproduct" CssClass="form-control" OnSelectedIndexChanged="Product_Change" AutoPostBack="true" runat="server"></asp:DropDownList>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="dropproduct" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                            <div class="col-md-1" style="padding-top: 23px">
                                <div class="form-group">
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal2">+</button>
                                </div>
                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="form-group">
                                    Category:<asp:DropDownList ID="dropcategory" CssClass="form-control" OnSelectedIndexChanged="Category_Change" AutoPostBack="true" runat="server"></asp:DropDownList>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="dropcategory" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                            <div class="col-md-1" style="padding-top: 23px">
                                <div class="form-group">
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal3">+</button>
                                </div>
                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="form-group">
                                    Size:<asp:DropDownList ID="dropsize" CssClass="form-control" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="dropsize" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                            <div class="col-md-1" style="padding-top: 23px">
                                <div class="form-group">
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal4">+</button>
                                </div>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                            <div class="col-md-3" style="padding-top: 18px">
                                <div class="form-group">
                                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="submit_Click" />
                                </div>
                            </div>
                            <div class="modal fade" id="myModal1" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Brand</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                <asp:TextBox ID="txtBrand" runat="server" class="form-control" />
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="producpopup" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Brand_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal2" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Product</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                <asp:TextBox ID="txtproduct" runat="server" class="form-control" />
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="Button1" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Product_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal3" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Category</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                <asp:TextBox ID="txtcategory" runat="server" class="form-control" />
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="Button2" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Category_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal4" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Size</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                <asp:TextBox ID="txtsize" runat="server" class="form-control" />
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="Button3" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Size_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
