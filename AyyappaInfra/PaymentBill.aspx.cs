﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class PaymentBill : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindList();
                BindAddress();
            }
        }

        protected void BindList()
         {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select c.name,p.billno,p.username,Convert(varchar(10),GETDATE(),103) as date from Payment p inner join Customer c on c.Id=p.CustomerID where p.Billno='"+ Session["Bill"].ToString() + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                lbluser.Text = ds.Tables[0].Rows[0]["username"].ToString();
                DataList1.DataSource = ds;
                DataList1.DataBind();

                DataList2.DataSource = ds;
                DataList2.DataBind();

                DataList3.DataSource = ds;
                DataList3.DataBind();
            }
        }
        private void BindGrid(GridView GridView1, String Billno)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select p.totalamt,p.paidamt,p.remainamt,c.name from Payment p inner join Customer c on c.Id=p.CustomerID where p.Billno='" + Session["Bill"].ToString() + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {


                GridView1.DataSource = ds;
                GridView1.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "నో రికార్డ్స్ ఫౌండ్ ";
            }

        }

        private void BindGrid2(GridView GridView2, String Billno)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select p.totalamt,p.paidamt,p.remainamt,c.name from Payment p inner join Customer c on c.Id=p.CustomerID where p.Billno='" + Session["Bill"].ToString() + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {


                GridView2.DataSource = ds;
                GridView2.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView2.DataSource = ds;
                GridView2.DataBind();
                int columncount = GridView2.Rows[0].Cells.Count;
                GridView2.Rows[0].Cells.Clear();
                GridView2.Rows[0].Cells.Add(new TableCell());
                GridView2.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView2.Rows[0].Cells[0].Text = "నో రికార్డ్స్ ఫౌండ్ ";
            }

        }

        private void BindGrid3(GridView GridView3, String Billno)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select p.totalamt,p.paidamt,p.remainamt,c.name from Payment p inner join Customer c on c.Id=p.CustomerID where p.Billno='" + Session["Bill"].ToString() + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {


                GridView3.DataSource = ds;
                GridView3.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView3.DataSource = ds;
                GridView3.DataBind();
                int columncount = GridView3.Rows[0].Cells.Count;
                GridView3.Rows[0].Cells.Clear();
                GridView3.Rows[0].Cells.Add(new TableCell());
                GridView3.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView3.Rows[0].Cells[0].Text = "నో రికార్డ్స్ ఫౌండ్ ";
            }

        }
        protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
        {

            GridView GridView1 = (GridView)e.Item.FindControl("GridView1");
            BindGrid(GridView1, DataList1.DataKeys[e.Item.ItemIndex].ToString());
        }

        protected void DataList2_ItemDataBound(object sender, DataListItemEventArgs e)
        {

            GridView GridView2 = (GridView)e.Item.FindControl("GridView2");
            BindGrid2(GridView2, DataList2.DataKeys[e.Item.ItemIndex].ToString());
        }

        protected void DataList3_ItemDataBound(object sender, DataListItemEventArgs e)
        {

            GridView GridView3 = (GridView)e.Item.FindControl("GridView3");
            BindGrid2(GridView3, DataList3.DataKeys[e.Item.ItemIndex].ToString());
        }

        protected void BindAddress()
        {
            string branch = string.Empty;
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select Branch from Login Where EmployeeName='" + lbluser.Text + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                branch = dr["Branch"].ToString();
            }
            if(branch == "Road 12 Banjara Hills")
            {
                DataList1.Visible = true;
            }
            else if (branch == "Road 7 Banjara Hills")
            {
                DataList2.Visible = true;
            }
            else if (branch == "Road 2 Banjara Hills")
            {
                DataList3.Visible = true;
            }
        }

    }
}