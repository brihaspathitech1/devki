﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class ItemsBilling : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindBrand();
                bindcategory();
                BindSize();
            }
            if (!IsPostBack)
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[12] { new DataColumn("Date"), new DataColumn("cname"), new DataColumn("Brand"), new DataColumn("Product"), new DataColumn("Category"), new DataColumn("size"), new DataColumn("qty"), new DataColumn("price"), new DataColumn("Tax"), new DataColumn("stotal"), new DataColumn("Tamount"), new DataColumn("Ttotal") });
                ViewState["Outward"] = dt;
                this.BindGrid();
            }
        }

        protected void BindGrid()
        {
            GridView1.DataSource = (DataTable)ViewState["Outward"];
            GridView1.DataBind();
        }
        protected void Customer_Change(object sender, EventArgs e)
        {
            if (radiocustomer.SelectedValue == "0")
            {
                div1.Visible = true;
                div2.Visible = false;
            }
            else
            {
                div2.Visible = true;
                div1.Visible = false;
            }
        }

        //Binding brand

        protected void BindBrand()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select Id,Brand from Brand order by Brand asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropbrand.DataSource = dr;
                    dropbrand.DataTextField = "Brand";
                    dropbrand.DataValueField = "Id";
                    dropbrand.DataBind();
                    dropbrand.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //Binding Product

        protected void Brand_Change(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from Product where Brand='" + dropbrand.SelectedValue + "' order by Product asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropproduct.DataSource = dr;
                    dropproduct.DataTextField = "Product";
                    dropproduct.DataValueField = "Id";
                    dropproduct.DataBind();
                    dropproduct.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        protected void bindcategory()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Category order by Category asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropcategory.DataSource = dr;
                    dropcategory.DataTextField = "Category";
                    dropcategory.DataValueField = "Id";
                    dropcategory.DataBind();
                    dropcategory.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        //Binding Category
        protected void Prodcut_Change(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Category order by Category asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropcategory.DataSource = dr;
                    dropcategory.DataTextField = "Category";
                    dropcategory.DataValueField = "Id";
                    dropcategory.DataBind();
                    dropcategory.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        protected void BindSize()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Size order by Size asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropsize.DataSource = dr;
                    dropsize.DataTextField = "Size";
                    dropsize.DataValueField = "Id";
                    dropsize.DataBind();
                    dropsize.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        //Binding Size

        protected void Category_Change(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //cmd.CommandText = "Select * from Size where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "' and Category='" + dropcategory.SelectedValue + "'";
                    cmd.CommandText = "Select * from Size order by Size asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropsize.DataSource = dr;
                    dropsize.DataTextField = "Size";
                    dropsize.DataValueField = "Id";
                    dropsize.DataBind();
                    dropsize.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //Binding quantity
        protected void Size_Change(object sender, EventArgs e)
        {
            if (Session["Branch"].ToString() == "Road 2 Banjara Hills")
            {
                using (SqlConnection conn = new SqlConnection(connstr))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "Select QuR2 from AddItem where Brand='" + dropbrand.SelectedItem.Text + "' and Product='" + dropproduct.SelectedItem.Text + "' and Category='" + dropcategory.SelectedItem.Text + "' and Size='" + dropsize.SelectedItem.Text + "'";
                        cmd.Connection = conn;
                        conn.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr.Read())
                        {
                            txtavail.Text = dr["QuR2"].ToString();
                        }
                        if (txtavail.Text == "")
                        {
                            txtavail.Text = "0";
                        }
                        conn.Close();
                    }
                }
            }
            else if (Session["Branch"].ToString() == "Road 7 Banjara Hills")
            {
                using (SqlConnection conn = new SqlConnection(connstr))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "Select Quantity from AddItem where Brand='" + dropbrand.SelectedItem.Text + "' and Product='" + dropproduct.SelectedItem.Text + "' and Category='" + dropcategory.SelectedItem.Text + "' and Size='" + dropsize.SelectedItem.Text + "'";
                        cmd.Connection = conn;
                        conn.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr.Read())
                        {
                            txtavail.Text = dr["Quantity"].ToString();
                        }
                        if (txtavail.Text == "")
                        {
                            txtavail.Text = "0";
                        }
                        conn.Close();
                    }
                }
            }
            else if (Session["Branch"].ToString() == "Road 12 Banjara Hills")
            {
                using (SqlConnection conn = new SqlConnection(connstr))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "Select QuR12 from AddItem where Brand='" + dropbrand.SelectedItem.Text + "' and Product='" + dropproduct.SelectedItem.Text + "' and Category='" + dropcategory.SelectedItem.Text + "' and Size='" + dropsize.SelectedItem.Text + "'";
                        cmd.Connection = conn;
                        conn.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr.Read())
                        {
                            txtavail.Text = dr["QuR12"].ToString();
                        }
                        if (txtavail.Text == "")
                        {
                            txtavail.Text = "0";
                        }
                        conn.Close();
                    }
                }
            }

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetMobileNo(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DevkiInterior"].ToString());
            SqlDataAdapter adp = new SqlDataAdapter(" SELECT Name FROM Customer where Name like'" + prefixText + "%'", con);


            DataTable dt = new DataTable();
            adp.Fill(dt);
            List<string> Mobilenumber = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Mobilenumber.Add(dt.Rows[i]["Name"].ToString());
            }
            return Mobilenumber;
        }

        //Binding Customer Details

        protected void Mobile_Change(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select Id,Mobile from Customer where Name='" + txtcustomer.Text + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                txtmobile.Text = dr["Mobile"].ToString();
                txtunique.Text = dr["Id"].ToString();
            }
        }

        //binding total amount into text box
        protected void Price_Change(object sender, EventArgs e)
        {
            double value = double.Parse(txtqty.Text) * double.Parse(txtprice.Text);
            txtamount.Text = value.ToString();
        }

        protected void TaxType_Change(object sender, EventArgs e)
        {
            taxdiv.Visible = true;
        }

        //Binding Values into Gridview 
        double stotal = 0, taxamt = 0, total = 0;
        double c9 = 0, s9 = 0;/*c14 = 0, s14 = 0, i18 = 0, i28 = 0, c5 = 0;*/
        protected void Add_Click(object sender, EventArgs e)
        {
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            if (txtqty.Text != "" && txtprice.Text != "")
            {
                if (txtavail.Text == "0")
                {
                    lblmsg.Text = "You Can't Bill because the available quantity is zero";
                    lblmsg.Visible = true;

                }
                else if (int.Parse(txtavail.Text) < int.Parse(txtqty.Text))
                {
                    lblmsg.Text = "You Can't Bill because the available quantity must be greater than Billing Quantity";
                    lblmsg.Visible = true;

                }
                else
                {
                    lblmsg.Visible = false;
                    if (droptaxtype.SelectedItem.Text == "Include Tax")
                    {


                        if (radiocustomer.SelectedValue == "1")
                        {
                            double qty = double.Parse(txtqty.Text);
                            double price = double.Parse(txtprice.Text);
                            string tax = droptax.SelectedItem.Text;
                            if (tax == "GST 5%")
                            {
                                //double sub = qty * price;
                                //double tamt = sub * 0.05;
                                //double tot = sub + tamt;
                                double amt = double.Parse(txtamount.Text);

                                double x = (amt * 100) / 105;

                                double tamt2 = x * 0.05;

                                double y = Math.Round(x, 2);
                                double z = Math.Round(tamt2, 2);

                                double ww = y + z;
                                DataTable dt = (DataTable)ViewState["Outward"];
                                dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                                    txtname.Text.Trim().Replace("&nbsp;", ""),
                                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                                    tax.ToString().Trim().Replace("&nbsp;", ""),
                                    y.ToString().Trim().Replace("&nbsp;", ""),
                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")
                                    );
                                ViewState["Outward"] = dt;
                                this.BindGrid();
                                txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                                dropsize.SelectedValue = null; droptax.SelectedValue = null;
                            }
                            else if (tax == "GST 12%")
                            {

                                //double sub = qty * price;
                                //double tamt = sub * 0.05;
                                //double tot = sub + tamt;
                                double amt = double.Parse(txtamount.Text);

                                double x = (amt * 100) / 112;

                                double tamt2 = x * 0.12;

                                double y = Math.Round(x, 2);
                                double z = Math.Round(tamt2, 2);

                                double ww = y + z;
                                DataTable dt = (DataTable)ViewState["Outward"];
                                dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                                    txtname.Text.Trim().Replace("&nbsp;", ""),
                                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                                    tax.ToString().Trim().Replace("&nbsp;", ""),
                                    y.ToString().Trim().Replace("&nbsp;", ""),
                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")
                                    );
                                ViewState["Outward"] = dt;
                                this.BindGrid();
                                txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                                dropsize.SelectedValue = null; droptax.SelectedValue = null;

                            }
                            else if (tax == "GST 18%")
                            {
                                //double sub = qty * price;
                                //double tamt = sub * 0.18;
                                //double tot = sub + tamt;

                                double amt = double.Parse(txtamount.Text);

                                double x = (amt * 100) / 118;

                                double tamt2 = x * 0.18;

                                double y = Math.Round(x, 2);
                                double z = Math.Round(tamt2, 2);

                                double ww = y + z;
                                //double cs = tamt / 2;
                                //c9 = c9 + cs;
                                //s9 = s9 + cs;
                                //stotal = stotal + sub;
                                //taxamt = taxamt + tot;
                                //lblstotal.Text = stotal.ToString();
                                //lblc9.Text = c9.ToString();
                                //lbls9.Text = s9.ToString();
                                //lbltotal.Text = taxamt.ToString();
                                DataTable dt = (DataTable)ViewState["Outward"];
                                dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                                    txtname.Text.Trim().Replace("&nbsp;", ""),
                                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                                    tax.ToString().Trim().Replace("&nbsp;", ""),
                                    y.ToString().Trim().Replace("&nbsp;", ""),
                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")
                                    );
                                ViewState["Outward"] = dt;
                                this.BindGrid();
                                txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                                dropsize.SelectedValue = null; txtamount.Text = null; droptax.SelectedValue = null;
                            }

                            else if (tax == "GST 28%")
                            {
                                //double sub = qty * price;
                                ////double tamt = sub * 0.18;
                                //double tot = sub + tamt;

                                double amt = double.Parse(txtamount.Text);

                                double x = (amt * 100) / 128;

                                double tamt2 = x * 0.28;

                                double y = Math.Round(x, 2);
                                double z = Math.Round(tamt2, 2);

                                double ww = y + z;
                                DataTable dt = (DataTable)ViewState["Outward"];
                                dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                                    txtname.Text.Trim().Replace("&nbsp;", ""),
                                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                                    tax.ToString().Trim().Replace("&nbsp;", ""),
                                    y.ToString().Trim().Replace("&nbsp;", ""),
                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")
                                    );
                                ViewState["Outward"] = dt;
                                this.BindGrid();
                                txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                                dropsize.SelectedValue = null;
                                droptax.SelectedValue = null;
                            }
                            Bindvalues();
                        }
                        else if (radiocustomer.SelectedValue == "0")
                        {
                            double qty = double.Parse(txtqty.Text);
                            double price = double.Parse(txtprice.Text);
                            string tax = droptax.SelectedItem.Text;
                            if (tax == "GST 5%")
                            {
                                //double sub = qty * price;
                                //double tamt = sub * 0.05;
                                //double tot = sub + tamt;
                                double amt = double.Parse(txtamount.Text);

                                double x = (amt * 100) / 105;

                                double tamt2 = x * 0.05;

                                double y = Math.Round(x, 2);
                                double z = Math.Round(tamt2, 2);

                                double ww = y + z;
                                DataTable dt = (DataTable)ViewState["Outward"];
                                dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                                    txtcustomer.Text.Trim().Replace("&nbsp;", ""),
                                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                                    tax.ToString().Trim().Replace("&nbsp;", ""),
                                    y.ToString().Trim().Replace("&nbsp;", ""),
                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")
                                    );
                                ViewState["Outward"] = dt;
                                this.BindGrid();
                                txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                                dropsize.SelectedValue = null; droptax.SelectedValue = null;
                            }
                            else if (tax == "GST 12%")
                            {

                                //double sub = qty * price;
                                //double tamt = sub * 0.05;
                                //double tot = sub + tamt;
                                double amt = double.Parse(txtamount.Text);

                                double x = (amt * 100) / 112;

                                double tamt2 = x * 0.12;

                                double y = Math.Round(x, 2);
                                double z = Math.Round(tamt2, 2);

                                double ww = y + z;
                                DataTable dt = (DataTable)ViewState["Outward"];
                                dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                                    txtcustomer.Text.Trim().Replace("&nbsp;", ""),
                                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                                    tax.ToString().Trim().Replace("&nbsp;", ""),
                                    y.ToString().Trim().Replace("&nbsp;", ""),
                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")
                                    );
                                ViewState["Outward"] = dt;
                                this.BindGrid();
                                txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                                dropsize.SelectedValue = null; droptax.SelectedValue = null;

                            }
                            else if (tax == "GST 18%")
                            {
                                //double sub = qty * price;
                                //double tamt = sub * 0.18;
                                //double tot = sub + tamt;

                                double amt = double.Parse(txtamount.Text);

                                double x = (amt * 100) / 118;

                                double tamt2 = x * 0.18;

                                double y = Math.Round(x, 2);
                                double z = Math.Round(tamt2, 2);

                                double ww = y + z;
                                //double cs = tamt / 2;
                                //c9 = c9 + cs;
                                //s9 = s9 + cs;
                                //stotal = stotal + sub;
                                //taxamt = taxamt + tot;
                                //lblstotal.Text = stotal.ToString();
                                //lblc9.Text = c9.ToString();
                                //lbls9.Text = s9.ToString();
                                //lbltotal.Text = taxamt.ToString();
                                DataTable dt = (DataTable)ViewState["Outward"];
                                dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                                    txtcustomer.Text.Trim().Replace("&nbsp;", ""),
                                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                                    tax.ToString().Trim().Replace("&nbsp;", ""),
                                    y.ToString().Trim().Replace("&nbsp;", ""),
                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")
                                    );
                                ViewState["Outward"] = dt;
                                this.BindGrid();
                                txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                                dropsize.SelectedValue = null; txtamount.Text = null;
                                droptax.SelectedValue = null;/*droptax.SelectedItem.Text = "";*/
                            }

                            else if (tax == "GST 28%")
                            {
                                //double sub = qty * price;
                                ////double tamt = sub * 0.18;
                                //double tot = sub + tamt;

                                double amt = double.Parse(txtamount.Text);

                                double x = (amt * 100) / 128;

                                double tamt2 = x * 0.28;

                                double y = Math.Round(x, 2);
                                double z = Math.Round(tamt2, 2);

                                double ww = y + z;
                                DataTable dt = (DataTable)ViewState["Outward"];
                                dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                                    txtcustomer.Text.Trim().Replace("&nbsp;", ""),
                                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                                    tax.ToString().Trim().Replace("&nbsp;", ""),
                                    y.ToString().Trim().Replace("&nbsp;", ""),
                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")
                                    );
                                ViewState["Outward"] = dt;
                                this.BindGrid();
                                txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                                dropsize.SelectedValue = null;
                                droptax.SelectedValue = null;
                            }
                            Bindvalues();
                        }
                    }
                    else if (droptaxtype.SelectedItem.Text == "Exclude Tax")
                    {
                        Bindviewstate();
                    }
                }
                btninsert.Visible = true;
                div12.Visible = true;
            }
        }

        protected void Bindviewstate()
        {
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");

            if (radiocustomer.SelectedValue == "1")
            {
                double qty = double.Parse(txtqty.Text);
                double price = double.Parse(txtprice.Text);
                string tax = droptax.SelectedItem.Text;
                if (tax == "GST 5%")
                {
                    double sub = qty * price;
                    double tamt = sub * 0.05;
                    double tot = sub + tamt;
                    double amt = double.Parse(txtamount.Text);

                    // double x = (amt * 100) / 105;

                    //  double tamt2 = x * 0.05;

                    //  double y = Math.Round(x, 2);
                    //  double z = Math.Round(tamt2, 2);

                    //   double ww = y + z;
                    DataTable dt = (DataTable)ViewState["Outward"];
                    dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                        txtname.Text.Trim().Replace("&nbsp;", ""),
                        dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        txtqty.Text.Trim().Replace("&nbsp;", ""),
                        txtprice.Text.Trim().Replace("&nbsp;", ""),
                        tax.ToString().Trim().Replace("&nbsp;", ""),
                        sub.ToString().Trim().Replace("&nbsp;", ""),
                        tamt.ToString().Trim().Replace("&nbsp;", ""),
                        tot.ToString().Trim().Replace("&nbsp;", "")
                        );
                    ViewState["Outward"] = dt;
                    this.BindGrid();
                    txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                    dropsize.SelectedValue = null; droptax.SelectedValue = null;
                }
                else if (tax == "GST 12%")
                {

                    double sub = qty * price;
                    double tamt = sub * 0.12;
                    double tot = sub + tamt;
                    //double amt = double.Parse(txtamount.Text);

                    ////double x = (amt * 100) / 112;

                    // double tamt2 = x * 0.12;

                    // double y = Math.Round(x, 2);
                    // double z = Math.Round(tamt2, 2);

                    //  double ww = y + z;
                    DataTable dt = (DataTable)ViewState["Outward"];
                    dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                        txtname.Text.Trim().Replace("&nbsp;", ""),
                        dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        txtqty.Text.Trim().Replace("&nbsp;", ""),
                        txtprice.Text.Trim().Replace("&nbsp;", ""),
                        tax.ToString().Trim().Replace("&nbsp;", ""),
                        sub.ToString().Trim().Replace("&nbsp;", ""),
                        tamt.ToString().Trim().Replace("&nbsp;", ""),
                        tot.ToString().Trim().Replace("&nbsp;", "")
                        );
                    ViewState["Outward"] = dt;
                    this.BindGrid();
                    txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                    dropsize.SelectedValue = null; droptax.SelectedValue = null;

                }
                else if (tax == "GST 18%")
                {
                    double sub = qty * price;
                    double tamt = sub * 0.18;
                    double tot = sub + tamt;

                    //double amt = double.Parse(txtamount.Text);

                    // double x = (amt * 100) / 118;

                    //  double tamt2 = x * 0.18;

                    //  double y = Math.Round(x, 2);
                    //  double z = Math.Round(tamt2, 2);

                    //  double ww = y + z;
                    //double cs = tamt / 2;
                    //c9 = c9 + cs;
                    //s9 = s9 + cs;
                    //stotal = stotal + sub;
                    //taxamt = taxamt + tot;
                    //lblstotal.Text = stotal.ToString();
                    //lblc9.Text = c9.ToString();
                    //lbls9.Text = s9.ToString();
                    //lbltotal.Text = taxamt.ToString();
                    DataTable dt = (DataTable)ViewState["Outward"];
                    dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                        txtname.Text.Trim().Replace("&nbsp;", ""),
                        dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        txtqty.Text.Trim().Replace("&nbsp;", ""),
                        txtprice.Text.Trim().Replace("&nbsp;", ""),
                        tax.ToString().Trim().Replace("&nbsp;", ""),
                        sub.ToString().Trim().Replace("&nbsp;", ""),
                        tamt.ToString().Trim().Replace("&nbsp;", ""),
                        tot.ToString().Trim().Replace("&nbsp;", "")
                        );
                    ViewState["Outward"] = dt;
                    this.BindGrid();
                    txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                    dropsize.SelectedValue = null; txtamount.Text = null; droptax.SelectedValue = null;
                }

                else if (tax == "GST 28%")
                {
                    double sub = qty * price;
                    double tamt = sub * 0.28;
                    double tot = sub + tamt;

                    //double amt = double.Parse(txtamount.Text);

                    //double x = (amt * 100) / 128;

                    //double tamt2 = x * 0.28;

                    //double y = Math.Round(x, 2);
                    //double z = Math.Round(tamt2, 2);

                    // double ww = y + z;
                    DataTable dt = (DataTable)ViewState["Outward"];
                    dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                        txtname.Text.Trim().Replace("&nbsp;", ""),
                        dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        txtqty.Text.Trim().Replace("&nbsp;", ""),
                        txtprice.Text.Trim().Replace("&nbsp;", ""),
                        tax.ToString().Trim().Replace("&nbsp;", ""),
                        sub.ToString().Trim().Replace("&nbsp;", ""),
                        tamt.ToString().Trim().Replace("&nbsp;", ""),
                        tot.ToString().Trim().Replace("&nbsp;", "")
                        );
                    ViewState["Outward"] = dt;
                    this.BindGrid();
                    txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                    dropsize.SelectedValue = null;
                    droptax.SelectedValue = null;
                }
                Bindvalues();
            }
            else if (radiocustomer.SelectedValue == "0")
            {
                double qty = double.Parse(txtqty.Text);
                double price = double.Parse(txtprice.Text);
                string tax = droptax.SelectedItem.Text;
                if (tax == "GST 5%")
                {
                    double sub = qty * price;
                    double tamt = sub * 0.05;
                    double tot = sub + tamt;
                    //double amt = double.Parse(txtamount.Text);

                    //double x = (amt * 100) / 105;

                    //double tamt2 = x * 0.05;

                    //double y = Math.Round(x, 2);
                    //double z = Math.Round(tamt2, 2);

                    //double ww = y + z;
                    DataTable dt = (DataTable)ViewState["Outward"];
                    dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                        txtcustomer.Text.Trim().Replace("&nbsp;", ""),
                        dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        txtqty.Text.Trim().Replace("&nbsp;", ""),
                        txtprice.Text.Trim().Replace("&nbsp;", ""),
                        tax.ToString().Trim().Replace("&nbsp;", ""),
                        sub.ToString().Trim().Replace("&nbsp;", ""),
                        tamt.ToString().Trim().Replace("&nbsp;", ""),
                        tot.ToString().Trim().Replace("&nbsp;", "")
                        );
                    ViewState["Outward"] = dt;
                    this.BindGrid();
                    txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                    dropsize.SelectedValue = null; droptax.SelectedValue = null;
                }
                else if (tax == "GST 12%")
                {

                    double sub = qty * price;
                    double tamt = sub * 0.12;
                    double tot = sub + tamt;
                    //double amt = double.Parse(txtamount.Text);

                    //    double x = (amt * 100) / 112;

                    //    double tamt2 = x * 0.12;

                    //    double y = Math.Round(x, 2);
                    //    double z = Math.Round(tamt2, 2);

                    //    double ww = y + z;
                    DataTable dt = (DataTable)ViewState["Outward"];
                    dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                        txtcustomer.Text.Trim().Replace("&nbsp;", ""),
                        dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        txtqty.Text.Trim().Replace("&nbsp;", ""),
                        txtprice.Text.Trim().Replace("&nbsp;", ""),
                        tax.ToString().Trim().Replace("&nbsp;", ""),
                        sub.ToString().Trim().Replace("&nbsp;", ""),
                        tamt.ToString().Trim().Replace("&nbsp;", ""),
                        tot.ToString().Trim().Replace("&nbsp;", "")
                        );
                    ViewState["Outward"] = dt;
                    this.BindGrid();
                    txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                    dropsize.SelectedValue = null; droptax.SelectedValue = null;

                }
                else if (tax == "GST 18%")
                {
                    double sub = qty * price;
                    double tamt = sub * 0.18;
                    double tot = sub + tamt;

                    //double amt = double.Parse(txtamount.Text);

                    //    double x = (amt * 100) / 118;

                    //    double tamt2 = x * 0.18;

                    //    double y = Math.Round(x, 2);
                    //    double z = Math.Round(tamt2, 2);

                    //    double ww = y + z;
                    //double cs = tamt / 2;
                    //c9 = c9 + cs;
                    //s9 = s9 + cs;
                    //stotal = stotal + sub;
                    //taxamt = taxamt + tot;
                    //lblstotal.Text = stotal.ToString();
                    //lblc9.Text = c9.ToString();
                    //lbls9.Text = s9.ToString();
                    //lbltotal.Text = taxamt.ToString();
                    DataTable dt = (DataTable)ViewState["Outward"];
                    dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                        txtcustomer.Text.Trim().Replace("&nbsp;", ""),
                        dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        txtqty.Text.Trim().Replace("&nbsp;", ""),
                        txtprice.Text.Trim().Replace("&nbsp;", ""),
                        tax.ToString().Trim().Replace("&nbsp;", ""),
                        sub.ToString().Trim().Replace("&nbsp;", ""),
                        tamt.ToString().Trim().Replace("&nbsp;", ""),
                        tot.ToString().Trim().Replace("&nbsp;", "")
                        );
                    ViewState["Outward"] = dt;
                    this.BindGrid();
                    txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                    dropsize.SelectedValue = null; txtamount.Text = null;
                    droptax.SelectedValue = null;/*droptax.SelectedItem.Text = "";*/
                }

                else if (tax == "GST 28%")
                {
                    double sub = qty * price;
                    double tamt = sub * 0.28;
                    double tot = sub + tamt;

                    //double amt = double.Parse(txtamount.Text);

                    //    double x = (amt * 100) / 128;

                    //    double tamt2 = x * 0.28;

                    //    double y = Math.Round(x, 2);
                    //    double z = Math.Round(tamt2, 2);

                    //    double ww = y + z;
                    DataTable dt = (DataTable)ViewState["Outward"];
                    dt.Rows.Add(date.ToString().Trim().Replace("&nbsp;", ""),
                        txtcustomer.Text.Trim().Replace("&nbsp;", ""),
                        dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                        txtqty.Text.Trim().Replace("&nbsp;", ""),
                        txtprice.Text.Trim().Replace("&nbsp;", ""),
                        tax.ToString().Trim().Replace("&nbsp;", ""),
                        sub.ToString().Trim().Replace("&nbsp;", ""),
                        tamt.ToString().Trim().Replace("&nbsp;", ""),
                        tot.ToString().Trim().Replace("&nbsp;", "")
                        );
                    ViewState["Outward"] = dt;
                    this.BindGrid();
                    txtqty.Text = null; txtprice.Text = null; txtavail.Text = null;
                    dropsize.SelectedValue = null;
                    droptax.SelectedValue = null;
                }
                Bindvalues();
            }

            btninsert.Visible = true;
            div12.Visible = true;

        }


        //Binding Tax Values
        protected void Bindvalues()
        {
            foreach (GridViewRow g1 in GridView1.Rows)
            {
                string tax = g1.Cells[8].Text;
                double sub = double.Parse(g1.Cells[9].Text);
                double tamt = double.Parse(g1.Cells[10].Text);
                double tot = double.Parse(g1.Cells[11].Text);

                if (tax == "GST 5%")
                {
                    stotal = stotal + sub;
                    lblstotal.Text = stotal.ToString();
                    double cs = tamt / 2;
                    c9 = c9 + cs;
                    s9 = s9 + cs;

                    lblc9.Text = c9.ToString();
                    lbls9.Text = s9.ToString();

                    total = total + tot;
                    lbltotal.Text = total.ToString();

                    double x = Math.Round(double.Parse(lblc9.Text), 2);
                    lblc9.Text = x.ToString();
                    double y = Math.Round(double.Parse(lbls9.Text), 2);
                    lbls9.Text = y.ToString();
                }
                else if (tax == "GST 12%")
                {
                    stotal = stotal + sub;
                    lblstotal.Text = stotal.ToString();
                    double cs = tamt / 2;
                    c9 = c9 + cs;
                    s9 = s9 + cs;

                    lblc9.Text = c9.ToString();
                    lbls9.Text = s9.ToString();

                    total = total + tot;
                    lbltotal.Text = total.ToString();

                    double x = Math.Round(double.Parse(lblc9.Text), 2);
                    lblc9.Text = x.ToString();
                    double y = Math.Round(double.Parse(lbls9.Text), 2);
                    lbls9.Text = y.ToString();
                }
                else if (tax == "GST 18%")
                {
                    stotal = stotal + sub;
                    lblstotal.Text = stotal.ToString();
                    double cs = tamt / 2;
                    c9 = c9 + cs;
                    s9 = s9 + cs;

                    lblc9.Text = c9.ToString();
                    lbls9.Text = s9.ToString();

                    total = total + tot;
                    lbltotal.Text = total.ToString();

                    double x = Math.Round(double.Parse(lblc9.Text), 2);
                    lblc9.Text = x.ToString();
                    double y = Math.Round(double.Parse(lbls9.Text), 2);
                    lbls9.Text = y.ToString();
                }

                else if (tax == "GST 28%")
                {
                    stotal = stotal + sub;
                    lblstotal.Text = stotal.ToString();
                    double cs = tamt / 2;
                    c9 = c9 + cs;
                    s9 = s9 + cs;

                    lblc9.Text = c9.ToString();
                    lbls9.Text = s9.ToString();

                    total = total + tot;
                    lbltotal.Text = total.ToString();

                    double x = Math.Round(double.Parse(lblc9.Text), 2);
                    lblc9.Text = x.ToString();
                    double y = Math.Round(double.Parse(lbls9.Text), 2);
                    lbls9.Text = y.ToString();
                }
            }
        }

        //Delteting rows in View State grids
        protected void Deleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["Outward"] as DataTable;
            dt.Rows[index].Delete();
            ViewState["Outward"] = dt;
            this.BindGrid();
            if (index == 0)
            {
                btninsert.Visible = false;
                div12.Visible = false;
                lblstotal.Text = "0";
                lbltotal.Text = "0";
                lblc9.Text = "0";
                lbls9.Text = "0";
            }
            Bindvalues();
        }

        //Inserting Values into Billing Table
        protected void Submit(object sender, EventArgs e)
        {
            string cid = string.Empty;
            string bill = string.Empty;
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd12 = new SqlCommand("Select Top 1 billno from Billing order by billno desc", conn);
            conn.Open();
            SqlDataReader dr = cmd12.ExecuteReader();
            if (dr.Read())
            {
                int value = int.Parse(dr["billno"].ToString());
                value = value + 1;
                bill = value.ToString();
            }
            if (bill == "")
            {
                bill = "1";
            }
            conn.Close();

            foreach (GridViewRow g1 in GridView1.Rows)
            {
                SqlCommand cmd2 = new SqlCommand("Select IsNull(Quantity,0) from AddItem where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                conn.Open();
                int availble = Convert.ToInt32(cmd2.ExecuteScalar());
                conn.Close();
                double value = (double.Parse(g1.Cells[10].Text)) / 2;
                if (radiocustomer.SelectedValue == "0")
                {
                    if (Session["Branch"].ToString() == "Road 2 Banjara Hills")
                    {
                        SqlCommand cmd123 = new SqlCommand("Select IsNull(QuR2,0) from AddItem where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                        conn.Open();
                        int availble1 = Convert.ToInt32(cmd123.ExecuteScalar());
                        conn.Close();

                        SqlCommand Insert = new SqlCommand("Insert into Billing(Date,CustomerName,Mobile,Brand,product,category,size,quantity,Price,amount,Tax,Taxamount,CGST9,SGST9,TotalTax,Total,Billno,Subtotal,Username) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + txtmobile.Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + g1.Cells[9].Text + "','" + g1.Cells[8].Text + "','" + g1.Cells[10].Text + "','" + value.ToString() + "','" + value.ToString() + "','" + (Convert.ToDouble(lblc9.Text) + Convert.ToDouble(lbls9.Text)) + "','" + lbltotal.Text + "','" + bill.ToString() + "','" + lblstotal.Text + "','" + Session["Empname"].ToString() + "')", conn);
                        conn.Open();
                        Insert.ExecuteNonQuery();
                        conn.Close();
                        SqlCommand update = new SqlCommand("Update Additem set QuR2='" + (availble1 - Convert.ToInt32(g1.Cells[6].Text)) + "' where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                        conn.Open();
                        update.ExecuteNonQuery();
                        conn.Close();
                        cid = txtunique.Text;
                    }
                    else if (Session["Branch"].ToString() == "Road 7 Banjara Hills")
                    {
                        SqlCommand cmd123 = new SqlCommand("Select IsNull(Quantity,0) from AddItem where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                        conn.Open();
                        int availble1 = Convert.ToInt32(cmd123.ExecuteScalar());
                        conn.Close();

                        SqlCommand Insert = new SqlCommand("Insert into Billing(Date,CustomerName,Mobile,Brand,product,category,size,quantity,Price,amount,Tax,Taxamount,CGST9,SGST9,TotalTax,Total,Billno,Subtotal,Username) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + txtmobile.Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + g1.Cells[9].Text + "','" + g1.Cells[8].Text + "','" + g1.Cells[10].Text + "','" + value.ToString() + "','" + value.ToString() + "','" + (Convert.ToDouble(lblc9.Text) + Convert.ToDouble(lbls9.Text)) + "','" + lbltotal.Text + "','" + bill.ToString() + "','" + lblstotal.Text + "','" + Session["Empname"].ToString() + "')", conn);
                        conn.Open();
                        Insert.ExecuteNonQuery();
                        conn.Close();
                        SqlCommand update = new SqlCommand("Update Additem set Quantity='" + (availble1 - Convert.ToInt32(g1.Cells[6].Text)) + "' where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                        conn.Open();
                        update.ExecuteNonQuery();
                        conn.Close();
                        cid = txtunique.Text;
                    }

                    else if (Session["Branch"].ToString() == "Road 12 Banjara Hills")
                    {
                        SqlCommand cmd123 = new SqlCommand("Select IsNull(QuR12,0) from AddItem where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                        conn.Open();
                        int availble1 = Convert.ToInt32(cmd123.ExecuteScalar());
                        conn.Close();

                        SqlCommand Insert = new SqlCommand("Insert into Billing(Date,CustomerName,Mobile,Brand,product,category,size,quantity,Price,amount,Tax,Taxamount,CGST9,SGST9,TotalTax,Total,Billno,Subtotal,Username) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + txtmobile.Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + g1.Cells[9].Text + "','" + g1.Cells[8].Text + "','" + g1.Cells[10].Text + "','" + value.ToString() + "','" + value.ToString() + "','" + (Convert.ToDouble(lblc9.Text) + Convert.ToDouble(lbls9.Text)) + "','" + lbltotal.Text + "','" + bill.ToString() + "','" + lblstotal.Text + "','" + Session["Empname"].ToString() + "')", conn);
                        conn.Open();
                        Insert.ExecuteNonQuery();
                        conn.Close();
                        SqlCommand update = new SqlCommand("Update Additem set QuR12='" + (availble1 - Convert.ToInt32(g1.Cells[6].Text)) + "' where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                        conn.Open();
                        update.ExecuteNonQuery();
                        conn.Close();
                        cid = txtunique.Text;
                    }

                }
                else if (radiocustomer.SelectedValue == "1")
                {
                    if (txtname.Text != "" && txtnmobile.Text != "")
                    {
                        SqlCommand retreive = new SqlCommand("Select * from Customer where mobile='" + txtnmobile.Text + "'", conn);
                        SqlDataAdapter da = new SqlDataAdapter(retreive);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            //string message = "alert('Duplicate Entry of Customer...!')";
                            //ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                        }
                        else
                        {
                            SqlCommand insert = new SqlCommand("Insert into Customer(Name,Mobile,Email,Address,GstDetails) values('" + txtname.Text + "','" + txtnmobile.Text + "','" + txtemail.Text + "','" + txtaddress.Text + "','" + txtgst.Text + "')", conn);
                            conn.Open();
                            insert.ExecuteNonQuery();
                            conn.Close();
                        }

                        if (Session["Branch"].ToString() == "Road 2 Banjara Hills")
                        {
                            SqlCommand cmd123 = new SqlCommand("Select IsNull(QuR2,0) from AddItem where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                            conn.Open();
                            int availble1 = Convert.ToInt32(cmd123.ExecuteScalar());
                            conn.Close();

                            SqlCommand Insert1 = new SqlCommand("Insert into Billing(Date,CustomerName,Mobile,Brand,product,category,size,quantity,Price,amount,Tax,Taxamount,CGST9,SGST9,TotalTax,Total,Billno,Subtotal,Username) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + txtnmobile.Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + g1.Cells[9].Text + "','" + g1.Cells[8].Text + "','" + g1.Cells[10].Text + "','" + value.ToString() + "','" + value.ToString() + "','" + (Convert.ToDouble(lblc9.Text) + Convert.ToDouble(lbls9.Text)) + "','" + lbltotal.Text + "','" + bill.ToString() + "','" + lblstotal.Text + "','" + Session["Empname"].ToString() + "')", conn);
                            conn.Open();
                            Insert1.ExecuteNonQuery();
                            conn.Close();
                            SqlCommand update = new SqlCommand("Update Additem set QuR2='" + (availble1 - Convert.ToInt32(g1.Cells[6].Text)) + "' where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                            conn.Open();
                            update.ExecuteNonQuery();
                            conn.Close();
                        }
                        else if (Session["Branch"].ToString() == "Road 7 Banjara Hills")
                        {
                            SqlCommand cmd123 = new SqlCommand("Select IsNull(Quantity,0) from AddItem where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                            conn.Open();
                            int availble1 = Convert.ToInt32(cmd123.ExecuteScalar());
                            conn.Close();

                            SqlCommand Insert1 = new SqlCommand("Insert into Billing(Date,CustomerName,Mobile,Brand,product,category,size,quantity,Price,amount,Tax,Taxamount,CGST9,SGST9,TotalTax,Total,Billno,Subtotal,Username) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + txtnmobile.Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + g1.Cells[9].Text + "','" + g1.Cells[8].Text + "','" + g1.Cells[10].Text + "','" + value.ToString() + "','" + value.ToString() + "','" + (Convert.ToDouble(lblc9.Text) + Convert.ToDouble(lbls9.Text)) + "','" + lbltotal.Text + "','" + bill.ToString() + "','" + lblstotal.Text + "','" + Session["Empname"].ToString() + "')", conn);
                            conn.Open();
                            Insert1.ExecuteNonQuery();
                            conn.Close();
                            SqlCommand update = new SqlCommand("Update Additem set Quantity='" + (availble1 - Convert.ToInt32(g1.Cells[6].Text)) + "' where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                            conn.Open();
                            update.ExecuteNonQuery();
                            conn.Close();
                        }
                        else if (Session["Branch"].ToString() == "Road 12 Banjara Hills")
                        {
                            SqlCommand cmd123 = new SqlCommand("Select IsNull(QuR12,0) from AddItem where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                            conn.Open();
                            int availble1 = Convert.ToInt32(cmd123.ExecuteScalar());
                            conn.Close();

                            SqlCommand Insert1 = new SqlCommand("Insert into Billing(Date,CustomerName,Mobile,Brand,product,category,size,quantity,Price,amount,Tax,Taxamount,CGST9,SGST9,TotalTax,Total,Billno,Subtotal,Username) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + txtnmobile.Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + g1.Cells[9].Text + "','" + g1.Cells[8].Text + "','" + g1.Cells[10].Text + "','" + value.ToString() + "','" + value.ToString() + "','" + (Convert.ToDouble(lblc9.Text) + Convert.ToDouble(lbls9.Text)) + "','" + lbltotal.Text + "','" + bill.ToString() + "','" + lblstotal.Text + "','" + Session["Empname"].ToString() + "')", conn);
                            conn.Open();
                            Insert1.ExecuteNonQuery();
                            conn.Close();
                            SqlCommand update = new SqlCommand("Update Additem set QuR12='" + (availble1 - Convert.ToInt32(g1.Cells[6].Text)) + "' where Brand='" + g1.Cells[2].Text + "' and Product='" + g1.Cells[3].Text + "' and Category='" + g1.Cells[4].Text + "' and Size='" + g1.Cells[5].Text + "'", conn);
                            conn.Open();
                            update.ExecuteNonQuery();
                            conn.Close();
                        }

                        SqlCommand select = new SqlCommand("Select * from Customer where Mobile='" + txtnmobile.Text + "'", conn);
                        conn.Open();
                        SqlDataReader rd = select.ExecuteReader();
                        if (rd.Read())
                        {
                            cid = rd["id"].ToString();
                        }
                        conn.Close();
                    }
                    else if (txtname.Text == "" || txtnmobile.Text == "")
                    {
                        string message = "alert('Fill all the Values...!')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                }


            }
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            if (radiopayment.SelectedValue == "0")
            {
                if (txtpaid.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Insert Into Payment(Date,CustomerID,Totalamt,Paidamt,Remainamt,status,Billno,username,Status1) values('" + date.ToString() + "','" + cid + "','" + txttotal.Text + "','" + txtpaid.Text + "','" + txtremain.Text + "','through cash','" + bill + "','" + Session["Empname"].ToString() + "','0')", conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    SqlCommand cmd2 = new SqlCommand("Insert Into PaymentStatus(Date,CustomerID,Totalamt,Paidamt,Remainamt,status,Billno,username,Status1) values('" + date.ToString() + "','" + cid + "','" + txttotal.Text + "','" + txtpaid.Text + "','" + txtremain.Text + "','through cash','" + bill + "','" + Session["Empname"].ToString() + "','0')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();
                }
                else if (txtpaid.Text == "")
                {
                    string message = "alert('Paid Amount id mandatory...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
            else if (radiopayment.SelectedValue == "1")
            {
                if (txtcard.Text != "" && txtcardpaid.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Insert Into Payment(Date,CustomerID,Totalamt,Paidamt,Remainamt,Cardno,status,Billno,username,Status1) values('" + date.ToString() + "','" + cid + "','" + txtamt.Text + "','" + txtcardpaid.Text + "','" + txtreamt.Text + "','" + txtcard.Text + "','through Card','" + bill + "','" + Session["Empname"].ToString() + "','0')", conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    SqlCommand cmd2 = new SqlCommand("Insert Into PaymentStatus(Date,CustomerID,Totalamt,Paidamt,Remainamt,Cardno,status,Billno,username,Status1) values('" + date.ToString() + "','" + cid + "','" + txtamt.Text + "','" + txtcardpaid.Text + "','" + txtreamt.Text + "','" + txtcard.Text + "','through Card','" + bill + "','" + Session["Empname"].ToString() + "','0')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();
                }
                else if (txtcard.Text == "" && txtcardpaid.Text == "")
                {
                    string message = "alert('Card number and Paid amount are mandatory...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
            else if (radiopayment.SelectedValue == "2")
            {
                if (txtcheque.Text != "" && paid.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Insert Into Payment(Date,CustomerID,Totalamt,Paidamt,Remainamt,Chequeno,status,Billno,username,Status1) values('" + date.ToString() + "','" + cid + "','" + total12.Text + "','" + paid.Text + "','" + remain.Text + "','" + txtcheque.Text + "','through Cheque','" + bill + "','" + Session["Empname"].ToString() + "','0')", conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();

                    SqlCommand cmd2 = new SqlCommand("Insert Into PaymentStatus(Date,CustomerID,Totalamt,Paidamt,Remainamt,Chequeno,status,Billno,username,Status1) values('" + date.ToString() + "','" + cid + "','" + total12.Text + "','" + paid.Text + "','" + remain.Text + "','" + txtcheque.Text + "','through Cheque','" + bill + "','" + Session["Empname"].ToString() + "','0')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();

                }
                else if (txtcheque.Text == "" && paid.Text == "")
                {
                    string message = "alert('Cheque number and Paid amount are mandatory...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }

            Session["Bill"] = bill.ToString();
            Session["cid"] = cid.ToString();
            Response.Redirect("~/BillView.aspx");
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            radiocash.Visible = true;
        }
        protected void Payment_Change(object sender, EventArgs e)
        {
            if (radiopayment.SelectedValue == "0")
            {
                cash.Visible = true;
                card.Visible = false;
                cheque.Visible = false;
            }
            else if (radiopayment.SelectedValue == "1")
            {
                cash.Visible = false;
                card.Visible = true;
                cheque.Visible = false;
            }
            else if (radiopayment.SelectedValue == "2")
            {
                cash.Visible = false;
                card.Visible = false;
                cheque.Visible = true;
            }
            Button1.Visible = true;
            txttotal.Text = lbltotal.Text;
            txtamt.Text = lbltotal.Text;
            total12.Text = lbltotal.Text;
        }


        protected void cashPaid_Change(object sender, EventArgs e)
        {
            double value = double.Parse(txttotal.Text) - double.Parse(txtpaid.Text);
            txtremain.Text = value.ToString();
        }

        protected void CardPaid_Change(object sender, EventArgs e)
        {
            double value = double.Parse(txtamt.Text) - double.Parse(txtcardpaid.Text);
            txtreamt.Text = value.ToString();
        }

        protected void ChequePaid(object sender, EventArgs e)
        {
            double value = double.Parse(total12.Text) - double.Parse(paid.Text);
            remain.Text = value.ToString();
        }
    }
}