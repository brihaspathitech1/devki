﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class ItemsInward : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtdate.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
            }
            if (!IsPostBack)
            {
                BindVendor();
                BindBrand();
                BindCategory();
                BindSize();
            }

            if (!IsPostBack)
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[15] { new DataColumn("Date"), new DataColumn("vendor"), new DataColumn("hsn"), new DataColumn("invno"), new DataColumn("invdate"), new DataColumn("Brand"), new DataColumn("Product"), new DataColumn("Category"), new DataColumn("Size"), new DataColumn("qty"), new DataColumn("price"), new DataColumn("amount"), new DataColumn("tax"), new DataColumn("taxamt"), new DataColumn("total") });
                ViewState["Inward"] = dt;
                this.BindGrid();
            }
        }

        protected void BindGrid()
        {
            GridView1.DataSource = (DataTable)ViewState["Inward"];
            GridView1.DataBind();
        }

        //Binding Vendor
        protected void BindVendor()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select Id,Name from Vendor order by id desc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropvendor.DataSource = dr;
                    dropvendor.DataTextField = "Name";
                    dropvendor.DataValueField = "Id";
                    dropvendor.DataBind();
                    dropvendor.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //Binding brand

        protected void BindBrand()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select Id,Brand from Brand order by Brand asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropbrand.DataSource = dr;
                    dropbrand.DataTextField = "Brand";
                    dropbrand.DataValueField = "Id";
                    dropbrand.DataBind();
                    dropbrand.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //binding Product
        protected void BindProduct()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from Product where Brand='" + dropbrand.SelectedValue + "' order by Product asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropproduct.DataSource = dr;
                    dropproduct.DataTextField = "Product";
                    dropproduct.DataValueField = "Id";
                    dropproduct.DataBind();
                    dropproduct.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        protected void Brand_Change(object sender, EventArgs e)
        {
            BindProduct();
        }

        protected void BindCategory()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Category order by Category asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropcategory.DataSource = dr;
                    dropcategory.DataTextField = "Category";
                    dropcategory.DataValueField = "Id";
                    dropcategory.DataBind();
                    dropcategory.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        protected void Product_Change(object sender, EventArgs e)
        {
            BindCategory();

        }

        //Binding Size
        protected void BindSize()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //cmd.CommandText = "Select * from Size where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "' and Category='" + dropcategory.SelectedValue + "'";
                    cmd.CommandText = "Select * from Size order by Size asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropsize.DataSource = dr;
                    dropsize.DataTextField = "Size";
                    dropsize.DataValueField = "Id";
                    dropsize.DataBind();
                    dropsize.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        protected void Category_Change(object sender, EventArgs e)
        {
            BindSize();
        }

        protected void Size_Change(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select Quantity from AddItem where Brand='" + dropbrand.SelectedItem.Text + "' and Product='" + dropproduct.SelectedItem.Text + "' and Category='" + dropcategory.SelectedItem.Text + "' and Size='" + dropsize.SelectedItem.Text + "'";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        txtavailable.Text = dr["Quantity"].ToString();
                    }
                    if (txtavailable.Text == "")
                    {
                        txtavailable.Text = "0";
                    }
                    conn.Close();
                }
            }
        }


        //Inserting Brand into Brand table
        protected void Brand_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from Brand where Brand='" + txtBrand.Text + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                string message = "alert('Duplicate Entry...!')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            }
            else
            {
                SqlCommand cmd2 = new SqlCommand("Insert into Brand(Brand) values ('" + txtBrand.Text + "')", conn);
                conn.Open();
                cmd2.ExecuteNonQuery();
                conn.Close();
                BindBrand();
            }
        }

        //Inserting values into Product table
        protected void Product_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Product where Brand='" + dropbrand.SelectedValue + "' and Product='" + txtproduct.Text + "'";
                    cmd.Connection = conn;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        string message = "alert('Duplicate Entry...!')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                    else
                    {
                        SqlCommand cmd2 = new SqlCommand("Insert into Product(Brand,Product) values ('" + dropbrand.SelectedValue + "','" + txtproduct.Text + "')", conn);
                        conn.Open();
                        cmd2.ExecuteNonQuery();
                        conn.Close();
                        BindProduct();
                    }

                }
            }
        }

        //Inserting values into category table
        protected void Category_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Category where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "' and Category='" + txtcategory.Text + "'";
                    cmd.Connection = conn;
                    //conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        string message = "alert('Duplicate Entry...!')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                    else
                    {
                        SqlCommand cmd2 = new SqlCommand("Insert into Category(Brand,Product,Category) values ('" + dropbrand.SelectedValue + "','" + dropproduct.SelectedValue + "','" + txtcategory.Text + "')", conn);
                        conn.Open();
                        cmd2.ExecuteNonQuery();
                        conn.Close();
                        BindCategory();
                    }
                    //conn.Close();
                }
            }
        }

        //insert values into Size table
        protected void Size_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Size where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "' and Category='" + dropcategory.SelectedValue + "' and Size='" + txtsize.Text + "'";
                    cmd.Connection = conn;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        string message = "alert('Duplicate Entry...!')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                    else
                    {
                        SqlCommand cmd2 = new SqlCommand("Insert into Size(Brand,Product,Category,Size) values ('" + dropbrand.SelectedValue + "','" + dropproduct.SelectedValue + "','" + dropcategory.SelectedValue + "','" + txtsize.Text + "')", conn);
                        conn.Open();
                        cmd2.ExecuteNonQuery();
                        conn.Close();
                        BindSize();
                    }

                }
            }
        }

        protected void Vendor_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Vendor where mobile='" + txtmobile.Text + "'";
                    cmd.Connection = conn;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        string message = "alert('Duplicate Entry...!')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                    else
                    {
                        if (txtname.Text == "" || txtmobile.Text == "" || txtaddress.Text == "")
                        {
                            string message = "alert('Fill all the values...!')";
                            ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                        }
                        else
                        {
                            SqlCommand cmd2 = new SqlCommand("Insert into Vendor(Name,mobile,email,address,GSTDetails) values('" + txtname.Text + "','" + txtmobile.Text + "','" + txtemail.Text + "','" + txtaddress.Text + "','" + txtgst.Text + "')", conn);
                            conn.Open();
                            cmd2.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                }
            }
        }

        protected void Price_Change(object sender, EventArgs e)
        {
            double value = double.Parse(txtprice.Text) * double.Parse(txtqty.Text);
            txtamount.Text = value.ToString();
        }
        protected void Add_Click(object sender, EventArgs e)
        {
            if (txtqty.Text != "")
            {
                if (droptaxtype.SelectedItem.Text == "Include Tax")
                {
                    double qty = double.Parse(txtqty.Text);
                    double price = double.Parse(txtprice.Text);
                    string tax = droptax.SelectedItem.Text;
                    if (tax == "GST 5%")
                    {
                        double amt = double.Parse(txtamount.Text);

                        double x = (amt * 100) / 105;

                        double tamt2 = x * 0.05;

                        double y = Math.Round(x, 2);
                        double z = Math.Round(tamt2, 2);

                        double ww = y + z;
                        DataTable dt = (DataTable)ViewState["Inward"];
                        dt.Rows.Add(txtdate.Text.Trim().Replace("&nbsp;", ""),
                            dropvendor.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txthsn.Text.Trim().Replace("&nbsp;", ""),
                    txtInv.Text.Trim().Replace("&nbsp;", ""),
                    txtInvDate.Text.Trim().Replace("&nbsp;", ""),
                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                     y.ToString().Trim().Replace("&nbsp;", ""),
                    tax.ToString().Trim().Replace("&nbsp;", ""),

                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")

                    );
                        ViewState["Inward"] = dt;
                    }
                    else if (tax == "GST 12%")
                    {
                        double amt = double.Parse(txtamount.Text);

                        double x = (amt * 100) / 112;

                        double tamt2 = x * 0.12;

                        double y = Math.Round(x, 2);
                        double z = Math.Round(tamt2, 2);

                        double ww = y + z;
                        DataTable dt = (DataTable)ViewState["Inward"];
                        dt.Rows.Add(txtdate.Text.Trim().Replace("&nbsp;", ""),
                            dropvendor.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txthsn.Text.Trim().Replace("&nbsp;", ""),
                    txtInv.Text.Trim().Replace("&nbsp;", ""),
                    txtInvDate.Text.Trim().Replace("&nbsp;", ""),
                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                     y.ToString().Trim().Replace("&nbsp;", ""),
                    tax.ToString().Trim().Replace("&nbsp;", ""),

                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")

                    );
                        ViewState["Inward"] = dt;
                    }
                    else if (tax == "GST 18%")
                    {
                        double amt = double.Parse(txtamount.Text);

                        double x = (amt * 100) / 118;

                        double tamt2 = x * 0.18;

                        double y = Math.Round(x, 2);
                        double z = Math.Round(tamt2, 2);

                        double ww = y + z;
                        DataTable dt = (DataTable)ViewState["Inward"];
                        dt.Rows.Add(txtdate.Text.Trim().Replace("&nbsp;", ""),
                            dropvendor.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txthsn.Text.Trim().Replace("&nbsp;", ""),
                    txtInv.Text.Trim().Replace("&nbsp;", ""),
                    txtInvDate.Text.Trim().Replace("&nbsp;", ""),
                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                     y.ToString().Trim().Replace("&nbsp;", ""),
                    tax.ToString().Trim().Replace("&nbsp;", ""),

                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")

                    );
                        ViewState["Inward"] = dt;
                    }
                    else if (tax == "GST 28%")
                    {
                        double amt = double.Parse(txtamount.Text);

                        double x = (amt * 100) / 128;

                        double tamt2 = x * 0.28;

                        double y = Math.Round(x, 2);
                        double z = Math.Round(tamt2, 2);

                        double ww = y + z;
                        DataTable dt = (DataTable)ViewState["Inward"];
                        dt.Rows.Add(txtdate.Text.Trim().Replace("&nbsp;", ""),
                            dropvendor.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txthsn.Text.Trim().Replace("&nbsp;", ""),
                    txtInv.Text.Trim().Replace("&nbsp;", ""),
                    txtInvDate.Text.Trim().Replace("&nbsp;", ""),
                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                     y.ToString().Trim().Replace("&nbsp;", ""),
                    tax.ToString().Trim().Replace("&nbsp;", ""),

                                    z.ToString().Trim().Replace("&nbsp;", ""),
                                    ww.ToString().Trim().Replace("&nbsp;", "")

                    );
                        ViewState["Inward"] = dt;
                    }
                }
                else if (droptaxtype.SelectedItem.Text == "Exclude Tax")
                {
                    double qty = double.Parse(txtqty.Text);
                    double price = double.Parse(txtprice.Text);
                    string tax = droptax.SelectedItem.Text;
                    if (tax == "GST 5%")
                    {
                        double sub = qty * price;
                        double tamt = sub * 0.05;
                        double tot = sub + tamt;
                        double amt = double.Parse(txtamount.Text);
                        DataTable dt = (DataTable)ViewState["Inward"];
                        dt.Rows.Add(txtdate.Text.Trim().Replace("&nbsp;", ""),
                            dropvendor.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txthsn.Text.Trim().Replace("&nbsp;", ""),
                    txtInv.Text.Trim().Replace("&nbsp;", ""),
                    txtInvDate.Text.Trim().Replace("&nbsp;", ""),
                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                     sub.ToString().Trim().Replace("&nbsp;", ""),
                    tax.ToString().Trim().Replace("&nbsp;", ""),

                                    tamt.ToString().Trim().Replace("&nbsp;", ""),
                                    tot.ToString().Trim().Replace("&nbsp;", "")

                    );
                        ViewState["Inward"] = dt;
                    }
                    else if (tax == "GST 12%")
                    {
                        double sub = qty * price;
                        double tamt = sub * 0.12;
                        double tot = sub + tamt;
                        double amt = double.Parse(txtamount.Text);
                        DataTable dt = (DataTable)ViewState["Inward"];
                        dt.Rows.Add(txtdate.Text.Trim().Replace("&nbsp;", ""),
                            dropvendor.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txthsn.Text.Trim().Replace("&nbsp;", ""),
                    txtInv.Text.Trim().Replace("&nbsp;", ""),
                    txtInvDate.Text.Trim().Replace("&nbsp;", ""),
                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                     sub.ToString().Trim().Replace("&nbsp;", ""),
                    tax.ToString().Trim().Replace("&nbsp;", ""),

                                    tamt.ToString().Trim().Replace("&nbsp;", ""),
                                    tot.ToString().Trim().Replace("&nbsp;", "")

                    );
                        ViewState["Inward"] = dt;
                    }
                    else if (tax == "GST 18%")
                    {
                        double sub = qty * price;
                        double tamt = sub * 0.18;
                        double tot = sub + tamt;
                        double amt = double.Parse(txtamount.Text);
                        DataTable dt = (DataTable)ViewState["Inward"];
                        dt.Rows.Add(txtdate.Text.Trim().Replace("&nbsp;", ""),
                            dropvendor.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txthsn.Text.Trim().Replace("&nbsp;", ""),
                    txtInv.Text.Trim().Replace("&nbsp;", ""),
                    txtInvDate.Text.Trim().Replace("&nbsp;", ""),
                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                     sub.ToString().Trim().Replace("&nbsp;", ""),
                    tax.ToString().Trim().Replace("&nbsp;", ""),

                                    tamt.ToString().Trim().Replace("&nbsp;", ""),
                                    tot.ToString().Trim().Replace("&nbsp;", "")

                    );
                        ViewState["Inward"] = dt;
                    }
                    else if (tax == "GST 28%")
                    {
                        double sub = qty * price;
                        double tamt = sub * 0.28;
                        double tot = sub + tamt;
                        double amt = double.Parse(txtamount.Text);
                        DataTable dt = (DataTable)ViewState["Inward"];
                        dt.Rows.Add(txtdate.Text.Trim().Replace("&nbsp;", ""),
                            dropvendor.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txthsn.Text.Trim().Replace("&nbsp;", ""),
                    txtInv.Text.Trim().Replace("&nbsp;", ""),
                    txtInvDate.Text.Trim().Replace("&nbsp;", ""),
                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txtqty.Text.Trim().Replace("&nbsp;", ""),
                    txtprice.Text.Trim().Replace("&nbsp;", ""),
                     sub.ToString().Trim().Replace("&nbsp;", ""),
                    tax.ToString().Trim().Replace("&nbsp;", ""),

                                    tamt.ToString().Trim().Replace("&nbsp;", ""),
                                    tot.ToString().Trim().Replace("&nbsp;", "")

                    );
                        ViewState["Inward"] = dt;
                    }
                }

                this.BindGrid();
                Bindvalues();
                txtqty.Text = null;
                txtavailable.Text = null;
                dropsize.SelectedValue = null;
                btninsert.Visible = true;
                div12.Visible = true;
            }
        }

        double stotal = 0, taxamt = 0, total = 0;
        double c9 = 0, s9 = 0;
        //Binding Tax Values
        protected void Bindvalues()
        {
            foreach (GridViewRow g1 in GridView1.Rows)
            {
                string tax = g1.Cells[12].Text;
                double sub = double.Parse(g1.Cells[11].Text);
                double tamt = double.Parse(g1.Cells[13].Text);
                double tot = double.Parse(g1.Cells[14].Text);

                if (tax == "GST 5%")
                {
                    stotal = stotal + sub;
                    lblstotal.Text = stotal.ToString();
                    double cs = tamt / 2;
                    c9 = c9 + cs;
                    s9 = s9 + cs;

                    lblc9.Text = c9.ToString();
                    lbls9.Text = s9.ToString();

                    total = total + tot;
                    lbltotal.Text = total.ToString();

                    double x = Math.Round(double.Parse(lblc9.Text), 2);
                    lblc9.Text = x.ToString();
                    double y = Math.Round(double.Parse(lbls9.Text), 2);
                    lbls9.Text = y.ToString();
                }
                else if (tax == "GST 12%")
                {
                    stotal = stotal + sub;
                    lblstotal.Text = stotal.ToString();
                    double cs = tamt / 2;
                    c9 = c9 + cs;
                    s9 = s9 + cs;

                    lblc9.Text = c9.ToString();
                    lbls9.Text = s9.ToString();

                    total = total + tot;
                    lbltotal.Text = total.ToString();

                    double x = Math.Round(double.Parse(lblc9.Text), 2);
                    lblc9.Text = x.ToString();
                    double y = Math.Round(double.Parse(lbls9.Text), 2);
                    lbls9.Text = y.ToString();
                }
                else if (tax == "GST 18%")
                {
                    stotal = stotal + sub;
                    lblstotal.Text = stotal.ToString();
                    double cs = tamt / 2;
                    c9 = c9 + cs;
                    s9 = s9 + cs;

                    lblc9.Text = c9.ToString();
                    lbls9.Text = s9.ToString();

                    total = total + tot;
                    lbltotal.Text = total.ToString();

                    double x = Math.Round(double.Parse(lblc9.Text), 2);
                    lblc9.Text = x.ToString();
                    double y = Math.Round(double.Parse(lbls9.Text), 2);
                    lbls9.Text = y.ToString();
                }

                else if (tax == "GST 28%")
                {
                    stotal = stotal + sub;
                    lblstotal.Text = stotal.ToString();
                    double cs = tamt / 2;
                    c9 = c9 + cs;
                    s9 = s9 + cs;

                    lblc9.Text = c9.ToString();
                    lbls9.Text = s9.ToString();

                    total = total + tot;
                    lbltotal.Text = total.ToString();

                    double x = Math.Round(double.Parse(lblc9.Text), 2);
                    lblc9.Text = x.ToString();
                    double y = Math.Round(double.Parse(lbls9.Text), 2);
                    lbls9.Text = y.ToString();
                }
            }
        }

        protected void Deleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["Inward"] as DataTable;
            dt.Rows[index].Delete();
            ViewState["Inward"] = dt;
            this.BindGrid();
            if (index == 0)
            {
                btninsert.Visible = false;
            }
            Bindvalues();
        }

        protected void Insert_Click(object sender, EventArgs e)
        {
            string bill = string.Empty;
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select Top 1 billno from Inward order by billno desc", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                int value = int.Parse(dr["billno"].ToString());
                value = value + 1;
                bill = value.ToString();
            }
            if (bill == "")
            {
                bill = "1";
            }
            conn.Close();
            foreach (GridViewRow g1 in GridView1.Rows)
            {
                SqlCommand cmd2 = new SqlCommand("Select IsNull(Quantity,0) from AddItem where Brand='" + g1.Cells[5].Text + "' and Product='" + g1.Cells[6].Text + "' and Category='" + g1.Cells[7].Text + "' and Size='" + g1.Cells[8].Text + "'", conn);
                conn.Open();
                int availble = Convert.ToInt32(cmd2.ExecuteScalar());
                conn.Close();

                SqlCommand insert = new SqlCommand("Insert into Inward(Date,vendorName,Brand,product,category,size,quantity,billno,Hsnno,Invno,Invdate,Price,Amount,Tax,TaxAmount,CGST,SGST,Subtotal,Total) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + g1.Cells[8].Text + "','" + g1.Cells[9].Text + "','" + bill.ToString() + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[10].Text + "','" + g1.Cells[11].Text + "','" + g1.Cells[12].Text + "','" + g1.Cells[13].Text + "','" + lblc9.Text + "','" + lbls9.Text + "','" + lblstotal.Text + "','" + lbltotal.Text + "')", conn);
                conn.Open();
                insert.ExecuteNonQuery();
                conn.Close();

                SqlCommand update = new SqlCommand("Update Additem set Quantity='" + (availble + Convert.ToInt32(g1.Cells[9].Text)) + "' where Brand='" + g1.Cells[5].Text + "' and Product='" + g1.Cells[6].Text + "' and Category='" + g1.Cells[7].Text + "' and Size='" + g1.Cells[8].Text + "'", conn);
                conn.Open();
                update.ExecuteNonQuery();
                conn.Close();

                string message = "alert('Inward Completed Successfully')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);

                Response.Redirect("~/ItemsInward.aspx");
            }
        }

    }
}