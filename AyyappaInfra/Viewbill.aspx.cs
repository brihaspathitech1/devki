﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AyyappaInfra
{
    public partial class Viewbill : System.Web.UI.Page
    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["DevkiInterior"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bindaddress();
                BindCustomer();
                BindItems();
                Bindpayment(); 
            }
        }

        protected void Bindaddress()
        {
            lblbranch.Text = Session["Branch"].ToString();
            if (lblbranch.Text == "Road 2 Banjara Hills")
            {
                divr2.Visible = true;
                r2.Visible = true;
                Label1.Text = "36APFPB4337P1ZO";
                Label2.Text = "36APFPB4337P1ZO";
                lblpan.Text = "APFPB4337P";
                lblpan2.Text = "APFPB4337P";
                Label3.Text = "DEVKI INTERIORS";
                Label4.Text = "DEVKI INTERIORS";
            }
            else if (lblbranch.Text == "Road 7 Banjara Hills")
            {
                divr7.Visible = true;
                r7.Visible = true;
                Label1.Text = "36AAXFS6224K1ZH";
                Label2.Text = "36AAXFS6224K1ZH";
                lblpan.Text = "AAXFS6224K";
                lblpan2.Text = "AAXFS6224K";
                Label3.Text = "SHARMA FURNITURES";
                Label4.Text = "SHARMA FURNITURES";
            }
            else if (lblbranch.Text == "Road 12 Banjara Hills")
            {
                divr12.Visible = true;
                divr122.Visible = true;
                Label1.Text = "36APFPB4337P1ZO";
                Label2.Text = "36APFPB4337P1ZO";
                lblpan.Text = "APFPB4337P";
                lblpan2.Text = "APFPB4337P";
                Label3.Text = "DEVKI INTERIORS";
                Label4.Text = "DEVKI INTERIORS";
            }
        }
        protected void BindCustomer()
        {
            string bill = Session["Bill"].ToString();
            SqlCommand cmd = new SqlCommand(" with cte as( select b.customername,ROW_NUMBER() over (partition by b.billno order by b.billno desc) as rn,c.name,c.GstDetails,c.mobile,c.Email,c.Address,c.id from Customer c inner join Billing b on  b.CustomerName=c.Name where b.Billno='" + Session["Bill"].ToString() + "') select * from cte where rn=1", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            //GridCustomer.DataSource = ds;
            //GridCustomer.DataBind();

            //GridCust.DataSource = ds;
            //GridCust.DataBind();

            if (ds.Tables[0].Rows.Count > 0)
            {
                CustName.Text = ds.Tables[0].Rows[0]["name"].ToString();
                lblname.Text = ds.Tables[0].Rows[0]["name"].ToString();
                Mobile.Text = ds.Tables[0].Rows[0]["mobile"].ToString();
                lblmobile.Text = ds.Tables[0].Rows[0]["mobile"].ToString();
                Address.Text = ds.Tables[0].Rows[0]["Address"].ToString();
                lbladdress.Text = ds.Tables[0].Rows[0]["Address"].ToString();
                Gst.Text = ds.Tables[0].Rows[0]["GstDetails"].ToString();
                lblGst.Text = ds.Tables[0].Rows[0]["GstDetails"].ToString();
                lblcid.Text= ds.Tables[0].Rows[0]["id"].ToString();
            }

        }

        protected void BindItems()
        {
            SqlCommand cmd = new SqlCommand("select Convert(varchar(10),date,103) as date,Billno,Brand,product,category,size,quantity,Price,amount,Tax,CGST9,SGST9,Subtotal,Total,IGST18,IGST28,Bno from Billing where Billno='" + Session["Bill"].ToString() + "'", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                double aa = 0, bb = 0,i1=0,i2=0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    double cgst = double.Parse(ds.Tables[0].Rows[i]["CGST9"].ToString());
                    aa = aa + cgst;
                    double sgst = double.Parse(ds.Tables[0].Rows[i]["SGST9"].ToString());
                    bb = bb + sgst;
                    double igst18 = double.Parse(ds.Tables[0].Rows[i]["IGST18"].ToString());
                    i1 = i1 + igst18;
                    double igst28 = double.Parse(ds.Tables[0].Rows[i]["IGST28"].ToString());
                    i2 = i2 + igst18;

                    lblcgst.Text = aa.ToString();
                    lblsgst.Text = bb.ToString();
                    lblcgst2.Text = aa.ToString();
                    lblsgst2.Text = bb.ToString();
                    lbli18.Text = i1.ToString();
                    lbli182.Text = i1.ToString();
                    lbli28.Text = i2.ToString();
                    lbli282.Text = i2.ToString();

                }
                lblsub.Text = ds.Tables[0].Rows[0]["Subtotal"].ToString();
                lbltotal.Text = ds.Tables[0].Rows[0]["Total"].ToString();
                lblsubtotal.Text = ds.Tables[0].Rows[0]["Subtotal"].ToString();
                lbltotal2.Text = ds.Tables[0].Rows[0]["Total"].ToString();

                lbldate.Text = ds.Tables[0].Rows[0]["Date"].ToString();
                //lblbill.Text = ds.Tables[0].Rows[0]["Billno"].ToString();
                lblbill.Text = ds.Tables[0].Rows[0]["Bno"].ToString();
                lbldate2.Text = ds.Tables[0].Rows[0]["Date"].ToString();
                //lblbill2.Text = ds.Tables[0].Rows[0]["Billno"].ToString();
                lblbill2.Text = ds.Tables[0].Rows[0]["Bno"].ToString();

                GridItems.DataSource = ds;
                GridItems.DataBind();
                GridItems2.DataSource = ds;
                GridItems2.DataBind();
            }
        }
        protected void Bindpayment()
        {
            SqlCommand cmd = new SqlCommand("Select Paidamt,Remainamt from Payment where BillNo='" + Session["Bill"].ToString() + "' and status1='0' and CustomerID='"+lblcid.Text+"' and back_up is null", connstr);
            connstr.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lbladvance.Text = dr["Paidamt"].ToString();
                advance.Text = dr["Paidamt"].ToString();
                lblbalance.Text = dr["Remainamt"].ToString();
                balance.Text = dr["Remainamt"].ToString();
            }
            connstr.Close();
        }
    }
}