﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="StockTransfer.aspx.cs" Inherits="AyyappaInfra.StockTranfer" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Stock Transfer
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                     <div class="col-md-3">
                                <div class="form-group"> 
                                   Date <asp:TextBox ID="txtdate" runat="server" CssClass="form-control"></asp:TextBox> 
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtdate" Format="dd/MM/yyyy" />
                                    </div>
                                         </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                             <asp:UpdatePanel ID="Update" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="form-group">                  
                             Branch From<asp:DropDownList ID="dropBranch" CssClass="form-control" runat="server">
                               <asp:ListItem></asp:ListItem>
                               <asp:ListItem Value="0">Road 2 Banjara Hills</asp:ListItem>
                               <asp:ListItem Value="1">Road 7 Banjara Hills</asp:ListItem>
                               <asp:ListItem Value="2">Road 12 Banjara Hills</asp:ListItem>
                                        </asp:DropDownList>
                                    <asp:Label ID="lblmsg" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                   
                                     </div>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                                   <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="form-group">                  
                           To<asp:DropDownList ID="dropbranchto" CssClass="form-control" OnSelectedIndexChanged="Branch_Change" AutoPostBack="true" runat="server">
                               <asp:ListItem></asp:ListItem>
                               <asp:ListItem Value="0">Road 2 Banjara Hills</asp:ListItem>
                             <%--  <asp:ListItem Value="1">Road 7 Banjara Hills</asp:ListItem>--%>
                               <asp:ListItem Value="2">Road 12 Banjara Hills</asp:ListItem>
                                        </asp:DropDownList>
                                     </div>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>

                            <div id="div1" runat="server" visible="false">
                                <div class="col-md-3">
                                <div class="form-group">  
                                Brand<asp:DropDownList ID="dropbrand" CssClass="form-control" runat="server" OnSelectedIndexChanged="Brand_Change" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                <div class="form-group">  
                                Product<asp:DropDownList ID="dropproduct" CssClass="form-control" runat="server" OnSelectedIndexChanged="Product_Change" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    </div>
                               
                                <div class="col-md-3">
                                <div class="form-group">  
                                Category<asp:DropDownList ID="dropcategory" CssClass="form-control" runat="server" OnSelectedIndexChanged="Category_Change" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    </div>
                                <div class="col-md-3">
                                <div class="form-group">  
                                Size<asp:DropDownList ID="dropsize" CssClass="form-control" runat="server" OnSelectedIndexChanged="Size_Change" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    </div>
                                <div class="col-md-3">
                                <div class="form-group">  
                                Available <asp:TextBox ID="txtavail" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                    </div>
                                <div class="col-md-3">
                                <div class="form-group">  
                                Quantity<asp:TextBox ID="txtqty" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtqty" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                    </div>
                                
                                 <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">  
                                    <asp:Button ID="btnadd" runat="server" CssClass="btn btn-primary" Text="Add" OnClick="Add_Click" />
                                    </div>
                                     </div>
                                 </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                              <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                <div class="col-md-12">
                                    <asp:GridView ID="GridView1" CssClass="table table-respnsive table-striped" GridLines="None" runat="server" AutoGenerateColumns="false" OnRowDeleting="Deleting">
                                        <Columns>
                                        <asp:BoundField DataField="Date" HeaderText="Date" />
                                        <asp:BoundField DataField="branch" HeaderText="From" />
                                             <asp:BoundField DataField="Tobranch" HeaderText="To" />
                                        <asp:BoundField DataField="Brand" HeaderText="Brand" />
                                        <asp:BoundField DataField="Product" HeaderText="Product" />
                                        <asp:BoundField DataField="Category" HeaderText="Category" />
                                        <asp:BoundField DataField="Size" HeaderText="Size" />
                                        <asp:BoundField DataField="qty" HeaderText="Quantity" />
                                             <asp:CommandField ShowDeleteButton="true" ControlStyle-CssClass="btn btn-danger btn-sm" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                    </ContentTemplate>
                                  </asp:UpdatePanel>
                              <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                <div class="col-md-3">
                                <div class="form-group"> 
                                    <asp:Button ID="btninsert" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" Visible="false" />
                                    </div>
                                </div>
                                    </ContentTemplate>
                                  </asp:UpdatePanel>
                           
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
