﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AyyappaInfra.Startup))]
namespace AyyappaInfra
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
