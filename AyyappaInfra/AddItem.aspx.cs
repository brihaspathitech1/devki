﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace AyyappaInfra
{
    public partial class AddItem : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindBrand();
                BindCategory();
                BindSize();
            }
        }

        //binding brand
        protected void BindBrand()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Brand order by Brand asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropBrand.DataSource = dr;
                    dropBrand.DataTextField = "Brand";
                    dropBrand.DataValueField = "Id";
                    dropBrand.DataBind();
                    dropBrand.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //binding Product
        protected void BindProduct()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from Product where Brand='" + dropBrand.SelectedValue + "' order by Product asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropproduct.DataSource = dr;
                    dropproduct.DataTextField = "Product";
                    dropproduct.DataValueField = "Id";
                    dropproduct.DataBind();
                    dropproduct.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        protected void Brand_Change(object sender,EventArgs e)
        {
            BindProduct();
        }

        //Binding Category
        protected void BindCategory()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    // cmd.CommandText = "Select * from Category where Brand='" + dropBrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "'";
                    cmd.CommandText = "Select Category,Id from Category order by Category asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropcategory.DataSource = dr;
                    dropcategory.DataTextField = "Category";
                    dropcategory.DataValueField = "Id";
                    dropcategory.DataBind();
                    dropcategory.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        protected void Product_Change(object sender,EventArgs e)
        {
            BindCategory();
        }

        //Binding Size
        protected void BindSize()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //cmd.CommandText = "Select * from Size where Brand='" + dropBrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "' and Category='" + dropcategory.SelectedValue + "'";
                    cmd.CommandText = "Select Size,Id from Size order by Size asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropsize.DataSource = dr;
                    dropsize.DataTextField = "Size";
                    dropsize.DataValueField = "Id";
                    dropsize.DataBind();
                    dropsize.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        protected void Category_Change(object sender,EventArgs e)
        {
            BindSize();
        }

        //Inserting Brand into Brand table
        protected void Brand_Click(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from Brand where Brand='" + txtBrand.Text + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if(dt.Rows.Count>0)
            {
                string message = "alert('Duplicate Entry...!')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            }
            else
            {
                SqlCommand cmd2 = new SqlCommand("Insert into Brand(Brand) values ('" + txtBrand.Text + "')", conn);
                conn.Open();
                cmd2.ExecuteNonQuery();
                conn.Close();
                BindBrand();
            }
        }

        //Inserting values into Product table
        protected void Product_Click(object sender,EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Product where Brand='" + dropBrand.SelectedValue + "' and Product='" + txtproduct.Text + "'";
                    cmd.Connection = conn;
                    
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if(dt.Rows.Count>0)
                    {
                        string message = "alert('Duplicate Entry...!')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                    else
                    {
                        SqlCommand cmd2 = new SqlCommand("Insert into Product(Brand,Product) values ('" + dropBrand.SelectedValue + "','" + txtproduct.Text + "')", conn);
                        conn.Open();
                        cmd2.ExecuteNonQuery();
                        conn.Close();
                        BindProduct();
                    }
                   
                }
            }
        }

        //Inserting values into category table
        protected void Category_Click(object sender,EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Category where Brand='" + dropBrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "' and Category='" + txtcategory.Text + "'";
                    cmd.Connection = conn;
                    //conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if(dt.Rows.Count>0)
                    {
                        string message = "alert('Duplicate Entry...!')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                    else
                    {
                        SqlCommand cmd2 = new SqlCommand("Insert into Category(Brand,Product,Category) values ('" + dropBrand.SelectedValue + "','" + dropproduct.SelectedValue + "','" + txtcategory.Text + "')", conn);
                        conn.Open();
                        cmd2.ExecuteNonQuery();
                        conn.Close();
                        BindCategory();
                    }
                    //conn.Close();
                }
            }
        }

        //insert values into Size table
        protected void Size_Click(object sender,EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from Size where Brand='" + dropBrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "' and Category='" + dropcategory.SelectedValue + "' and Size='" + txtsize.Text + "'";
                    cmd.Connection = conn;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if(dt.Rows.Count>0)
                    {
                        string message = "alert('Duplicate Entry...!')";
                        ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                    }
                    else
                    {
                        SqlCommand cmd2 = new SqlCommand("Insert into Size(Brand,Product,Category,Size) values ('" + dropBrand.SelectedValue + "','" + dropproduct.SelectedValue + "','" + dropcategory.SelectedValue + "','" + txtsize.Text + "')", conn);
                        conn.Open();
                        cmd2.ExecuteNonQuery();
                        conn.Close();
                        BindSize();
                    }
                   
                }
            }
        }

        //Inserting values into Additem table
        protected void submit_Click(object sender,EventArgs e)
        {
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            SqlConnection conn = new SqlConnection(connstr);
            if (dropBrand.SelectedItem.Text == "" || dropproduct.SelectedItem.Text == "" || dropcategory.SelectedItem.Text == "" || dropsize.SelectedItem.Text == "")
            {
                string message = "alert('Select All the Items...!')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            }
            else
            {
                SqlCommand cmd = new SqlCommand("select * from AddItem where Brand='" + dropBrand.SelectedItem.Text + "' and Product='" + dropproduct.SelectedItem.Text + "' and Category='" + dropcategory.SelectedItem.Text + "' and size='" + dropsize.SelectedItem.Text + "'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    string message = "alert('Duplicate Entry...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
                else
                {
                    SqlCommand cmd2 = new SqlCommand("Insert into AddItem(date,Brand,Product,Category,Size) values('" + date.ToString() + "','" + dropBrand.SelectedItem.Text + "','" + dropproduct.SelectedItem.Text + "','" + dropcategory.SelectedItem.Text + "','" + dropsize.SelectedItem.Text + "')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();
                    Response.Redirect("~/AddItem.aspx");
                }
            }
        }
    }
}