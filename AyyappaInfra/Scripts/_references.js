/// <autosync enabled="true" />
/// <reference path="../bootstrap/js/bootstrap.js" />
/// <reference path="../bootstrap/js/npm.js" />
/// <reference path="../dist/js/app.js" />
/// <reference path="../dist/js/demo.js" />
/// <reference path="../dist/js/pages/dashboard.js" />
/// <reference path="../dist/js/pages/dashboard2.js" />
/// <reference path="../plugins/chartjs/chart.js" />
/// <reference path="../plugins/ckeditor/adapters/jquery.js" />
/// <reference path="../plugins/ckeditor/build-config.js" />
/// <reference path="../plugins/ckeditor/ckeditor.js" />
/// <reference path="../plugins/ckeditor/config.js" />
/// <reference path="../plugins/ckeditor/lang/af.js" />
/// <reference path="../plugins/ckeditor/lang/ar.js" />
/// <reference path="../plugins/ckeditor/lang/bg.js" />
/// <reference path="../plugins/ckeditor/lang/bn.js" />
/// <reference path="../plugins/ckeditor/lang/bs.js" />
/// <reference path="../plugins/ckeditor/lang/ca.js" />
/// <reference path="../plugins/ckeditor/lang/cs.js" />
/// <reference path="../plugins/ckeditor/lang/cy.js" />
/// <reference path="../plugins/ckeditor/lang/da.js" />
/// <reference path="../plugins/ckeditor/lang/de.js" />
/// <reference path="../plugins/ckeditor/lang/de-ch.js" />
/// <reference path="../plugins/ckeditor/lang/el.js" />
/// <reference path="../plugins/ckeditor/lang/en.js" />
/// <reference path="../plugins/ckeditor/lang/en-au.js" />
/// <reference path="../plugins/ckeditor/lang/en-ca.js" />
/// <reference path="../plugins/ckeditor/lang/en-gb.js" />
/// <reference path="../plugins/ckeditor/lang/eo.js" />
/// <reference path="../plugins/ckeditor/lang/es.js" />
/// <reference path="../plugins/ckeditor/lang/et.js" />
/// <reference path="../plugins/ckeditor/lang/eu.js" />
/// <reference path="../plugins/ckeditor/lang/fa.js" />
/// <reference path="../plugins/ckeditor/lang/fi.js" />
/// <reference path="../plugins/ckeditor/lang/fo.js" />
/// <reference path="../plugins/ckeditor/lang/fr.js" />
/// <reference path="../plugins/ckeditor/lang/fr-ca.js" />
/// <reference path="../plugins/ckeditor/lang/gl.js" />
/// <reference path="../plugins/ckeditor/lang/gu.js" />
/// <reference path="../plugins/ckeditor/lang/he.js" />
/// <reference path="../plugins/ckeditor/lang/hi.js" />
/// <reference path="../plugins/ckeditor/lang/hr.js" />
/// <reference path="../plugins/ckeditor/lang/hu.js" />
/// <reference path="../plugins/ckeditor/lang/id.js" />
/// <reference path="../plugins/ckeditor/lang/is.js" />
/// <reference path="../plugins/ckeditor/lang/it.js" />
/// <reference path="../plugins/ckeditor/lang/ja.js" />
/// <reference path="../plugins/ckeditor/lang/ka.js" />
/// <reference path="../plugins/ckeditor/lang/km.js" />
/// <reference path="../plugins/ckeditor/lang/ko.js" />
/// <reference path="../plugins/ckeditor/lang/ku.js" />
/// <reference path="../plugins/ckeditor/lang/lt.js" />
/// <reference path="../plugins/ckeditor/lang/lv.js" />
/// <reference path="../plugins/ckeditor/lang/mk.js" />
/// <reference path="../plugins/ckeditor/lang/mn.js" />
/// <reference path="../plugins/ckeditor/lang/ms.js" />
/// <reference path="../plugins/ckeditor/lang/nb.js" />
/// <reference path="../plugins/ckeditor/lang/nl.js" />
/// <reference path="../plugins/ckeditor/lang/no.js" />
/// <reference path="../plugins/ckeditor/lang/pl.js" />
/// <reference path="../plugins/ckeditor/lang/pt.js" />
/// <reference path="../plugins/ckeditor/lang/pt-br.js" />
/// <reference path="../plugins/ckeditor/lang/ro.js" />
/// <reference path="../plugins/ckeditor/lang/ru.js" />
/// <reference path="../plugins/ckeditor/lang/si.js" />
/// <reference path="../plugins/ckeditor/lang/sk.js" />
/// <reference path="../plugins/ckeditor/lang/sl.js" />
/// <reference path="../plugins/ckeditor/lang/sq.js" />
/// <reference path="../plugins/ckeditor/lang/sr.js" />
/// <reference path="../plugins/ckeditor/lang/sr-latn.js" />
/// <reference path="../plugins/ckeditor/lang/sv.js" />
/// <reference path="../plugins/ckeditor/lang/th.js" />
/// <reference path="../plugins/ckeditor/lang/tr.js" />
/// <reference path="../plugins/ckeditor/lang/tt.js" />
/// <reference path="../plugins/ckeditor/lang/ug.js" />
/// <reference path="../plugins/ckeditor/lang/uk.js" />
/// <reference path="../plugins/ckeditor/lang/vi.js" />
/// <reference path="../plugins/ckeditor/lang/zh.js" />
/// <reference path="../plugins/ckeditor/lang/zh-cn.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/a11yhelp.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/af.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/ar.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/bg.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/ca.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/cs.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/cy.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/da.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/de.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/de-ch.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/el.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/en.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/en-gb.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/eo.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/es.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/et.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/eu.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/fa.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/fi.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/fo.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/fr.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/fr-ca.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/gl.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/gu.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/he.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/hi.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/hr.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/hu.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/id.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/it.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/ja.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/km.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/ko.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/ku.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/lt.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/lv.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/mk.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/mn.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/nb.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/nl.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/no.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/pl.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/pt.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/pt-br.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/ro.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/ru.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/si.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/sk.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/sl.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/sq.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/sr.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/sr-latn.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/sv.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/th.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/tr.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/tt.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/ug.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/uk.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/vi.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/zh.js" />
/// <reference path="../plugins/ckeditor/plugins/a11yhelp/dialogs/lang/zh-cn.js" />
