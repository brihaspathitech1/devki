﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin2.Master" AutoEventWireup="true" CodeBehind="InwardReport.aspx.cs" Inherits="AyyappaInfra.InwardReport" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Inward List
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="from-group">
                                    Date<asp:TextBox ID="txtdate" CssClass="form-control" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtdate" />
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtdate" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3">
                                <div class="from-group">
                                    Vendor<asp:DropDownList ID="dropvendor" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3" style="padding-top:20px">
                                <div class="from-group">
                                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit" CausesValidation="true" />
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                            <div class="col-md-12">
                                <asp:GridView ID="GridInward" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="billno" HeaderText="Bill No" />
                                        <asp:BoundField DataField="vendorName" HeaderText="Name" />
                                        <asp:BoundField DataField="Hsnno" HeaderText="HSN No" />
                                        <asp:BoundField DataField="Invno" HeaderText="Invoice No" />
                                        <asp:BoundField DataField="Invdate" HeaderText="Invoice Date" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="Total" HeaderText="Total" />
                                       
                                         <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnview" CssClass="btn btn-success" runat="server" Text="View" CommandArgument='<%# Eval("Billno") %>' OnClick="View_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" CancelControlID="btnclose" PopupControlID="Panel1"></cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="500px" Width="650px">
                                 <h3 class="text-center">Item Details</h3>
                                <hr />
                                  <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                <asp:GridView ID="Gridpopup" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="Brand" HeaderText="Brand" />
                                        <asp:BoundField DataField="Product" HeaderText="Product" />
                                        <asp:BoundField DataField="Category" HeaderText="Category" />
                                        <asp:BoundField DataField="Size" HeaderText="Size" />
                                        <asp:BoundField DataField="Quantity" HeaderText="Qty" />
                                        <asp:BoundField DataField="Price" HeaderText="Price" />
                                        <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                        <asp:BoundField DataField="Tax" HeaderText="Tax" />
                                        <asp:BoundField DataField="TaxAmount" HeaderText="Tax Amt" />
                                        <asp:BoundField DataField="totalamt" HeaderText="Total" />
                                    </Columns>
                                </asp:GridView>
                                    </ContentTemplate>
                                      </asp:UpdatePanel>
                                  <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                <div class="clearfix"></div>
                                <div class=" col-md-12 text-right">
                                     
                                    <span style="padding-right:150px">   Sub Total <asp:Label ID="lblsubtotal" runat="server" Text='<%# Eval("subtotal") %>'></asp:Label></span>
                                    
                                
                                  <span style="padding-right:2px"> 
                                        Total <asp:Label ID="lbltotal" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                        </span>
                                     </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                        <asp:Button ID="btnclose" CssClass="btn btn-danger" runat="server" Text="Close" />
                                        </div>
                                    </div>
                            </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
