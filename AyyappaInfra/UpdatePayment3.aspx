﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin2.Master" AutoEventWireup="true" CodeBehind="UpdatePayment3.aspx.cs" Inherits="AyyappaInfra.UpdatePayment3" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Update Payment
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                              Select Payment
                            <asp:RadioButtonList ID="radiopayment" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="Payment_Change" AutoPostBack="true">
                            
                                <asp:ListItem Value="0">Cash</asp:ListItem>
                                <asp:ListItem Value="1">Card</asp:ListItem>
                                <asp:ListItem Value="2">Cheque</asp:ListItem>
                            </asp:RadioButtonList>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                            <div id="cash" runat="server" visible="false">
                               <%-- <div class="col-md-3">
                                    <div class="form-group">

                                    
                                Cash<asp:TextBox ID="txtcash" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                </div>--%>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Total Amount<asp:TextBox ID="txttotal" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>

                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Paid Amount<asp:TextBox ID="txtpaid" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                      
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Remaining Amount<asp:TextBox ID="txtremain" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                        Present Paid Amount<asp:TextBox ID="txtppaid" CssClass="form-control" runat="server" OnTextChanged="Cash_Change" AutoPostBack="true"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtppaid" FilterType="Numbers" />
                                        <asp:Label ID="Label5" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                    </div>
                                        </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Remaining<asp:TextBox ID="txtpremain" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                            <div id="card" runat="server" visible="false">
                                <div class="col-md-3">
                                    <div class="form-group">
                                Card No<asp:TextBox ID="txtcard" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:Label ID="Label1" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                    </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Total Amount<asp:TextBox ID="txtamt" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Paid Amount<asp:TextBox ID="txtcardpaid" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtcardpaid" FilterType="Numbers" />
                                    </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Remaining Amount<asp:TextBox ID="txtreamt" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Present Paid Amount<asp:TextBox ID="txtcppaid" CssClass="form-control" runat="server" OnTextChanged="Card_Change" AutoPostBack="true"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtcppaid" FilterType="Numbers" />
                                        <asp:Label ID="Label2" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                         Remaining<asp:TextBox ID="txtcremain" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                     </div>
                            </div>
</ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                            <div id="cheque" runat="server" visible="false">
                                <div class="col-md-3">
                                    <div class="form-group">
                                Cheque No<asp:TextBox ID="txtcheque" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:Label ID="Label3" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Total Amount<asp:TextBox ID="total" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Paid Amount<asp:TextBox ID="paid" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                       <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="paid" FilterType="Numbers" />
                                         </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Remaining Amount<asp:TextBox ID="remain" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Present Paid Amount<asp:TextBox ID="txtchpaid" CssClass="form-control" runat="server" OnTextChanged="Cheque_Change" AutoPostBack="true"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtchpaid" FilterType="Numbers" />
                                        <asp:Label ID="Label4" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                         Remaining<asp:TextBox ID="txtchremain" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                     </div>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3" style="padding-top:20px">
                                    <div class="form-group">
                                        <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Payment_Click" Visible="false" />
                                        </div>
                                </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>

                            <asp:Label ID="lblbill" runat="server" Text="" Visible="false"></asp:Label>
                             <asp:Label ID="lblid" runat="server" Text="" Visible="false"></asp:Label>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
