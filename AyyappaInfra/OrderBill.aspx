﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin2.Master" AutoEventWireup="true" CodeBehind="OrderBill.aspx.cs" Inherits="AyyappaInfra.OrderBill" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Billing
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary" style="min-height:880px">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3">
                                <div class="form-group">
                                Customer Name<span class="style1">*</span><asp:TextBox ID="txtcustomer" CssClass="form-control" runat="server" OnTextChanged="Mobile_Change" AutoPostBack="true"></asp:TextBox>
                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtcustomer"
                                                MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1"
                                                CompletionInterval="1" ServiceMethod="GetMobileNo" UseContextKey="True">
                                            </cc1:AutoCompleteExtender>
                                    </div>
                                     </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                 <div class="col-md-3">
                                <div class="form-group">
                                Mobile<asp:TextBox ID="txtmobile" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                     </div>
                                   </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                 <div class="col-md-3" runat="server" visible="false">
                                <div class="form-group">
                                Unique Id<asp:TextBox ID="txtunique" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                     </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                              <div class="col-md-3">
                                <div class="form-group">
                            Brand<span class="style1">*</span><asp:DropDownList ID="dropbrand" CssClass="form-control" runat="server" OnSelectedIndexChanged="Brand_Change" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                           
                             <div class="col-md-3">
                                <div class="form-group">
                            Product<span class="style1">*</span><asp:DropDownList ID="dropproduct" CssClass="form-control" runat="server" OnSelectedIndexChanged="Prodcut_Change" AutoPostBack="true" ></asp:DropDownList>
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3">
                                <div class="form-group">
                            Category<span class="style1">*</span><asp:DropDownList ID="dropcategory" CssClass="form-control" runat="server" OnSelectedIndexChanged="Category_Change" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                              <div class="col-md-3">
                                <div class="form-group">
                            Size<span class="style1">*</span><asp:DropDownList ID="dropsize" CssClass="form-control" runat="server" OnSelectedIndexChanged="Size_Change" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3">
                                <div class="form-group">
                            Available<asp:TextBox ID="txtavail" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                 </div>
                                    </ContentTemplate>

                                    </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3">
                                <div class="form-group">
                            Quantity<span class="style1">*</span><asp:TextBox ID="txtqty" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="txtqty" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="form-group">
                                    Price per Unit<span class="style1">*</span><asp:TextBox ID="txtprice" CssClass="form-control" OnTextChanged="Price_Change" AutoPostBack="true" runat="server"></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="txtprice" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="form-group">
                                    Total Amount<span class="style1">*</span><asp:TextBox ID="txtamount" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="txtamount" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3">
                                <div class="form-group">
                                    Tax Type<span class="style1">*</span><asp:DropDownList ID="droptaxtype" CssClass="form-control" runat="server" OnSelectedIndexChanged="TaxType_Change" AutoPostBack="true">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>Include Tax</asp:ListItem>
                                        <asp:ListItem>Exclude Tax</asp:ListItem>
                                            </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="droptaxtype" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                     </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                <ContentTemplate>
                            <div id="taxdiv" runat="server" visible="false">
                                 <div class="col-md-3">
                                <div class="form-group">
                                    Select Tax<span class="style1">*</span><asp:DropDownList ID="droptax" CssClass="form-control" runat="server">
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem>GST 5%</asp:ListItem>
                                        <asp:ListItem>GST 12%</asp:ListItem>
                                        <asp:ListItem>GST 18%</asp:ListItem>
                                        <asp:ListItem>GST 28%</asp:ListItem>
                                              </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="droptax" ForeColor="Red" Font-Bold="true"></asp:RequiredFieldValidator>
                                    </div>
                                     </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                            <asp:Button ID="btnadd" CssClass="btn btn-primary" runat="server" Text="Add" OnClick="Add_Click" />
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            <asp:Label ID="lblmsg" runat="server" Text="" Visible="false" Font-Bold="true" ForeColor="Red"></asp:Label>
                            <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                <ContentTemplate>
                             <div class="col-md-12">
                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false" OnRowDeleting="Deleting">
                                    <Columns>
                                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:yyyy-MM-dd}" />
                                        <asp:BoundField DataField="CustomerName" HeaderText="Name" />
                                        <asp:BoundField DataField="Brand" HeaderText="Brand" />
                                        <asp:BoundField DataField="Product" HeaderText="Product" />
                                        <asp:BoundField DataField="category" HeaderText="Category" />
                                        <asp:BoundField DataField="size" HeaderText="Size" />
                                        <asp:BoundField DataField="quantity" HeaderText="Qty" />
                                        <asp:BoundField DataField="price" HeaderText="Price" />
                                        <asp:BoundField DataField="Tax" HeaderText="Tax" />
                                        <asp:BoundField DataField="amount" HeaderText="Sub Total" />
                                        <asp:BoundField DataField="Taxamount" HeaderText="Tax Amount" />
                                        <asp:BoundField DataField="tot" HeaderText="Total" />
                                         <asp:CommandField ShowDeleteButton="true" ControlStyle-CssClass="btn btn-danger btn-sm" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                <ContentTemplate>
                             <div id="div12" runat="server">
                                    <div class="col-md-offset-8 col-md-4" style="text-align:right; padding-right:108px" >
                             <div class="col-md-6">Sub Total:</div> <div class="col-md-6"><asp:Label ID="lblstotal" runat="server" Text="0"></asp:Label><br /></div>
                                    
                           <%-- GST [5%]:<asp:Label ID="lblc5" runat="server" Text="0"></asp:Label><br />--%>
                           <div class="col-md-6"> CGST:</div> <div class="col-md-6"><asp:Label ID="lblc9" runat="server" Text="0"></asp:Label><br /></div>
                                    
                           <div class="col-md-6"> SGST:</div> <div class="col-md-6"><asp:Label ID="lbls9" runat="server" Text="0"></asp:Label><br /></div>
                           <%-- CGST [14%]:<asp:Label ID="lblc14" runat="server" Text="0"></asp:Label><br />
                            SGST [14%]:<asp:Label ID="lbls14" runat="server" Text="0"></asp:Label><br />
                            IGST [18%]:<asp:Label ID="lblI18" runat="server" Text="0"></asp:Label><br />
                            IGST [28%]:<asp:Label ID="lblI28" runat="server" Text="0"></asp:Label><br />--%>
                                    
                            <div class="col-md-6">Total:</div> <div class="col-md-6"><asp:Label ID="lbltotal" runat="server" Text="0"></asp:Label></div>
                                </div> 
                                </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                    <asp:Button ID="btninsert" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit_Click" /><asp:Label ID="lblmessage" runat="server" Text="" Visible="false" ForeColor="Red" Font-Bold="true"></asp:Label>
                                    </div>
                                </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <div class="clearfix"></div>
                          
                                          <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                <ContentTemplate>
                               <div class="col-md-6">
                                         <div id="radiocash" runat="server" visible="false">
                                   Select Payment
                            <asp:RadioButtonList ID="radiopayment" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="Payment_Change" AutoPostBack="true">
                            
                                <asp:ListItem Value="0">Cash</asp:ListItem>
                                <asp:ListItem Value="1">Card</asp:ListItem>
                                <asp:ListItem Value="2">Cheque</asp:ListItem>
                            </asp:RadioButtonList>  
                            </div>
                                      </div>
                                    <div class="clearfix"></div>
                                     </ContentTemplate>
                                 </asp:UpdatePanel>

                                 
                            

                                    <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                <ContentTemplate>
                             <div id="cash" runat="server" visible="false">
                               <div class="col-md-3">
                                    <div class="form-group">
                                Total Amount<asp:TextBox ID="txttotal" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>

                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Paid Amount<asp:TextBox ID="txtpaid" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                      
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Remaining Amount<asp:TextBox ID="txtremain" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                        Present Paid Amount<asp:TextBox ID="txtppaid" CssClass="form-control" runat="server" OnTextChanged="Cash_Change" AutoPostBack="true"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtppaid" FilterType="Numbers" />
                                        <asp:Label ID="Label3" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                    </div>
                                        </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Remaining<asp:TextBox ID="txtpremain" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>

                            </div>
                          <div class="clearfix"></div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                           <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                <ContentTemplate>
                             <div id="card" runat="server" visible="false">
                                 <div class="col-md-3">
                                    <div class="form-group">
                                Card No<asp:TextBox ID="txtcard" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:Label ID="Label2" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                    </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Total Amount<asp:TextBox ID="txtamt" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Paid Amount<asp:TextBox ID="txtcardpaid" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtcardpaid" FilterType="Numbers" />
                                    </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Remaining Amount<asp:TextBox ID="txtreamt" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Present Paid Amount<asp:TextBox ID="txtcppaid" CssClass="form-control" runat="server" OnTextChanged="Card_Change" AutoPostBack="true"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtcppaid" FilterType="Numbers" />
                                        <asp:Label ID="Label1" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                         Remaining<asp:TextBox ID="txtcremain" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                     </div>
                            </div>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                            
                            <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                <ContentTemplate>
                             <div id="cheque" runat="server" visible="false">
                              <div class="col-md-3">
                                    <div class="form-group">
                                Cheque No<asp:TextBox ID="txtcheque" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:Label ID="Label4" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Total Amount<asp:TextBox ID="total12" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Paid Amount<asp:TextBox ID="paid" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                       <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="paid" FilterType="Numbers" />
                                         </div>
                                    </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                Remaining Amount<asp:TextBox ID="remain" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        Present Paid Amount<asp:TextBox ID="txtchpaid" CssClass="form-control" runat="server" OnTextChanged="Cheque_Change" AutoPostBack="true"></asp:TextBox>
                                          <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtchpaid" FilterType="Numbers" />
                                        <asp:Label ID="Label5" runat="server" Text="Required" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                         Remaining<asp:TextBox ID="txtchremain" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                     </div>
                            </div>
                                </ContentTemplate>
                                 </asp:UpdatePanel>

                           <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                <ContentTemplate>
                                     <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                    <asp:Button ID="Button1" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="Submit" Visible="false" />
                                    </div>
                                         </div>
                            <asp:Label ID="lbldate" runat="server" Text="" Visible="false"></asp:Label>   
                                    </ContentTemplate>
                                  </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
