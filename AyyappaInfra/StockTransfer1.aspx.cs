﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class StcokTransfer1 : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtdate.Text = System.DateTime.Now.ToString("yyyy-MM-dd");
                BindBrand();
                BindCategory();
                BindSize();
            }
            if (!IsPostBack)
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[8] { new DataColumn("Date"), new DataColumn("branch"), new DataColumn("Tobranch"), new DataColumn("Brand"), new DataColumn("Product"), new DataColumn("Category"), new DataColumn("Size"), new DataColumn("qty") });
                ViewState["BInward"] = dt;
                this.BindGrid();

            }
        }
        protected void BindGrid()
        {
            GridView1.DataSource = (DataTable)ViewState["BInward"];
            GridView1.DataBind();
        }

        protected void Branch_Change(object sender, EventArgs e)
        {
            div1.Visible = true;
        }

        //Binding brand

        protected void BindBrand()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select Id,Brand from Brand order by Brand asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropbrand.DataSource = dr;
                    dropbrand.DataTextField = "Brand";
                    dropbrand.DataValueField = "Id";
                    dropbrand.DataBind();
                    dropbrand.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //Binding product
        protected void Brand_Change(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from Product where Brand='" + dropbrand.SelectedValue + "' order by Product asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropproduct.DataSource = dr;
                    dropproduct.DataTextField = "Product";
                    dropproduct.DataValueField = "Id";
                    dropproduct.DataBind();
                    dropproduct.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //Binding Category

        protected void Product_Change(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //cmd.CommandText = "Select * from Category where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "'";
                    cmd.CommandText = "Select * from Category order by Category asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropcategory.DataSource = dr;
                    dropcategory.DataTextField = "Category";
                    dropcategory.DataValueField = "Id";
                    dropcategory.DataBind();
                    dropcategory.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        protected void BindCategory()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    // cmd.CommandText = "Select * from Category where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "'";
                    cmd.CommandText = "Select * from Category order by Category asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropcategory.DataSource = dr;
                    dropcategory.DataTextField = "Category";
                    dropcategory.DataValueField = "Id";
                    dropcategory.DataBind();
                    dropcategory.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        protected void BindSize()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //cmd.CommandText = "Select * from Size where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "' and Category='" + dropcategory.SelectedValue + "'";
                    cmd.CommandText = "Select * from Size order by Size asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropsize.DataSource = dr;
                    dropsize.DataTextField = "Size";
                    dropsize.DataValueField = "Id";
                    dropsize.DataBind();
                    dropsize.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //Binding Size

        protected void Category_Change(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //cmd.CommandText = "Select * from Size where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "' and Category='" + dropcategory.SelectedValue + "'";
                    cmd.CommandText = "Select * from Size order by Size asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropsize.DataSource = dr;
                    dropsize.DataTextField = "Size";
                    dropsize.DataValueField = "Id";
                    dropsize.DataBind();
                    dropsize.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }

        //binding Available qty

        protected void Size_Change(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select Quantity from AddItem where Brand='" + dropbrand.SelectedItem.Text + "' and Product='" + dropproduct.SelectedItem.Text + "' and Category='" + dropcategory.SelectedItem.Text + "' and Size='" + dropsize.SelectedItem.Text + "'";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        txtavail.Text = dr["Quantity"].ToString();
                    }
                    conn.Close();
                }
            }
        }

        //Binding values into Gridview

        protected void Add_Click(object sender, EventArgs e)
        {

            if (txtavail.Text != "" && txtqty.Text != "")
            {
                DataTable dt = (DataTable)ViewState["BInward"];
                dt.Rows.Add(txtdate.Text.ToString().Trim().Replace("&nbsp;", ""),
                    dropBranch.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropbranchto.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropbrand.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropproduct.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropcategory.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    dropsize.SelectedItem.Text.Trim().Replace("&nbsp;", ""),
                    txtqty.Text.Trim().Replace("&nbsp;", "")

                    );
                ViewState["BInward"] = dt;
                this.BindGrid();
                txtqty.Text = null;
                txtavail.Text = null;
                dropsize.SelectedValue = null;
                btninsert.Visible = true;
            }
        }
        protected void Deleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["BInward"] as DataTable;
            dt.Rows[index].Delete();
            ViewState["BInward"] = dt;
            this.BindGrid();
            if (index == 0)
            {
                btninsert.Visible = false;

            }

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (dropbranchto.SelectedValue == "0")
            {
                string bill = string.Empty;
                SqlConnection conn = new SqlConnection(connstr);
                SqlCommand cmd = new SqlCommand("Select Top 1 billno from R2BHInward order by Billno desc", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    int value = int.Parse(dr["billno"].ToString());
                    value = value + 1;
                    bill = value.ToString();
                }
                if (bill == "")
                {
                    bill = "1";
                }
                conn.Close();
                foreach (GridViewRow g1 in GridView1.Rows)
                {
                    SqlCommand cmd2 = new SqlCommand("Select IsNull(Quantity,0) from AddItem where Brand='" + g1.Cells[3].Text + "' and Product='" + g1.Cells[4].Text + "' and Category='" + g1.Cells[5].Text + "' and Size='" + g1.Cells[6].Text + "'", conn);
                    conn.Open();
                    int availble = Convert.ToInt32(cmd2.ExecuteScalar());
                    conn.Close();

                    SqlCommand cmd12 = new SqlCommand("Select IsNull(QuR2,0) from AddItem where Brand='" + g1.Cells[3].Text + "' and Product='" + g1.Cells[4].Text + "' and Category='" + g1.Cells[5].Text + "' and Size='" + g1.Cells[6].Text + "'", conn);
                    conn.Open();
                    int availble1 = Convert.ToInt32(cmd12.ExecuteScalar());
                    conn.Close();


                    SqlCommand insert = new SqlCommand("Insert into R2BHInward(Date,BranchName,Tobranch,Brand,product,category,size,quantity,billno) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + bill.ToString() + "')", conn);
                    conn.Open();
                    insert.ExecuteNonQuery();
                    conn.Close();

                    SqlCommand outward = new SqlCommand("Insert into Outward(Date,BranchName,Tobranch,Brand,product,category,size,quantity,billno,Status) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + bill.ToString() + "','0')", conn);
                    conn.Open();
                    outward.ExecuteNonQuery();
                    conn.Close();

                    SqlCommand update = new SqlCommand("Update Additem set QuR2='" + (availble1 + Convert.ToInt32(g1.Cells[7].Text)) + "', Quantity='" + (availble - Convert.ToInt32(g1.Cells[7].Text)) + "' where Brand='" + g1.Cells[3].Text + "' and Product='" + g1.Cells[4].Text + "' and Category='" + g1.Cells[5].Text + "' and Size='" + g1.Cells[6].Text + "'", conn);
                    conn.Open();
                    update.ExecuteNonQuery();
                    conn.Close();

                }

            }
            else if (dropbranchto.SelectedValue == "1")
            {
                string bill = string.Empty;
                SqlConnection conn = new SqlConnection(connstr);
                SqlCommand cmd = new SqlCommand("Select Top 1 billno from R7BHInward order by Billno desc", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    int value = int.Parse(dr["billno"].ToString());
                    value = value + 1;
                    bill = value.ToString();
                }
                if (bill == "")
                {
                    bill = "1";
                }
                conn.Close();
                foreach (GridViewRow g1 in GridView1.Rows)
                {
                    SqlCommand cmd2 = new SqlCommand("Select IsNull(Quantity,0) from AddItem where Brand='" + g1.Cells[3].Text + "' and Product='" + g1.Cells[4].Text + "' and Category='" + g1.Cells[5].Text + "' and Size='" + g1.Cells[6].Text + "'", conn);
                    conn.Open();
                    int availble = Convert.ToInt32(cmd2.ExecuteScalar());
                    conn.Close();

                    SqlCommand insert = new SqlCommand("Insert into R7BHInward(Date,BranchName,Tobranch,Brand,product,category,size,quantity,billno) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + bill.ToString() + "')", conn);
                    conn.Open();
                    insert.ExecuteNonQuery();
                    conn.Close();

                    SqlCommand outward = new SqlCommand("Insert into Outward(Date,BranchName,Tobranch,Brand,product,category,size,quantity,billno,Status) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + bill.ToString() + "','0')", conn);
                    conn.Open();
                    outward.ExecuteNonQuery();
                    conn.Close();

                    SqlCommand update = new SqlCommand("Update Additem set Quantity='" + (availble - Convert.ToInt32(g1.Cells[7].Text)) + "' where Brand='" + g1.Cells[3].Text + "' and Product='" + g1.Cells[4].Text + "' and Category='" + g1.Cells[5].Text + "' and Size='" + g1.Cells[6].Text + "'", conn);
                    conn.Open();
                    update.ExecuteNonQuery();
                    conn.Close();

                }
            }
            else if (dropbranchto.SelectedValue == "2")
            {
                string bill = string.Empty;
                SqlConnection conn = new SqlConnection(connstr);
                SqlCommand cmd = new SqlCommand("Select Top 1 billno from R12BHInward order by Billno desc", conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    int value = int.Parse(dr["billno"].ToString());
                    value = value + 1;
                    bill = value.ToString();
                }
                if (bill == "")
                {
                    bill = "1";
                }
                conn.Close();
                foreach (GridViewRow g1 in GridView1.Rows)
                {
                    SqlCommand cmd2 = new SqlCommand("Select IsNull(Quantity,0) from AddItem where Brand='" + g1.Cells[3].Text + "' and Product='" + g1.Cells[4].Text + "' and Category='" + g1.Cells[5].Text + "' and Size='" + g1.Cells[6].Text + "'", conn);
                    conn.Open();
                    int availble = Convert.ToInt32(cmd2.ExecuteScalar());
                    conn.Close();

                    SqlCommand cmd12 = new SqlCommand("Select IsNull(QuR12,0) from AddItem where Brand='" + g1.Cells[3].Text + "' and Product='" + g1.Cells[4].Text + "' and Category='" + g1.Cells[5].Text + "' and Size='" + g1.Cells[6].Text + "'", conn);
                    conn.Open();
                    int availble1 = Convert.ToInt32(cmd12.ExecuteScalar());
                    conn.Close();

                    SqlCommand insert = new SqlCommand("Insert into R12BHInward(Date,BranchName,Tobranch,Brand,product,category,size,quantity,billno) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + bill.ToString() + "')", conn);
                    conn.Open();
                    insert.ExecuteNonQuery();
                    conn.Close();

                    SqlCommand outward = new SqlCommand("Insert into Outward(Date,BranchName,Tobranch,Brand,product,category,size,quantity,billno,Status) values('" + g1.Cells[0].Text + "','" + g1.Cells[1].Text + "','" + g1.Cells[2].Text + "','" + g1.Cells[3].Text + "','" + g1.Cells[4].Text + "','" + g1.Cells[5].Text + "','" + g1.Cells[6].Text + "','" + g1.Cells[7].Text + "','" + bill.ToString() + "','0')", conn);
                    conn.Open();
                    outward.ExecuteNonQuery();
                    conn.Close();

                    SqlCommand update = new SqlCommand("Update Additem set QuR12='" + (availble1 + Convert.ToInt32(g1.Cells[7].Text)) + "', Quantity='" + (availble - Convert.ToInt32(g1.Cells[7].Text)) + "' where Brand='" + g1.Cells[3].Text + "' and Product='" + g1.Cells[4].Text + "' and Category='" + g1.Cells[5].Text + "' and Size='" + g1.Cells[6].Text + "'", conn);
                    conn.Open();
                    update.ExecuteNonQuery();
                    conn.Close();
                }
            }
            Response.Redirect("~/StockTransfer1.aspx");
        }
    }
}