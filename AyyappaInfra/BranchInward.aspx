﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="BranchInward.aspx.cs" Inherits="AyyappaInfra.BranchInward" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Branch Inward
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                             <div class="col-md-3">
                                <div class="form-group">
                                   Select Branch To <asp:DropDownList ID="dropbranch" runat="server" CssClass="form-control" OnSelectedIndexChanged="Branch_Change" AutoPostBack="true">
                                       <asp:ListItem></asp:ListItem>
                               <asp:ListItem Value="0">Road 2 Banjara Hills</asp:ListItem>
                               <asp:ListItem Value="1">Road 7 Banjara Hills</asp:ListItem>
                               <asp:ListItem Value="2">Road 12 Banjara Hills</asp:ListItem>
                                                </asp:DropDownList>
                                    </div>
                                 </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div id="div1" runat="server" visible="false">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                Start Date<asp:TextBox ID="txtstart" CssClass="form-control" runat="server"></asp:TextBox>
                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtstart" />
                                                </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                               End Date <asp:TextBox ID="txtend" CssClass="form-control" runat="server"></asp:TextBox>
                                                 <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtend" />
                                                </div>
                                        </div>
                                     <div class="col-md-3">
                                <div class="form-group">
                                   Search By Category <asp:DropDownList ID="dropcategory" CssClass="form-control" runat="server" OnSelectedIndexChanged="Category_Change" AutoPostBack="true">

                                                      </asp:DropDownList>
                                    </div>
                                         </div>
                                         <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="btnsubmit" CssClass="btn btn-success" OnClick="Submit_Click" runat="server" Text="Submit" />
                                    </div>
                                             </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                              <div class="col-md-12">
                                  <asp:GridView ID="GridBranch" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false">
                                      <Columns>
                                          <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                                           <asp:BoundField DataField="billno" HeaderText="Bill No" />
                                          <asp:BoundField DataField="BranchName" HeaderText="From Branch" />
                                          <asp:BoundField DataField="Brand" HeaderText="Brand" />
                                          <asp:BoundField DataField="product" HeaderText="Product" />
                                          <asp:BoundField DataField="category" HeaderText="Category" />
                                          <asp:BoundField DataField="size" HeaderText="Size" />
                                          <asp:BoundField DataField="quantity" HeaderText="quantity" />
                                        

                                      </Columns>
                                  </asp:GridView>
                                  </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
