﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="VendorsList.aspx.cs" Inherits="AyyappaInfra.VendorsList" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Vendor's List
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="form-group">
                                    Search Vendor<asp:TextBox ID="txtvendor" runat="server" OnTextChanged="Name_Change" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                                     <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtvendor"
                                                MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1"
                                                CompletionInterval="1" ServiceMethod="GetVendor" UseContextKey="True">
                                            </cc1:AutoCompleteExtender>
                                </div>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                            <div class="col-md-12">
                                <asp:GridView ID="GridVendor" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField DataField="Mobile" HeaderText="Mobile No" />
                                        <asp:BoundField DataField="Email" HeaderText="Email Id" />
                                        <asp:BoundField DataField="Address" HeaderText="Address" />
                                        <asp:BoundField DataField="GstDetails" HeaderText="GST No" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary btn-sm" Text="Edit" CommandArgument='<%# Eval("Id") %>' OnClick="Edit" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btndelete" runat="server" CssClass="btn btn-danger btn-sm" Text="Delete" CommandArgument='<%# Eval("Id") %>' OnClientClick="if ( !confirm('Are you sure you want to delete this Vendor?')) return false;" OnClick="delete" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" PopupControlID="Panel1" CancelControlID="btnClose"></cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="300px" Width="650px">
                                 <h3 class="text-center">Update Vendor Details</h3>
                                <hr />
                                 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                <div class="col-md-6">
                                <div class="form-group">
                               <strong> Name</strong><asp:TextBox ID="txtname" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                <div class="col-md-6">
                                <div class="form-group">
                              <strong>Mobile No</strong><asp:TextBox ID="txtmobile" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                <div class="col-md-6">
                                <div class="form-group">
                               <strong> Email </strong><asp:TextBox ID="txtemail" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                <div class="col-md-6">
                                <div class="form-group">
                               <strong> Address </strong><asp:TextBox ID="txtaddress" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                 <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                <div class="col-md-6">
                                <div class="form-group">
                               <strong> GST No </strong><asp:TextBox ID="txtgst" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    </div>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:Button ID="btnUpdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="Update" />
                                    </div>
                                    </div>
                                <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                <asp:Button ID="btnClose" CssClass="btn btn-danger" runat="server" Text="Close" />
                                    </div>
                                    </div>
                            </asp:Panel>
                            <asp:Label ID="lblid" runat="server" Text="" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
