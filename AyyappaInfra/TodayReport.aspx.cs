﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class TodayReport : System.Web.UI.Page
    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["DevkiInterior"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            SqlCommand cmd = new SqlCommand(" With cte as (select Date,CustomerName,Mobile,Billno,Total,ROW_NUMBER() over (partition by billno order by billno desc) as rn from Billing) select * from cte where rn=1 order by billno desc", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand(" With cte as (select Date,CustomerName,Mobile,Billno,Total,ROW_NUMBER() over (partition by billno order by billno desc) as rn from Billing where date between '" + txtstart.Text + "' and '" + txtend.Text + "') select * from cte where rn=1 order by billno desc", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
        protected void View_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string billno = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Select Brand,product,category,size,quantity,Price,amount,Tax,Taxamount,Subtotal,Total,SUM(convert(float,amount)+convert(float,Taxamount)) as totalamt from Billing where Billno='" + billno.ToString() + "' group by Brand,product,category,size,quantity,Price,amount,Tax,Subtotal,Total,Taxamount", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblsubtotal.Text = ds.Tables[0].Rows[0]["Subtotal"].ToString();
                lbltotal.Text = ds.Tables[0].Rows[0]["Total"].ToString();
                Gridpopup.DataSource = ds;
                Gridpopup.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                Gridpopup.DataSource = ds;
                Gridpopup.DataBind();
                int columncount = Gridpopup.Rows[0].Cells.Count;
                Gridpopup.Rows[0].Cells.Clear();
                Gridpopup.Rows[0].Cells.Add(new TableCell());
                Gridpopup.Rows[0].Cells[0].ColumnSpan = columncount;
                Gridpopup.Rows[0].Cells[0].Text = "No Records Found";
            }
            ModalPopupExtender1.Show();
        }
    }
}