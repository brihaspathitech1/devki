﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class DashBoard : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Bindcash();
                Bindcard();
                BindCheque();
                BindTotal();
            }
        }

        protected void Bindcash()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select SUM(isnull(CONVERT(float,Paidamt),0)) as cash from Payment where status='through cash' and back_up is null", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                lblcash.Text = dr["cash"].ToString();
            }
            conn.Close();
        }

        protected void Bindcard()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select SUM(isnull(CONVERT(float,Paidamt),0)) as card from Payment where status='through Card' and back_up is null", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lblcard.Text = dr["card"].ToString();
            }
            conn.Close();
        }

        protected void BindCheque()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select SUM(isnull(CONVERT(float,Paidamt),0)) as cheque from Payment where status='through cheque' and back_up is null", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lblcheque.Text = dr["cheque"].ToString();
            }
            if(lblcheque.Text=="")
            {
                lblcheque.Text = "0";
            }
            conn.Close();
        }

        protected void BindTotal()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select SUM(isnull(CONVERT(float,Paidamt),0)) as total from payment Where back_up is null", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                lbltotal.Text = dr["total"].ToString();
            }
            if (lblcheque.Text == "")
            {
                lbltotal.Text = "0";
            }
            conn.Close();
        }
    }
}