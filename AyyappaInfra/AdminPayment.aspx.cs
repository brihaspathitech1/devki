﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class AdminPayment : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblbill.Text = Session["bill"].ToString();
                lblid.Text = Session["cid"].ToString();
            }
            if (!IsPostBack)
            {
                Bindvalues();
            }
        }

        protected void Payment_Change(object sender, EventArgs e)
        {
            if (radiopayment.SelectedValue == "0")
            {
                cash.Visible = true;
                card.Visible = false;
                cheque.Visible = false;
            }
            else if (radiopayment.SelectedValue == "1")
            {
                cash.Visible = false;
                card.Visible = true;
                cheque.Visible = false;
            }
            else if (radiopayment.SelectedValue == "2")
            {
                cash.Visible = false;
                card.Visible = false;
                cheque.Visible = true;
            }
            btnsubmit.Visible = true;
        }

        //Binding Total value based on bill no
        protected void Bindvalues()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select Total from Billing where Billno='" + lblbill.Text + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                txttotal.Text = dr["Total"].ToString();
                txtamt.Text = dr["Total"].ToString();
                total.Text = dr["Total"].ToString();
            }
        }

        //Calculation for remaining amount
        protected void cashPaid_Change(object sender, EventArgs e)
        {
            double value = double.Parse(txttotal.Text) - double.Parse(txtpaid.Text);
            txtremain.Text = value.ToString();
        }

        protected void CardPaid_Change(object sender, EventArgs e)
        {
            double value = double.Parse(txtamt.Text) - double.Parse(txtcardpaid.Text);
            txtreamt.Text = value.ToString();
        }

        protected void ChequePaid(object sender, EventArgs e)
        {
            double value = double.Parse(total.Text) - double.Parse(paid.Text);
            remain.Text = value.ToString();
        }

        //Inserting into Payment Table
        protected void Payment_Click(object sender, EventArgs e)
        {
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            SqlConnection conn = new SqlConnection(connstr);
            if (radiopayment.SelectedValue == "0")
            {
                if (txtpaid.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Insert Into Payment(Date,CustomerID,Totalamt,Paidamt,Remainamt,status,Billno,username) values('" + date.ToString() + "','" + lblid.Text + "','" + txttotal.Text + "','" + txtpaid.Text + "','" + txtremain.Text + "','through cash','" + lblbill.Text + "','" + Session["Empname"].ToString() + "')", conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    SqlCommand cmd2 = new SqlCommand("Insert Into PaymentStatus(Date,CustomerID,Totalamt,Paidamt,Remainamt,status,Billno,username) values('" + date.ToString() + "','" + lblid.Text + "','" + txttotal.Text + "','" + txtpaid.Text + "','" + txtremain.Text + "','through cash','" + lblbill.Text + "','" + Session["Empname"].ToString() + "')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();
                }
                else if (txtpaid.Text == "")
                {
                    string message = "alert('Paid Amount id mandatory...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
            else if (radiopayment.SelectedValue == "1")
            {
                if (txtcard.Text != "" && txtcardpaid.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Insert Into Payment(Date,CustomerID,Totalamt,Paidamt,Remainamt,Cardno,status,Billno,username) values('" + date.ToString() + "','" + lblid.Text + "','" + txtamt.Text + "','" + txtcardpaid.Text + "','" + txtreamt.Text + "','" + txtcard.Text + "','through Card','" + lblbill.Text + "','" + Session["Empname"].ToString() + "')", conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    SqlCommand cmd2 = new SqlCommand("Insert Into PaymentStatus(Date,CustomerID,Totalamt,Paidamt,Remainamt,Cardno,status,Billno,username) values('" + date.ToString() + "','" + lblid.Text + "','" + txtamt.Text + "','" + txtcardpaid.Text + "','" + txtreamt.Text + "','" + txtcard.Text + "','through Card','" + lblbill.Text + "','" + Session["Empname"].ToString() + "')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();
                }
                else if (txtcard.Text == "" && txtcardpaid.Text == "")
                {
                    string message = "alert('Card number and Paid amount are mandatory...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
            else if (radiopayment.SelectedValue == "2")
            {
                if (txtcheque.Text != "" && paid.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Insert Into Payment(Date,CustomerID,Totalamt,Paidamt,Remainamt,Chequeno,status,Billno,username) values('" + date.ToString() + "','" + lblid.Text + "','" + total.Text + "','" + paid.Text + "','" + remain.Text + "','" + txtcheque.Text + "','through Cheque','" + lblbill.Text + "','" + Session["Empname"].ToString() + "')", conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();

                    SqlCommand cmd2 = new SqlCommand("Insert Into PaymentStatus(Date,CustomerID,Totalamt,Paidamt,Remainamt,Chequeno,status,Billno,username) values('" + date.ToString() + "','" + lblid.Text + "','" + total.Text + "','" + paid.Text + "','" + remain.Text + "','" + txtcheque.Text + "','through Cheque','" + lblbill.Text + "','" + Session["Empname"].ToString() + "')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();

                }
                else if (txtcheque.Text == "" && paid.Text == "")
                {
                    string message = "alert('Cheque number and Paid amount are mandatory...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
            Session["Bill"] = lblbill.Text;
            Response.Redirect("~/PaymentBill.aspx");
        }
    }
}