﻿using System;
using System.Data;
using System.Data.SqlClient;
using devki.DataConnection;

namespace devki.DataAccessLayer
{
    class DataQueries
    {

        #region default Constructor
        public DataQueries()
        {
            //Constructor
        }
        #endregion

        #region Select Count
        public static double SelectCount(string strQuery)
        {
            DataSet ds = new DataSet();
            SqlConnection con = dataconnection.GetConnection();
            SqlDataAdapter adap = new SqlDataAdapter(strQuery, con);
            adap.Fill(ds);
            if (ds.Tables[0].Rows.Count == 0)
            {
                return 0;
            }
            else
            {
                Double count;
                string s = Convert.ToString(ds.Tables[0].Rows[0][0].ToString());
                if (s != "")
                {
                    count = Double.Parse(s);
                }
                else { count = 0; }

                return count;
            }
        }

        #endregion

        #region Select Common
        public static DataSet SelectCommon(string strQuery)
        {

            DataSet ds = new DataSet();
            SqlConnection con = dataconnection.GetConnection();
            SqlDataAdapter adap = new SqlDataAdapter(strQuery, con);
            adap.Fill(ds);
            return ds;

        }

        #endregion

        #region Common Insert
        public static void InsertCommon(string strQuery)
        {
            SqlConnection con = dataconnection.GetConnection();
            SqlCommand command = new SqlCommand(strQuery);
            command.Connection = con;
            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }
        #endregion

        #region Common Delete
        public static void DeleteCommon(string strQuery)
        {
            SqlConnection con = dataconnection.GetConnection();
            SqlCommand command = new SqlCommand(strQuery);
            command.Connection = con;
            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }
        #endregion

        #region Common Update
        public static void UpdateCommon(string strQuery)
        {
            SqlConnection con = dataconnection.GetConnection();
            SqlCommand command = new SqlCommand(strQuery);
            command.Connection = con;
            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }
        #endregion

        #region Get Login ID
        public static DataSet GetloginID(string strQuery)
        {
            DataSet dsLoginId = new DataSet();
            SqlConnection con = dataconnection.GetConnection();
            SqlDataAdapter adap = new SqlDataAdapter(strQuery, con);
            adap.Fill(dsLoginId);
            return dsLoginId;

        }
        #endregion
    }

}

