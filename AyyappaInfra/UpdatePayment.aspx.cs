﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace AyyappaInfra
{
    public partial class UpdatePayment : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lblbill.Text = Session["bill"].ToString();
            }
            if(!IsPostBack)
            {
                BindValues();
            }
        }

        protected void Payment_Change(object sender, EventArgs e)
        {
            if (radiopayment.SelectedValue == "0")
            {
                cash.Visible = true;
                card.Visible = false;
                cheque.Visible = false;
            }
            else if (radiopayment.SelectedValue == "1")
            {
                cash.Visible = false;
                card.Visible = true;
                cheque.Visible = false;
            }
            else if (radiopayment.SelectedValue == "2")
            {
                cash.Visible = false;
                card.Visible = false;
                cheque.Visible = true;
            }
            btnsubmit.Visible = true;
        }

        protected void BindValues()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from Payment where Billno='" + lblbill.Text + "' and status1='" + Session["status"].ToString() + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                lblid.Text = dr["CustomerID"].ToString();
                txttotal.Text = dr["Totalamt"].ToString();
                txtpaid.Text = dr["Paidamt"].ToString();
                txtremain.Text = dr["Remainamt"].ToString();
                txtamt.Text = dr["Totalamt"].ToString();
                txtcardpaid.Text = dr["Paidamt"].ToString();
                txtreamt.Text = dr["Remainamt"].ToString();
                total.Text = dr["Totalamt"].ToString();
                paid.Text = dr["Paidamt"].ToString();
                remain.Text = dr["Remainamt"].ToString();
            }
            conn.Close();
        }
        protected void Cash_Change(object sender,EventArgs e)
        {
            double value = double.Parse(txtremain.Text) - double.Parse(txtppaid.Text);
            txtpremain.Text = value.ToString();
        }

        protected void Card_Change(object sender,EventArgs e)
        {
            double value = double.Parse(txtreamt.Text) - double.Parse(txtcppaid.Text);
            txtcremain.Text = value.ToString();
        }

        protected void Cheque_Change(object sender,EventArgs e)
        {
            double value = double.Parse(remain.Text) - double.Parse(txtchpaid.Text);
            txtchremain.Text = value.ToString();
        }

        protected void Payment_Click(object sender,EventArgs e)
        {
            string date = System.DateTime.Now.ToString("yyyy-MM-dd");
            SqlConnection conn = new SqlConnection(connstr);
            if (radiopayment.SelectedValue == "0")
            {
                if(txtppaid.Text!="")
                {
                    SqlCommand cmd = new SqlCommand("Update Payment set Paidamt='" + (double.Parse(txtpaid.Text) + double.Parse(txtppaid.Text)) + "',Remainamt='" + txtpremain.Text + "' where billno='" + lblbill.Text + "' and status1='"+ Session["status"].ToString() + "'",conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    SqlCommand cmd2 = new SqlCommand("Insert Into PaymentStatus(Date,CustomerID,Totalamt,Paidamt,Remainamt,status,Billno,username,Status1) values('" + date.ToString() + "','" + lblid.Text + "','" + txttotal.Text + "','" + txtppaid.Text + "','" + txtpremain.Text + "','through cash','" + lblbill.Text + "','" + Session["Empname"].ToString() + "','0')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();
                }
                else if (txtppaid.Text == "")
                {
                    string message = "alert('Paid Amount id mandatory...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
            else if (radiopayment.SelectedValue == "1")
            {
                if (txtcard.Text != "" && txtcppaid.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Update Payment set Paidamt='" + (double.Parse(txtcardpaid.Text) + double.Parse(txtcppaid.Text)) + "',Remainamt='" + txtcremain.Text + "' where billno='" + lblbill.Text + "' and Status1='"+ Session["status"].ToString() + "'",conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    SqlCommand cmd2 = new SqlCommand("Insert Into PaymentStatus(Date,CustomerID,Totalamt,Paidamt,Remainamt,status,Billno,username,Status1) values('" + date.ToString() + "','" + lblid.Text + "','" + txttotal.Text + "','" + txtcppaid.Text + "','" + txtcremain.Text + "','through card','" + lblbill.Text + "','" + Session["Empname"].ToString() + "','0')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();
                }
                else if (txtcard.Text == "" && txtcppaid.Text == "")
                {
                    string message = "alert('Card number and Paid amount are mandatory...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
            else if (radiopayment.SelectedValue == "2")
            {
                if (txtcheque.Text != "" && txtchpaid.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Update Payment set Paidamt='" + (double.Parse(paid.Text) + double.Parse(txtchpaid.Text)) + "',Remainamt='" + txtcremain.Text + "' where billno='" + lblbill.Text + "' and status1='"+ Session["status"].ToString() + "'",conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    SqlCommand cmd2 = new SqlCommand("Insert Into PaymentStatus(Date,CustomerID,Totalamt,Paidamt,Remainamt,status,Billno,username,Status1) values('" + date.ToString() + "','" + lblid.Text + "','" + txttotal.Text + "','" + txtchpaid.Text + "','" + txtchremain.Text + "','through Cheque','" + lblbill.Text + "','" + Session["Empname"].ToString() + "','0')", conn);
                    conn.Open();
                    cmd2.ExecuteNonQuery();
                    conn.Close();
                }
                else if (txtcheque.Text == "" && paid.Text == "")
                {
                    string message = "alert('Cheque number and Paid amount are mandatory...!')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
            }
            Session["Bill"] = lblbill.Text;
            Response.Redirect("~/CashReport.aspx");
        }
    }
}