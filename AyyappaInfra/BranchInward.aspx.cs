﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class BranchInward : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindCategory();
            }
        }

        protected void BindCategory()
        {
            
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    // cmd.CommandText = "Select * from Category where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "'";
                    cmd.CommandText = "Select * from Category order by Category asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropcategory.DataSource = dr;
                    dropcategory.DataTextField = "Category";
                    dropcategory.DataValueField = "Id";
                    dropcategory.DataBind();
                    dropcategory.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        protected void Branch_Change(object sender,EventArgs e)
        {
            div1.Visible = true;
            SqlConnection conn = new SqlConnection(connstr);
            DataSet ds = new DataSet();
            if (dropbranch.SelectedValue == "0")
            {
                SqlCommand cmd = new SqlCommand("Select * from R2BHInward order by id desc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            else if(dropbranch.SelectedValue=="1")
            {
                SqlCommand cmd = new SqlCommand("Select * from R7BHInward order by id desc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            else if(dropbranch.SelectedValue=="2")
            {
                SqlCommand cmd = new SqlCommand("Select * from R12BHInward order by id desc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridBranch.DataSource = ds;
                GridBranch.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridBranch.DataSource = ds;
                GridBranch.DataBind();
                int columncount = GridBranch.Rows[0].Cells.Count;
                GridBranch.Rows[0].Cells.Clear();
                GridBranch.Rows[0].Cells.Add(new TableCell());
                GridBranch.Rows[0].Cells[0].ColumnSpan = columncount;
                GridBranch.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
        protected void Category_Change(object sender,EventArgs e)
        {

           /* div1.Visible = true;
            SqlConnection conn = new SqlConnection(connstr);
            DataSet ds = new DataSet();
            if (dropbranch.SelectedValue == "0"&& dropcategory.SelectedItem.Text!="")
            {
                SqlCommand cmd = new SqlCommand("Select * from R2BHInward where Category='"+dropcategory.SelectedItem.Text+"' order by id desc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            else if (dropbranch.SelectedValue == "1"&& dropcategory.SelectedItem.Text!="")
            {
                SqlCommand cmd = new SqlCommand("Select * from R7BHInward where Category='" + dropcategory.SelectedItem.Text + "' order by id desc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            else if (dropbranch.SelectedValue == "2"&& dropcategory.SelectedItem.Text!="")
            {
                SqlCommand cmd = new SqlCommand("Select * from R12BHInward where Category='" + dropcategory.SelectedItem.Text + "' order by id desc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridBranch.DataSource = ds;
                GridBranch.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridBranch.DataSource = ds;
                GridBranch.DataBind();
                int columncount = GridBranch.Rows[0].Cells.Count;
                GridBranch.Rows[0].Cells.Clear();
                GridBranch.Rows[0].Cells.Add(new TableCell());
                GridBranch.Rows[0].Cells[0].ColumnSpan = columncount;
                GridBranch.Rows[0].Cells[0].Text = "No Records Found";
            }*/
        }


        protected void Submit_Click(object sender,EventArgs e)
        {
            div1.Visible = true;
            SqlConnection conn = new SqlConnection(connstr);
            DataSet ds = new DataSet();
            if (txtstart.Text==""&&txtend.Text=="")
            {
                if (dropbranch.SelectedValue == "0" && dropcategory.SelectedItem.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Select * from R2BHInward where Category='" + dropcategory.SelectedItem.Text + "' order by id desc", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
                else if (dropbranch.SelectedValue == "1" && dropcategory.SelectedItem.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Select * from R7BHInward where Category='" + dropcategory.SelectedItem.Text + "' order by id desc", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
                else if (dropbranch.SelectedValue == "2" && dropcategory.SelectedItem.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Select * from R12BHInward where Category='" + dropcategory.SelectedItem.Text + "' order by id desc", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
            }
            else
            {
                if (dropbranch.SelectedValue == "0" && dropcategory.SelectedItem.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Select * from R2BHInward where Category='" + dropcategory.SelectedItem.Text + "' and (Date between ('"+txtstart.Text+"') and ('"+txtend.Text+"')) order by id desc", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
                else if (dropbranch.SelectedValue == "1" && dropcategory.SelectedItem.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Select * from R7BHInward where Category='" + dropcategory.SelectedItem.Text + "' and (Date between ('" + txtstart.Text + "') and ('" + txtend.Text + "')) order by id desc", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
                else if (dropbranch.SelectedValue == "2" && dropcategory.SelectedItem.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Select * from R12BHInward where Category='" + dropcategory.SelectedItem.Text + "' and (Date between ('" + txtstart.Text + "') and ('" + txtend.Text + "')) order by id desc", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridBranch.DataSource = ds;
                GridBranch.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridBranch.DataSource = ds;
                GridBranch.DataBind();
                int columncount = GridBranch.Rows[0].Cells.Count;
                GridBranch.Rows[0].Cells.Clear();
                GridBranch.Rows[0].Cells.Add(new TableCell());
                GridBranch.Rows[0].Cells[0].ColumnSpan = columncount;
                GridBranch.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}