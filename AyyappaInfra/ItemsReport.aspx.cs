﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class ItemsReport : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindCategory();
                BindGrid();
            }
        }
        protected void BindCategory()
        {
            using (SqlConnection conn = new SqlConnection(connstr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    // cmd.CommandText = "Select * from Category where Brand='" + dropbrand.SelectedValue + "' and Product='" + dropproduct.SelectedValue + "'";
                    cmd.CommandText = "Select * from Category order by Category asc";
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dropcategory.DataSource = dr;
                    dropcategory.DataTextField = "Category";
                    dropcategory.DataValueField = "Id";
                    dropcategory.DataBind();
                    dropcategory.Items.Insert(0, new ListItem("", "0"));
                    conn.Close();
                }
            }
        }
        //Binding Items from AddItems table
        protected void BindGrid()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from AddItem order by Product asc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if(ds.Tables[0].Rows.Count>0)
            {
                griditems.DataSource = ds;
                griditems.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                griditems.DataSource = ds;
                griditems.DataBind();
                int columncount = griditems.Rows[0].Cells.Count;
                griditems.Rows[0].Cells.Clear();
                griditems.Rows[0].Cells.Add(new TableCell());
                griditems.Rows[0].Cells[0].ColumnSpan = columncount;
                griditems.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Edit_Click(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from AddItem where id='" + lblid.Text + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.Read())
            {
                txtbrand.Text = dr["Brand"].ToString();
                txtproduct.Text = dr["Product"].ToString();
                txtcategory.Text = dr["Category"].ToString();
                txtsize.Text = dr["size"].ToString();
                txtquantity.Text = dr["Quantity"].ToString();    
            }
            conn.Close();
            ModalPopupExtender1.Show();
        }

        protected void Update_Click(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Update AddItem set Brand='" + txtbrand.Text + "',Product='" + txtproduct.Text + "',Category='" + txtcategory.Text + "',Size='" + txtsize.Text + "',Quantity='"+txtquantity.Text+"' where id='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid();
            dropcategory.SelectedValue = null;
        }

        protected void Delete_Click(object sender,EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("delete from AddItem where id='" + lblid.Text + "'", conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            BindGrid();
            dropcategory.SelectedValue = null;
        }

        protected void Category_Change(object sender,EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from AddItem where category='"+dropcategory.SelectedItem.Text+"' order by Product asc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                griditems.DataSource = ds;
                griditems.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                griditems.DataSource = ds;
                griditems.DataBind();
                int columncount = griditems.Rows[0].Cells.Count;
                griditems.Rows[0].Cells.Clear();
                griditems.Rows[0].Cells.Add(new TableCell());
                griditems.Rows[0].Cells[0].ColumnSpan = columncount;
                griditems.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}