﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Viewbill.aspx.cs" Inherits="AyyappaInfra.Viewbill" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .btn-success {
            background-color: #00a65a;
            border-color: #008d4c;
        }

        .btn {
            border-radius: 3px;
            -webkit-box-shadow: none;
            box-shadow: none;
            border: 1px solid transparent;
        }
    </style>
    <style>
        .nav-tabs > li.active > a,
        .nav-tabs > li.active > a:focus,
        .nav-tabs > li.active > a:hover {
            padding: 10px 0;
            margin-left: 15px;
            margin-right: 15px;
            border-bottom: 3px solid #2fa3e6;
            border-left: 0;
            border-top: 0;
            border-right: 0;
            border-radius: 0;
        }

        .borderless td, .borderless th {
            border: none;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        }

            page[size="A4"] {
                width: 21cm;
                height: 29.7cm;
            }

        }

        @media print {
            body, page {
                margin: 0;
                box-shadow: 0;
            }


            .pagebreak {
                page-break-after: always;
            }
        }

        .borderless td, .borderless th {
            border: none;
        }

        .hritem {
            margin-top: 0px;
            margin-bottom: 9px;
            border: 0;
            border-top: 1px solid #d6f4ff;
        }

        table {
            border: none;
        }

        @media print {

            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }

            .col-sm-12 {
                width: 100%;
            }
             .pagebreak {page-break-after: always !important;}
            .col-sm-11 {
                width: 91.66666666666666%;
            }

            .col-sm-10 {
                width: 83.33333333333334%;
            }

            .col-sm-9 {
                width: 75%;
            }

            .col-sm-8 {
                width: 66.66666666666666%;
            }

            .col-sm-7 {
                width: 58.333333333333336%;
            }

            .col-sm-6 {
                width: 50%;
            }

            .col-sm-5 {
                width: 41.66666666666667%;
            }

            .col-sm-4 {
                width: 33.33333333333333%;
            }

            .col-sm-3 {
                width: 25%;
            }

            .col-sm-2 {
                width: 16.666666666666664%;
            }

            .col-sm-1 {
                width: 8.333333333333332%;
            }

            #btnPrint {
                display: none !important;
            }

            #btnpdf {
                display: none !important;
            }

            #btnback {
                display: none !important;
            }

            table {
                border: none;
            }


            .vendorListHeading {
                background: #1a4567 !important;
                color: white;
            }


                .vendorListHeading th {
                    color: white;
                }
        }





        .vendorListHeading {
            background: #1a4567 !important;
            color: white;
        }

            .vendorListHeading th {
                color: white;
            }

        .dcprint p {
            font-size: 11px !important;
        }

        /* Styles for print (include this after the above) */
        #print_helper {
            display: block;
            overflow: visible;
            margin-left: -37px;
            white-space: pre;
            white-space: pre-wrap;
        }

        .the_textarea {
            display: none;
        }
    </style>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            function copy_to_print_helper() {
                $('#print_helper').text($('.the_textarea').val());
            }
            $('.the_textarea').bind('keydown keyup keypress cut copy past blur change', function () {
                copy_to_print_helper(); // consider debouncing this to avoid slowdowns!
            });
            copy_to_print_helper(); // on initial page load
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <input type="button" id="btnPrint" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" style="background: #5cb85c; padding: 9px 8px; border-radius: 4px; color: #fff;" />
           <%-- <asp:Button ID="btnpdf" runat="server" Text="PDF" OnClick="pdf_Click" CssClass="btn btn btn-success" Style="background: #5cb85c; padding: 9px 8px; border-radius: 4px; color: #fff;" />--%>
            <%--<a href="BranchCustomer.aspx" id="btnback" type="button" onclick="goBack()"><strong style="background: #5cb85c; padding: 9px 8px; border-radius: 4px; color: #fff;">Go Back</strong></a>--%>
          <div style="min-height:441px">
            
            <table width="100%">
                <tr>
                    <td width="33.33%">
                        <div style="float:left">
            <div id="divr12" runat="server" visible="false" style="text-align:left">
                <h3 style="text-align: left">DEVKI INTERIORS </h3>
                <h5 style="text-align: left">Specialist in: Furniture, Mattresses, Interior Work<br />
                    Shop No. 8-2-686/D/B-4/1, Road No. 12, Banjara Hills, Hyderabad - 500 038.<br />
                    Off No: 040-65162215, Cell No: 9989062215/8121110081<br />
                    <%--GST NO:36APFPB4337P1ZO<br />--%>
                    E-mail: devki_interior@yahoo.com</h5>
            </div>
            <div id="divr7" runat="server" visible="false" style="text-align:left">
                <h3 style="text-align: left">SHARMA FURNITURE</h3>
                <h3 style="text-align: left">Specialist in: Carved & Plain Furniture, Mattresses & Interior Work<br />
                    H.No. 8-2-548/44, Road No. 7, Banjara Hills, Hyderabad - 500 034. Telangana.<br />
                    Off No:040-23352214, Cell No: 9989062215<br />
                  <%--  GST No:36AAXFS6224K1ZH<br />--%>
                    E-mail: Sharma_furnituresnb@yahoo.co.in</h3>
            </div>
            <div id="divr2" runat="server" visible="false" style="text-align:left"> 
                <h3 style="text-align: left">DEVKI INTERIORS </h3>
                <h5 style="text-align: left">Specialist in: Furniture, Mattresses, Interior Work<br />
                    Shop No. 8-2-273/276/277, Pavani Indra Dhanush Complex, Ground Floor<br />
                    Road No. 2, Banjara Hills, Hyderabad - 500 038.<br />
                   
                    Off No: 040-65162216, Cell No:9989062215<br />
                   <%-- GST NO:36APFPB4337P1ZO<br />--%>
                    E-mail: devki_interior@yahoo.com</h5>
            </div>
            </div>

                    </td>
                    
                       <td width="33.33%">  <h3 style="text-align: center">TAX INVOICE</h3></td>
                    <td width="33.33%">
                        <div style="">
                    <div style="float: right;text-align: right">
                Date:<asp:Label ID="lbldate" runat="server" Text="Label"></asp:Label>
                <br />
                Invoice No:<asp:Label ID="lblbill" runat="server" Text="Label"></asp:Label>
                 <br />
                Customer Name:<asp:Label ID="CustName" runat="server" Text=""></asp:Label>
                <br />
                Mobile No:<asp:Label ID="Mobile" runat="server" Text=""></asp:Label>
                <br />
                Address:<asp:Label ID="Address" runat="server" Text=""></asp:Label>
                <br />
                GST No:<asp:Label ID="Gst" runat="server" Text=""></asp:Label>
            </div>

            </div>

                    </td>


                </tr>


            </table>


            
            
            <div style="float: left; width: 50%">
                <asp:GridView ID="GridCustomer" runat="server" GridLines="None" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Customer Details">
                            <ItemTemplate>
                                <%# Eval("Name") +"<br/>"+ Eval("mobile")+"<br/>"+ Eval("Email")+"<br/>"+Eval("Address")+"<br/>"+Eval("GstDetails") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        
            <div style="clear: both"></div>
            <asp:GridView ID="GridItems" runat="server" Width="100%" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="Brand" HeaderText="Brand" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="product" HeaderText="product" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="category" HeaderText="category" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="size" HeaderText="size" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="quantity" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="Price" HeaderText="Price" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="Tax" HeaderText="Tax" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="amount" HeaderText="Amount" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                </Columns>
            </asp:GridView>

            <div style="text-align: right; width: 100%">
                Sub Total: &nbsp &nbsp &nbsp
                <asp:Label ID="lblsub" runat="server" Text="Label"></asp:Label><br />
                CGST: &nbsp &nbsp &nbsp &nbsp
                <asp:Label ID="lblcgst" runat="server" Text="Label"></asp:Label><br />
                SGST: &nbsp &nbsp &nbsp &nbsp
                <asp:Label ID="lblsgst" runat="server" Text="Label"></asp:Label><br />
                IGST 18: &nbsp &nbsp &nbsp &nbsp
                <asp:Label ID="lbli18" runat="server" Text="Label"></asp:Label><br />
                IGST 28: &nbsp &nbsp &nbsp &nbsp
                <asp:Label ID="lbli28" runat="server" Text="Label"></asp:Label><br />
                Total: &nbsp &nbsp &nbsp
                <asp:Label ID="lbltotal" runat="server" Text="Label"></asp:Label><br />
                Advance Amount:&nbsp &nbsp &nbsp
                <asp:Label ID="lbladvance" runat="server" Text="Label"></asp:Label><br />
                Balance Amount:&nbsp &nbsp &nbsp
                <asp:Label ID="lblbalance" runat="server" Text="Label"></asp:Label>
                <br />
                <br />
                <br />
                For <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
            </div>
            <div style="margin-top:-105px">
                <strong>Terms & Conditions:</strong><br />
                Goods once sold will not be taken back or excahnged<br />
                Hyderabad Jurisdiction Only<br />
                GST IN:<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>&nbsp &nbsp PAN NO:<asp:Label ID="lblpan" runat="server" Text="Label"></asp:Label>
            </div>

            </div>


      <%--  <div   style="page-break-before: always !important">--%>
        <div style="page-break-before: always !important">
                         <table width="100%">
                <tr>
                    <td width="33.33%">
                        
            <div id="divr122" runat="server" visible="false">
                <h3 style="text-align: left">DEVKI INTERIORS </h3>
                <h5 style="text-align: left">Specialist in: Furniture, Mattresses, Interior Work<br />
                    Shop No. 8-2-686/D/B-4/1, Road No. 12, Banjara Hills, Hyderabad.<br />
                    Off No: 040-65162215, Cell No: 9989062215/8121110081<br />
                   <%-- GST NO:36APFPB4337P1ZO<br />--%>
                    E-mail: devki_interior@yahoo.com</h5>
            </div>
            <div id="r7" runat="server" visible="false">
                <h3 style="text-align: left">SHARMA FURNITURE</h3>
                <h5 style="text-align: left">Specialist in: Carved & Plain Furniture, Mattresses & Interior Work<br />
                    H.No. 8-2-548/44, Road No. 7, Banjara Hills, Hyderabad - 500 034. Telangana.<br />
                    Off No:040-23352214, Cell No: 9989062215<br />
                   <%-- GST No:36AAXFS6224K1ZH<br />--%>
                    E-mail: Sharma_furnituresnb@yahoo.co.in</h5>
            </div>
            <div id="r2" runat="server" visible="false">
                <h3 style="text-align: left">DEVKI INTERIORS </h3>
                <h5 style="text-align: left">Specialist in: Furniture, Mattresses, Interior Work<br />
                    Shop No. 8-2-273/276/277, Pavani Indra Dhanush Complex, Ground Floor<br />
                    Road No. 2, Banjara Hills, Hyderabad - 500 038.<br />                   
                    Off No: 040-65162216, Cell No:9989062215<br />
                 <%--   GST NO:36APFPB4337P1ZO<br />--%>
                    E-mail: devki_interior@yahoo.com</h5>
            </div>
         

                    </td>

                    <td width="33.33%">  <h3 style="text-align: center">TAX INVOICE</h3></td>

                    <td width="33.33%">
                        <div style="">
                    <div style="float: right;text-align: right">
                Date:<asp:Label ID="lbldate2" runat="server" Text=""></asp:Label>
                <br />
                Invoice No:<asp:Label ID="lblbill2" runat="server" Text=""></asp:Label>
                <br />
                Customer Name:<asp:Label ID="lblname" runat="server" Text=""></asp:Label>
                <br />
                Mobile No:<asp:Label ID="lblmobile" runat="server" Text=""></asp:Label>
                <br />
                Address:<asp:Label ID="lbladdress" runat="server" Text=""></asp:Label>
                <br />
                GST No:<asp:Label ID="lblGst" runat="server" Text=""></asp:Label>
            </div>
                            </div>
                    </td>

                </tr>
             </table>

 
           
            <div style="float: left; width: 50%">
                <asp:GridView ID="GridCust" runat="server" GridLines="None" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Customer Details">
                            <ItemTemplate>
                                <%# Eval("Name") +"<br/>"+ Eval("mobile")+"<br/>"+ Eval("Email")+"<br/>"+Eval("Address")+"<br/>"+Eval("GstDetails") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

       
            <div style="clear: both"></div>

            <asp:GridView ID="GridItems2" runat="server" Width="100%" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="Brand" HeaderText="Brand" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="product" HeaderText="product" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="category" HeaderText="category" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="size" HeaderText="size" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="quantity" HeaderText="Qty" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="Price" HeaderText="Price" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="Tax" HeaderText="Tax" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="amount" HeaderText="Amount" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                </Columns>
            </asp:GridView>
            <div style="text-align: right; width: 100%">
                SubTotal: &nbsp &nbsp &nbsp
                <asp:Label ID="lblsubtotal" runat="server" Text="Label"></asp:Label><br />
                CGST: &nbsp &nbsp &nbsp &nbsp
                <asp:Label ID="lblcgst2" runat="server" Text="Label"></asp:Label><br />
                SGST: &nbsp &nbsp &nbsp &nbsp
                <asp:Label ID="lblsgst2" runat="server" Text="Label"></asp:Label><br />
                 IGST 18: &nbsp &nbsp &nbsp &nbsp
                <asp:Label ID="lbli182" runat="server" Text="Label"></asp:Label><br />
                IGST 28: &nbsp &nbsp &nbsp &nbsp
                <asp:Label ID="lbli282" runat="server" Text="Label"></asp:Label><br />
                Total: &nbsp &nbsp &nbsp
                <asp:Label ID="lbltotal2" runat="server" Text="Label"></asp:Label><br />
                Advance Amount:&nbsp &nbsp &nbsp
                <asp:Label ID="advance" runat="server" Text="Label"></asp:Label><br />
                Balance Amount:&nbsp &nbsp &nbsp
                <asp:Label ID="balance" runat="server" Text="Label"></asp:Label>
                 <br />
                <br />
                <br />
                For <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
            </div>
            <div style="margin-top:-105px">
                <strong>Terms & Conditions:</strong><br />
                Goods once sold will not be taken back or excahnged<br />
                Hyderabad Jurisdiction Only<br />
                GST IN:<asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>&nbsp &nbsp PAN NO:<asp:Label ID="lblpan2" runat="server" Text="Label"></asp:Label>
            </div>
            <asp:Label ID="lblbranch" runat="server" Text="" Visible="false"></asp:Label>
             <asp:Label ID="lblcid" runat="server" Text="" Visible="false"></asp:Label>
        </div>
    <%--</div>--%>
    </form>
</body>
</html>
