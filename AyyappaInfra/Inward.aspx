﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Inward.aspx.cs" Inherits="AyyappaInfra.Inward" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .style1 {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Inward
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>


                                    <div class="col-md-3">
                                        <div class="form-group">
                                            Date<asp:TextBox ID="txtdate" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            Vendor Name<span class="style1">*</span><asp:DropDownList ID="dropvendor" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>                          
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-1" style="padding-top: 23px">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal5">+</button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            HSN No<asp:TextBox ID="txthsn" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                              <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                    Invoice No<asp:TextBox ID="txtInv" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                  </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                           Invoice Date<asp:TextBox ID="txtInvDate" CssClass="form-control" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtInvDate" runat="server" />
                                             </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            Brand<span class="style1">*</span><asp:DropDownList ID="dropbrand" CssClass="form-control" runat="server" OnSelectedIndexChanged="Brand_Change" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                           
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-1" style="padding-top: 23px">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal1">+</button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            Product<span class="style1">*</span><asp:DropDownList ID="dropproduct" CssClass="form-control" runat="server" OnSelectedIndexChanged="Product_Change" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-1" style="padding-top: 23px">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal2">+</button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            Category<span class="style1">*</span><asp:DropDownList ID="dropcategory" CssClass="form-control" runat="server" OnSelectedIndexChanged="Category_Change" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-1" style="padding-top: 23px">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal3">+</button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            Size<span class="style1">*</span><asp:DropDownList ID="dropsize" CssClass="form-control" runat="server" OnSelectedIndexChanged="Size_Change" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-1" style="padding-top: 23px">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal4">+</button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            Available<asp:TextBox ID="txtavailable" CssClass="form-control" runat="server" Text="0" ReadOnly="true"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator31" ControlToValidate="txtavailable"
                                                runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                <ContentTemplate>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            Quantity<span class="style1">*</span><asp:TextBox ID="txtqty" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtqty"
                                                runat="server" ErrorMessage="Required" Style="color: #FF0000"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            Price per Unit<span class="style1">*</span><asp:TextBox ID="txtprice" CssClass="form-control" runat="server" OnTextChanged="Price_Change" AutoPostBack="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <div class="clearfix"></div>
                              <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                   Total Amount<asp:TextBox ID="txtamount" CssClass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                              </div>
                                        </div>
                                    </ContentTemplate>
                                  </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                <ContentTemplate>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            Tax Type<span class="style1">*</span><asp:DropDownList ID="droptaxtype" CssClass="form-control" runat="server">
                                                <asp:ListItem></asp:ListItem>
                                                <asp:ListItem>Include Tax</asp:ListItem>
                                                <asp:ListItem>Exclude Tax</asp:ListItem>
                                                    </asp:DropDownList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            
                            <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                <ContentTemplate>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            Tax<span class="style1">*</span><asp:DropDownList ID="droptax" CssClass="form-control" runat="server">
                                                <asp:ListItem></asp:ListItem>
                                                <asp:ListItem>GST 5%</asp:ListItem>
                                                <asp:ListItem>GST 12%</asp:ListItem>
                                                <asp:ListItem>GST 18%</asp:ListItem>
                                                <asp:ListItem>GST 28%</asp:ListItem>
                                                <asp:ListItem>IGST 5%</asp:ListItem>
                                                <asp:ListItem>IGST 12%</asp:ListItem>
                                                <asp:ListItem>IGST 18%</asp:ListItem>
                                                <asp:ListItem>IGST 28%</asp:ListItem>
                                               </asp:DropDownList>
                                             </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-3" style="padding-top:20px">
                                        <div class="form-group">
                                            <asp:Button ID="btnsubmit" CssClass="btn btn-primary" runat="server" Text="Add" OnClick="Add_Click" CausesValidation="true" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                <ContentTemplate>

                                    <div class="col-md-12">
                                        <asp:GridView ID="GridView1" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false" OnRowDeleting="Deleting">
                                            <Columns>
                                                <asp:BoundField DataField="Date" HeaderText="Date" />
                                                <asp:BoundField DataField="vendor" HeaderText="Vendor" />
                                                <asp:BoundField DataField="hsn" HeaderText="HSN No" />
                                                <asp:BoundField DataField="invno" HeaderText="Inv No" />
                                                <asp:BoundField DataField="invdate" HeaderText="Inv Date" />
                                                <asp:BoundField DataField="Brand" HeaderText="Brand" />
                                                <asp:BoundField DataField="Product" HeaderText="Product" />
                                                <asp:BoundField DataField="Category" HeaderText="Category" />
                                                <asp:BoundField DataField="Size" HeaderText="Size" />
                                                <asp:BoundField DataField="qty" HeaderText="Quantity" />
                                                <asp:BoundField DataField="price" HeaderText="Price" />
                                                <asp:BoundField DataField="amount" HeaderText="Amount" />
                                                <asp:BoundField DataField="tax" HeaderText="Tax" />
                                                <asp:BoundField DataField="taxamt" HeaderText="Taxamt" />
                                                <asp:BoundField DataField="total" HeaderText="Total" />
                                                <asp:CommandField ShowDeleteButton="true" ControlStyle-CssClass="btn btn-danger btn-sm" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                              <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                <ContentTemplate>
                            <div id="div12" runat="server" visible="false">
                                    <div class="col-md-offset-8 col-md-4" style="text-align:right; padding-right:108px" >
                             <div class="col-md-6">Sub Total:</div> <div class="col-md-6"><asp:Label ID="lblstotal" runat="server" Text="0"></asp:Label><br /></div>
                                    
                        
                           <div class="col-md-6"> CGST:</div> <div class="col-md-6"><asp:Label ID="lblc9" runat="server" Text="0"></asp:Label><br /></div>
                                    
                           <div class="col-md-6"> SGST:</div> <div class="col-md-6"><asp:Label ID="lbls9" runat="server" Text="0"></asp:Label><br /></div>
                           <div class="col-md-6"> IGST 5%:</div> <div class="col-md-6"><asp:Label ID="lbli5" runat="server" Text="0"></asp:Label><br /></div>
                          <div class="col-md-6"> IGST 12%:</div> <div class="col-md-6"><asp:Label ID="lbli12" runat="server" Text="0"></asp:Label><br /></div>
                                         <div class="col-md-6"> IGST 18%:</div> <div class="col-md-6"><asp:Label ID="lbli18" runat="server" Text="0"></asp:Label><br /></div>

                           <div class="col-md-6"> IGST 28%:</div> <div class="col-md-6"><asp:Label ID="lbli28" runat="server" Text="0"></asp:Label><br /></div>
                                    
                            <div class="col-md-6">Total:</div> <div class="col-md-6"><asp:Label ID="lbltotal" runat="server" Text="0"></asp:Label></div>
                                </div> 
                                </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                <ContentTemplate>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <asp:Button ID="btninsert" CssClass="btn btn-success" runat="server" Text="Submit" Visible="false" OnClick="Insert_Click" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <div class="modal fade" id="myModal1" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Brand</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                <asp:TextBox ID="txtBrand" runat="server" class="form-control" />
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="producpopup" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Brand_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal2" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Product</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                <asp:TextBox ID="txtproduct" runat="server" class="form-control" />
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="Button1" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Product_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal3" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Category</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                <asp:TextBox ID="txtcategory" runat="server" class="form-control" />
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="Button2" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Category_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal4" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Size</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                <asp:TextBox ID="txtsize" runat="server" class="form-control" />
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="Button3" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Size_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal5" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add Vendor</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                Name<asp:TextBox ID="txtname" runat="server" class="form-control" />
                                            </p>
                                            <p>
                                                Mobile No<asp:TextBox ID="txtmobile" runat="server" class="form-control" />
                                            </p>
                                            <p>
                                                Email Id<asp:TextBox ID="txtemail" runat="server" class="form-control" />
                                            </p>
                                            <p>
                                                Address
                                                <asp:TextBox ID="txtaddress" runat="server" class="form-control" />
                                            </p>
                                            <p>
                                                GST Details
                                                <asp:TextBox ID="txtgst" runat="server" class="form-control" />
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="Button4" CommandName="Update" runat="server" CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Vendor_Click" />
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>
