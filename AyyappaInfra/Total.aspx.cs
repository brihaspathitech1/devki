﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace AyyappaInfra
{
    public partial class Total : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCash();
            }
        }

        protected void BindCash()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select *, isnull((case when status='through cash' then paidamt end),0)as Pcash,isnull((case when status='through Card' then paidamt end),0)as Pcard, isnull((case when status='through Cheque' then paidamt end),0)as Pcheque from Payment p inner join  Customer c on c.id=p.CustomerID  where p.back_up is null and  p.id<>'20300' and p.id <>'20372' AND P.ID<>'20103' and P.ID<>'20048' order by p.Id desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        double total = 0, paidtotal = 0, remaintotal = 0, cardtotal = 0, chequetotal = 0;
        protected void Databound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                if (DataBinder.Eval(e.Row.DataItem, "Totalamt") == DBNull.Value)
                {
                    total += 0;

                }
                else
                {
                    total += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Totalamt"));
                }
                if (DataBinder.Eval(e.Row.DataItem, "Pcash") == DBNull.Value)
                {
                    paidtotal += 0;
                }
                else
                {
                    paidtotal += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Pcash"));
                }
                if (DataBinder.Eval(e.Row.DataItem, "Pcard") == DBNull.Value)
                {
                    cardtotal += 0;
                }
                else
                {
                    cardtotal += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Pcard"));
                }
                if (DataBinder.Eval(e.Row.DataItem, "Pcheque") == DBNull.Value)
                {
                    chequetotal += 0;
                }
                else
                {
                    chequetotal += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "Pcheque"));
                }
                if (DataBinder.Eval(e.Row.DataItem, "remainamt") == DBNull.Value)
                {
                    remaintotal += 0;
                }
                //else
                //{
                //    remaintotal += Convert.ToDouble(DataBinder.Eval(e.Row.DataItem, "remainamt"));
                //}
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lPOAmount = (Label)e.Row.FindControl("lblgrandtotal");
                lPOAmount.Text = total.ToString();
                Label lblvalue = (Label)e.Row.FindControl("lblpaidtotal");
                lblvalue.Text = paidtotal.ToString();
                Label lblvalue1 = (Label)e.Row.FindControl("remaintotal");
                lblvalue1.Text = remaintotal.ToString();
                Label lblvalue2 = (Label)e.Row.FindControl("lblcardtotal");
                lblvalue2.Text = cardtotal.ToString();
                Label lblvalue3 = (Label)e.Row.FindControl("lblchequetotal");
                lblvalue3.Text = chequetotal.ToString();
            }
        }

        protected void Submit(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            if (txtstart.Text != "" && txtend.Text != "" && dropaddress.SelectedItem.Text != "")
            {
                DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand("select *, isnull((case when status='through cash' then paidamt end),0)as Pcash,isnull((case when status='through Card' then paidamt end),0)as Pcard, isnull((case when status='through Cheque' then paidamt end),0)as Pcheque from Payment p inner join  Customer c on c.id=p.CustomerID inner join Login l on l.EmployeeName=p.username where l.Branch='" + dropaddress.SelectedItem.Text + "' and (Date between ('" + txtstart.Text + "') and ('" + txtend.Text + "')) and p.back_up is null order by p.Id desc", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
            else
            {
                BindCash();
            }
        }

        protected void Excel_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Charset = "";
            string FileName = "Cash Report" + DateTime.Now + ".xls";
            StringWriter strwritter = new StringWriter();
            HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            GridView1.GridLines = GridLines.Both;
            GridView1.HeaderStyle.Font.Bold = true;
            GridView1.RenderControl(htmltextwrtter);
            Response.Write(strwritter.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            //required to avoid the run time error "  
            //Control 'GridView1' of type 'Grid View' must be placed inside a form tag with runat=server."  
        }
    }
}