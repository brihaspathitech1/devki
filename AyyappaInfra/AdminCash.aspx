﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin2.Master" AutoEventWireup="true" CodeBehind="AdminCash.aspx.cs" Inherits="AyyappaInfra.AdminCash" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Cash Report
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            <div class="col-md-3">
                                <div class="form-group">
                                    Search Customer<asp:TextBox ID="txtcustomer" CssClass="form-control" runat="server" AutoPostBack="true" OnTextChanged="Name_Change"></asp:TextBox>
                                       <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtcustomer"
                                                MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1"
                                                CompletionInterval="1" ServiceMethod="GetVendor" UseContextKey="True">
                                            </cc1:AutoCompleteExtender>
                                </div>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>
                             <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false" DataKeyNames="status1">
                                    <Columns>
                                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                                        <asp:BoundField DataField="CustomerId" HeaderText="cid" />
                                        <asp:BoundField DataField="Billno" HeaderText="Bill No" />
                                        <asp:BoundField DataField="Name" HeaderText="Customer Name" />
                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <%# Eval("Mobile")+"-"+Eval("Address") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    <%--    <asp:BoundField DataField="Mobile" HeaderText="Mobile" />
                                        <asp:BoundField DataField="Address" HeaderText="Address" />--%>
                                        <asp:BoundField DataField="TotalAmt" HeaderText="Total Amount" />
                                        <asp:BoundField DataField="Paidamt" HeaderText="Paid Amount" />
                                        <asp:BoundField DataField="Remainamt" HeaderText="Remaining" />
                                       <asp:TemplateField HeaderText="Cheque/Credit No">
                                          <ItemTemplate>
                                              <%# Eval("Cardno")+""+Eval("Chequeno") %>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                        <asp:BoundField DataField="status" HeaderText="Payment Through" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnview" runat="server" Text="View" CssClass="btn btn-primary btn-xs" CommandArgument='<%# Eval("Billno") %>' OnClick="View_Click"  />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnupdate" runat="server" CssClass="btn btn-success btn-xs" CommandArgument='<%# Eval("Billno") %>' Text="Update Payment" OnClick="Payment_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                     <%--   <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnprint" runat="server" CssClass="btn btn-warning btn-xs" CommandArgument='<%# Eval("Billno") %>' Text="Print Bill" OnClick="Print_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                                    </ContentTemplate>
                                 </asp:UpdatePanel>

                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" CancelControlID="btnclose" PopupControlID="Panel1"></cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="300px" Width="650px">
                                 <h3 class="text-center">Payment Details</h3>
                                <hr />
                                 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                <asp:GridView ID="gridpanel" runat="server" CssClass="table table-respnsive table-striped" GridLines="None" AutoGenerateColumns="false" >
                                    <Columns>
                                         <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:yyyy-MM-dd}" />
                                        <asp:BoundField DataField="Billno" HeaderText="Bill No" />
                                         <asp:BoundField DataField="TotalAmt" HeaderText="Total Amount" />
                                        <asp:BoundField DataField="Paidamt" HeaderText="Paid Amount" />
                                        <asp:BoundField DataField="Remainamt" HeaderText="Remaining" />
                                       <asp:TemplateField HeaderText="Cheque/Credit No">
                                          <ItemTemplate>
                                              <%# Eval("Cardno")+""+Eval("Chequeno") %>
                                          </ItemTemplate>
                                       </asp:TemplateField>
                                        <asp:BoundField DataField="status" HeaderText="Payment Through" />

                                    </Columns>
                                </asp:GridView>
                                    </ContentTemplate>
                                     </asp:UpdatePanel>
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <div class="col-md-3">
                                    <div class="form-group">
                                         <asp:Button ID="btnclose" CssClass="btn btn-danger btn-sm" runat="server" Text="Close" />
                                    </div>
                                </div>
                               
                            </asp:Panel>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
