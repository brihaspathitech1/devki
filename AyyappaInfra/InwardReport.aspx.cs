﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class InwardReport : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
                BindVendor();
            }
        }

        //Binding Vendors
        protected void BindVendor()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select Id,Name from Vendor", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            dropvendor.DataSource = dr;
            dropvendor.DataTextField = "Name";
            dropvendor.DataValueField = "Id";
            dropvendor.DataBind();
            dropvendor.Items.Insert(0, new ListItem("", "0"));
            conn.Close();
        }
        //Binding Grid from Inward table
        protected void BindGrid()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("With cte as (Select date,vendorName,hsnno,Invno,InvDate,Total,billno,ROW_NUMBER() over (partition by billno order by billno asc) as rn from Inward) select * from cte where rn=1", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridInward.DataSource = ds;
                GridInward.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridInward.DataSource = ds;
                GridInward.DataBind();
                int columncount = GridInward.Rows[0].Cells.Count;
                GridInward.Rows[0].Cells.Clear();
                GridInward.Rows[0].Cells.Add(new TableCell());
                GridInward.Rows[0].Cells[0].ColumnSpan = columncount;
                GridInward.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Submit(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            DataSet ds = new DataSet();
            if (txtdate.Text != "")
            {
                if (dropvendor.SelectedItem.Text == "" && txtdate.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("Select * from Inward where date='" + txtdate.Text + "' order by id desc", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("Select * from Inward where date='" + txtdate.Text + "' and vendorName='" + dropvendor.SelectedItem.Text + "'", conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridInward.DataSource = ds;
                    GridInward.DataBind();
                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridInward.DataSource = ds;
                    GridInward.DataBind();
                    int columncount = GridInward.Rows[0].Cells.Count;
                    GridInward.Rows[0].Cells.Clear();
                    GridInward.Rows[0].Cells.Add(new TableCell());
                    GridInward.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridInward.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }
        protected void View_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            Button btn = sender as Button;
            string billno = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Select Brand,product,category,size,quantity,Price,amount,Tax,Taxamount,Subtotal,Total,SUM(convert(float,amount)+convert(float,Taxamount)) as totalamt from Billing where Billno='" + billno.ToString() + "' group by Brand,product,category,size,quantity,Price,amount,Tax,Subtotal,Total,Taxamount", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblsubtotal.Text = ds.Tables[0].Rows[0]["Subtotal"].ToString();
                lbltotal.Text = ds.Tables[0].Rows[0]["Total"].ToString();
                Gridpopup.DataSource = ds;
                Gridpopup.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                Gridpopup.DataSource = ds;
                Gridpopup.DataBind();
                int columncount = Gridpopup.Rows[0].Cells.Count;
                Gridpopup.Rows[0].Cells.Clear();
                Gridpopup.Rows[0].Cells.Add(new TableCell());
                Gridpopup.Rows[0].Cells[0].ColumnSpan = columncount;
                Gridpopup.Rows[0].Cells[0].Text = "No Records Found";
            }
            ModalPopupExtender1.Show();
        }
    }
}