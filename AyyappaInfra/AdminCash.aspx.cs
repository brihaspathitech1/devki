﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AyyappaInfra
{
    public partial class AdminCash : System.Web.UI.Page
    {
        String connstr = ConfigurationManager.ConnectionStrings["DevkiInterior"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        //Binding gridview
        protected void BindGrid()
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select * from Payment p inner join Customer c on c.id=p.CustomerID where p.username='" + Session["Empname"].ToString() + "' order by p.Id desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
                GridView1.Columns[1].Visible = false;
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetVendor(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DevkiInterior"].ToString());
            SqlDataAdapter adp = new SqlDataAdapter(" SELECT Name FROM Customer where Name like'" + prefixText + "%'", con);


            DataTable dt = new DataTable();
            adp.Fill(dt);
            List<string> Mobilenumber = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Mobilenumber.Add(dt.Rows[i]["Name"].ToString());
            }
            return Mobilenumber;
        }

        protected void Name_Change(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("select * from Payment p inner join Customer c on c.id=p.CustomerID where c.Name='" + txtcustomer.Text + "' and p.username='" + Session["Empname"].ToString() + "' order by p.Id desc", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
                GridView1.Columns[1].Visible = false;
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void Payment_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow row = btn.NamingContainer as GridViewRow;
            string status = GridView1.DataKeys[row.RowIndex].Values[0].ToString();
            string bill = btn.CommandArgument;
            Session["bill"] = bill;
            Session["status"] = status;
            Response.Redirect("~/UpdatePayment3.aspx");
        }

        protected void View_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string bill = btn.CommandArgument;
            GridView1.Columns[1].Visible = false;
            GridViewRow row = btn.NamingContainer as GridViewRow;
            string status = GridView1.DataKeys[row.RowIndex].Values[0].ToString();
            string value = GridView1.Rows[row.RowIndex].Cells[1].Text;
            SqlConnection conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("Select * from PaymentStatus where billno='" + bill.ToString() + "' and Status1='" + status + "' and CustomerID='" + value + "'", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gridpanel.DataSource = ds;
                gridpanel.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gridpanel.DataSource = ds;
                gridpanel.DataBind();
                int columncount = gridpanel.Rows[0].Cells.Count;
                gridpanel.Rows[0].Cells.Clear();
                gridpanel.Rows[0].Cells.Add(new TableCell());
                gridpanel.Rows[0].Cells[0].ColumnSpan = columncount;
                gridpanel.Rows[0].Cells[0].Text = "No Records Found";
            }
            ModalPopupExtender1.Show();
        }

        //protected void Print_Click(object sender, EventArgs e)
        //{
        //    Button btn = sender as Button;
        //    string bill = btn.CommandArgument;
        //    Session["Bill"] = bill;
        //    Response.Redirect("~/PaymentBill.aspx");
        //}
    }
}