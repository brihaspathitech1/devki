﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="ItemsReport.aspx.cs" Inherits="AyyappaInfra.ItemsReport" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Available Items
        
      </h1>

        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <asp:UpdatePanel ID="Update" runat="server">
                                <ContentTemplate>

                                
                            <div class="col-md-3">
                                <div class="form-group">
                                    Search by Category<asp:DropDownList ID="dropcategory" CssClass="form-control" OnSelectedIndexChanged="Category_Change" AutoPostBack="true" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                                    </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            <div class="col-md-12">
                                <asp:GridView ID="griditems" CssClass="table table-respnsive table-striped" GridLines="None" runat="server" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="Brand" HeaderText="Brand" />
                                         <asp:BoundField DataField="Product" HeaderText="Product" />
                                         <asp:BoundField DataField="Category" HeaderText="Category" />
                                         <asp:BoundField DataField="Size" HeaderText="Size" />
                                         <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnedit" CssClass="btn btn-success btn-sm" CommandArgument='<%# Eval("Id") %>' runat="server" Text="Edit" OnClick="Edit_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btndelete" CssClass="btn btn-danger btn-sm" CommandArgument='<%# Eval("Id") %>' runat="server" Text="Delete" OnClick="Delete_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1" CancelControlID="btnclose" TargetControlID="HiddenField1"></cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="300px" Width="650px">
                                 <h3 class="text-center">Update Item Details</h3>
                                <hr />
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                 <div class="col-md-6">
                                <div class="form-group">
                                Brand<asp:TextBox ID="txtbrand" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                     </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                <div class="col-md-6">
                                <div class="form-group">
                                Product<asp:TextBox ID="txtproduct" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                <div class="col-md-6">
                                <div class="form-group">
                                Category<asp:TextBox ID="txtcategory" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                <div class="col-md-6">
                                <div class="form-group">
                                Size<asp:TextBox ID="txtsize" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                <div class="col-md-6">
                                <div class="form-group">
                                Quantity<asp:TextBox ID="txtquantity" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>

                                <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                   
                                    <asp:Button ID="btnupdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="Update_Click" />
                                   
                                    </div>
                                    </div>
                                <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                    
                                    <asp:Button ID="btnclose" CssClass="btn btn-danger" runat="server" Text="Close" />
                                  
                                    </div>
                                    </div>
                            </asp:Panel>
                            <asp:Label ID="lblid" runat="server" Text="" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
</asp:Content>
